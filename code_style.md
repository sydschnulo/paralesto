# M2DO ParaLeSTO code style guide


## Introduction
This project uses the [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)

Any exceptions or clarifications to the style guide are described in this document.


## Naming

### Variable Names

#### Class Data members
Class data members do not have trailing underscores.

### Enumerator Names
Enumerators should be names like macros


## Comments
For documentation, use Doxygen style comments in the header files. See the 
following sections for Doxygen formatting examples. For a more in-depth 
explanation of creating Doxygen code blocks see the [Doxygen manual](https://www.doxygen.nl/manual/docblocks.html).

### Comment Style
Use the `//` syntax.

### Class comments
Comments that describe the class should be in the .h file and formatted as follows:

`//! \class Element`
`/*! \brief Base class for FEA elements`
`    This is an abstract class that is used to interface elements of different`
`    physics. This class assumes linear shape functions and defines the methods`
`    for computing the Jacobian matrix, B matrix, and transformations between`
`    the natural and physical coordinate systems. Children of this class must`
`    define a method for calculating the element stiffness matrix, K.`
`*/`

### Function comments
Comments that describe the class should be in the .h file and formatted as follows:

`//! \brief Gets the indicies of the elements within the defined area`
`/*! \param coord`
`      The (physical) coordinate of the centroid of the rectangular area`
`      defined by the tolerance, tol. The first and second elements are the`
`      the x and y coordinates, respectively.`
` `
`    \param tol`
`      A vector that defines the area of the selected nodes. The first and`
`      second elements define half of the x and y dimensions of the`
`      selected area, respectively.`
` `
`    \return`
`      Indicies of elements within the defined area`
` `
`    \note The area is the rectangle defined by the x coord +/- the first`
`    element of tol and the y coord +/- the second element of tol.`
`*/`
`std::vector<int> GetElementsByCoordinates (std::vector<double> coord,
                                           std::vector<double> tol) ;`

### Variable Comments
Comments that describe class data members should be written inline using `//!<` syntax
for short comments and `/*!*/` for longer comments

### TODO Comments
In .h files, include a doxygen `\todo` tag in front of the usual TODO comment syntax
`//! \todo TODO(carolina): Add a brief description`

### Deprecation Comments
In .h files, include a doxygen `\deprecated` tag in front of the usual DEPRECATED
comment syntax
`//! \deprecated DEPRECATED(carolina): Use MeshHyperRectangle instead`


## Formatting

### Line Length
Use the 80 character line length as more of a soft limit than hard rule.

### Horizontal Whitespace

#### General
Always put a space before an open parenthesis, e.g.
Function declaration `int GetNumNodes () const ;`
Function call `element.GetNumNodes () ;`

Always put a space before a semi-colon, e.g.
`for (int i = 0 ; i < 5 ; i++) {`
`count += 1 ;`
`}`

> Note from carolina: I personally dislike the above whitespace rules, but I was
> out voted, so I guess this is life now.

#### Operators
Include spaces between binary operators

### Vertical Whitesapce
Use two blank lines between function implementations in .cpp files
`int Mesh :: NumEntries () {`
`\\ Implementation`
`...`
`}`
`\\ blank line`
`\\ blank line`
`void Mesh :: AssignDof () { \\ start of other function implementation`
`...`
`}`
