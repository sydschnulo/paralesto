#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include "para_lsm.h"

namespace lsm = PARA_LSM ;

int main(){

	//mesh size
	int box_x = 250;
	int box_y = 100;
	int box_z = 250;

	// Define the grid
	lsm::Grid grid(box_x, box_y, box_z);

	// Create an object of the
	// levelset class
	lsm::LevelSet3D levelSet(grid);

	// Make a Box
	levelSet.MakeBox() ;

	// create a boudnary object
	lsm::Boundary boundary(levelSet);

	// create a sensitivity class
	double r0 = 0.28*box_x;
	double r1 = 0.4*r0, r2 = 0.15*r0;
	int p0 = 5, p1 = 100, q0 = -2, q1 = -2;

	// create a sensitivity class
	lsm::Sensitivity helix(r0,r1,r2,
		p0, p1, q0, q1,
		box_x/2.0,  box_y/2.0, box_z/2.0);

 int max_iter = 80; // loop for max_iter iterations
 for(int iter = 0; iter <= max_iter; iter++){

	 // Marching cubes to
	 // compute boundary mesh
	 boundary.MarchCubes();

	 std::cout << "Finished marching cubes." << std::endl;

	 // ComputeVolumeFractions
	 levelSet.ComputeVolumeFractions();
	 std::cout << "Finished volume fractions." << std::endl;

		// compute boundary point sensitivities
		std::vector<double> bsens(boundary.numTriangles , 0.0);
		#pragma omp parallel for
	 	for(int i = 0 ; i < boundary.numTriangles; i++){
			bsens[i] = helix.GetSensitivity(boundary.bPoints[i]);
	 	}
		std::cout << "Finished sensitivity." << std::endl;


	 	//Optimize
		double moveLimit = 1.5;
		double volCons = 0.02;

	 	boundary.Optimize(bsens, moveLimit,
			levelSet.volume, volCons);

		std::cout << "Finished optimizing." << std::endl;

		boundary.ExtrapolateVelocities();
		std::cout << "Finished Extrapolating." << std::endl;

	 // Update level set
	 levelSet.Update();

	 std::cout << "Finished updating." << std::endl;


	 // output stl files every now end then
		if(fmod( iter , 10 ) == 0 ){
			// levelSet.SmoothPhi(1);
			boundary.WriteSTL("helixAfterIter" + std::to_string(iter) + ".stl");
		}
		std::cout << "Completed iteration "<< iter << "\n"<< std::endl;
	}// iter

	return 0;
}
