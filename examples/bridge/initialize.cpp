#include "initialize.h"

namespace PARA_FEA {

InitializeOpt :: InitializeOpt () {

  nelx = 128; nely = 64; nelz = 64 ;

  nlvls = 3 ;

  lx = 2.0; ly = 1.0; lz = 1.0 ;

  maxItr = 101 ;

  Emax = 1.0; Emin = 1.0e-9; nu = 0.3 ;
  volCons = 0.15 ;

  double a = lx/nelx ;
  double b = ly/nely ;
  double c = lz/nelz ;
  double eps = 0.1*std::min ({a,b,c}) ;//small value
  double infi = 10.0*std::max ({lx,ly,lz}) ;


  loads.push_back (Force ( 0.0, 0.0, -0.001,
                  0.5*lx, 0.5*ly, lz,
                  infi, infi, eps)) ;
  //

  fixedDofs.push_back (FixedDof (0, 0, 0,
                  0.0*lx, 0.0*ly, 0.0*lz,
                  3*a, infi, eps)) ;
  //

  fixedDofs.push_back (FixedDof (0, 1, 1,
                  lx, 0.0*ly, 0.0*lz,
                  eps, infi, infi)) ;
  //

  static PARA_LSM::Cuboid cube0 (nelx/2.0, nely/2.0, nelz-2.0,
  			             nelx/2.0,  nely/2.0, 2.0) ;
  //
  fixedBlobs.push_back (std::make_unique<PARA_LSM::Cuboid> (cube0)) ;

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA