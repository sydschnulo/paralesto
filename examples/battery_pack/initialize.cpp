#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 200; nely = 80; nelz = 100;

  nlvls = 3;

  lx = 0.4; ly = 0.16; lz = 0.2;

  maxItr = 20;


  Emax = 69.0e9; Emin = 69.0; nu = 0.3;
  volCons = 0.15;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  loads.push_back(Force( 0.0, 0.0, 0.0 ,
                  lx, 0.5*ly,  0.0*lz,
                  1.0*a+eps, infi, 1.0*c+eps));
  //
  loads.push_back(Force( 0.0, -0.0, 0.0,
                  lx, 0.5*ly,  1.0*lz,
                  1.0*a+eps, infi, 1.0*c+eps));
  //
  loads.push_back(Force( 0.0, 0.0, -5.0,
                  lx, 0.0*ly,  0.5*lz,
                  0.0*a+eps, 0.0*b+eps, infi));
  //
  loads.push_back(Force( 0.0, 0.0, 5.0,
                  lx, 1.0*ly,  0.5*lz,
                  0.0*a+eps, 0.0*b+eps, infi));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 0.0*ly, 0.0*lz,
                  eps, infi, infi));
  //
  // // Fixed Blobs
  // double hFix = 5.0;
  // static Cuboid cube0(1*nelx-hFix, 0*nely + hFix, 0.4*nelz-hFix,
  //             hFix,  hFix, hFix);
  // static Cuboid cube1(1*nelx-hFix, 0.4*nely - hFix, 0.4*nelz-hFix,
  //             hFix,  hFix, hFix);
  // static Cuboid cube2(0*nelx+hFix, 1*nely -hFix, 0.4*nelz-hFix,
  //             hFix,  hFix, hFix);
  // static Cuboid cube3(0.4*nelx-hFix, 1.0*nely - hFix, 0.4*nelz-hFix,
  //            hFix,  hFix, hFix);

  // //
  // fixedBlobs.push_back(&cube0);
  // fixedBlobs.push_back(&cube1);
  // fixedBlobs.push_back(&cube2);
  // fixedBlobs.push_back(&cube3);
  //


  // static Cuboid hole0(0.7*nelx, 0.7*nely, 0.0*nelz,
  //             0.3*nelx,  0.3*nely, 10.0*nelz);
  //
  // static Cuboid hole1(0.7*nelx, 0.0*nely, 0.7*nelz,
  //             0.3*nelx,  10.0*nely, 0.3*nelz);
  //
  // static Cuboid hole2(0.0*nelx, 0.7*nely, 0.7*nelz,
  //             10.0*nelx,  0.3*nely, 0.3*nelz);
  //
  //
  // initialVoids.push_back(std::move(&hole0));
  // initialVoids.push_back(std::move(&hole1));
  // initialVoids.push_back(std::move(&hole2));
  //
  // domainVoids.push_back(std::move(&hole0));
  // domainVoids.push_back(std::move(&hole1));
  // domainVoids.push_back(std::move(&hole2));

  static PARA_LSM::Cuboid fix1(nelx, 0.5*nely, 0.0*nelz,
              5.0,  0.5*nely, nelz);
  //

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (fix1));


  //channels
  static PARA_LSM::Cuboid hole0(0.5*nelx, 0.5*nely, 0.5*nelz,
              0.5*nelx,  0.5*nely-10.0, 0.5*nelz-2);
  // //
  // static Cuboid hole1(0.0*nelx, nely, 0.2*nelz,
  //             nelx,  2.0/8.0*nely, 0.075*nelz);
  // //
  // static Cuboid hole2(0.0*nelx, nely, 0.4*nelz,
  //             nelx,  2.0/8.0*nely, 0.075*nelz);
  // //
  // static Cuboid hole3(0.0*nelx, nely, 0.6*nelz,
  //             nelx,  2.0/8.0*nely, 0.075*nelz);
  // //
  // static Cuboid hole4(0.0*nelx, nely, 0.8*nelz,
  //               nelx,  2.0/8.0*nely, 0.075*nelz);
  // //

  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole0));
  // initialVoids.push_back(&hole1);
  // initialVoids.push_back(&hole2);
  // initialVoids.push_back(&hole3);
  // initialVoids.push_back(&hole4);

  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole0));

  static std::vector<PARA_LSM::Cylinder> batteries;
  static std::vector<PARA_LSM::Cylinder> nuts;

  for(int i = 0.1*nelz; i < nelx; i=i+0.2*nelz){
      for(int k = 0.1*nelz; k < nelz; k=k+0.2*nelz){
          PARA_LSM::Cylinder cylinder1(i, 0*nely, k,
                      nely, 0.05*nelz, 0.0, 1);
            batteries.push_back(cylinder1);

            PARA_LSM::Cylinder cylinder2(i, 0*nely, k,
                        3.0, 7.0, 5.0, 1);
            //
            PARA_LSM::Cylinder cylinder3(i, nely, k,
                        3.0, 7.0, 5.0, 1);
            //
            nuts.push_back(cylinder2);
            nuts.push_back(cylinder3);

      }
  }

  // for(int i = 0.2*nelz; i < nelx; i=i+0.2*nelz){
  //     for(int k = 0.2*nelz; k < nelz; k=k+0.2*nelz){
  //         Cylinder cylinder1(i, 0*nely, k,
  //                     5.0, 0.03*nelz, 0.0, 1);
  //           batteries.push_back(cylinder1);
  //     }
  // }

  for(int i = 0; i < batteries.size(); i++){
      initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (batteries[i]));
      domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (batteries[i]));
  }

  for(int i = 0; i < nuts.size(); i++){
      fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (nuts[i]));
  }

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA
