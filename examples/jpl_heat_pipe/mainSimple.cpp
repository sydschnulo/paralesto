#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";

int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;

			// set the flow variables
			fea::InitializeOpt myInitFlow(true);

			fea::TopOpt1D *optHeat = new fea::TopOpt1D(myInit);
			fea::Poisson *physicsHeat = new fea::Poisson(optHeat, myInit);

			fea::TopOpt1D *optFlow = new fea::TopOpt1D(myInitFlow);
			fea::Poisson *physicsFlow = new fea::Poisson(optFlow, myInitFlow);

			// set the convection parameters for the flow to be 0
			optFlow->hSurfConv = 0.0;
			double hFlow = 1.0; // the constant that needs to be computed, that related heat flux to flow rate


		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = optHeat->nxyz[0]-1;
			int nely = optHeat->nxyz[1]-1;
			int nelz = optHeat->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// create quadrature object
			lsm::MomentQuadrature momQuad(boundary);

			// std::vector<lsm::Cuboid> cubes;
			// lsm::Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			std::ofstream txtfile_iterhist;//io

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);

				txtfile_iterhist.open("iterhist.txt");

				#if 1
				std::ifstream infile;
				infile.open("SDatgridpointsInp.txt");
				for(int k = 0; k <= nelz; k ++){
			     for(int j = 0; j <= nely; j ++){
			       for(int i = 0; i <= nelx; i ++){
							 double tempval;
							 infile >> tempval;
							  level_set_3d.phi[level_set_3d.GridPtToIndex(i,j,k)].val = tempval;
						 }//i
					 }//j
				 }//k
				 #endif
			}//rank

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
				std::vector<std::vector<double>>areaToVolForMap;
				int numNodes = 8;
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
				areaToVolForMap.resize(nElements);
				for(int i = 0; i < nElements; i++){
					areaToVolForMap[i].resize(numNodes,0.0);
				}
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(optHeat, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < myInit.maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					std::cout << "Finished boundary computation." << std::endl;
					boundary.WriteSTL();
					// level_set_3d.WriteSD();
					// return 0;
					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					// Compute quadrature
					bool ignoreBoundingBox = true;
					momQuad.ComputeElementsAreaToVolume(ignoreBoundingBox);

					std::cout << "Finished elements to area computation." << std::endl;

					// for(int i = 0; i < 8; i++){
					// 	std::cout << "momQuad.elementsAreaToVolume = " << momQuad.elementsAreaToVolume[0][i] << std::endl;
					// }
					//==================================================================

					// assign volume fractions to petsc mesh
					// step-1
					double count_rigid = 0;
					for(int k = 0; k < nelz; k++){
						for(int j = 0; j < nely; j++){
							for(int i = 0; i < nelx; i++){
								int countFEM = i + j*nelx + k*nelx*nely;
								// centroids
								double xc = i+0.5; double yc = j+0.5;	double zc = k+0.5;

								volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
								= std::max(level_set_3d.volFractions[countFEM],0.0);
							}
						}
					}

					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(optHeat->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;
					//=================================================================

					// assign quadrature to petsc mesh
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
							for(int i = 0; i < numNodes; i++){
								areaToVolForMap[volfrac_to_petsc_map[countFEM]][i]
								 = momQuad.elementsAreaToVolume[countFEM][i];
							}
					}
					// step-2
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						for(int i = 0; i < numNodes; i++){
								VecSetValue(optHeat->areaToVolume[i], countFEM,
									areaToVolForMap[countFEM][i] , INSERT_VALUES );
						}
					}

					std::cout << "Assigned area to volume." << std::endl;

					//=================================================================

				}
				VecAssemblyBegin(optHeat->x);	VecAssemblyEnd(optHeat->x);

				for(int i = 0; i < numNodes; i++){
					VecAssemblyBegin(optHeat->areaToVolume[i]);	VecAssemblyEnd(optHeat->areaToVolume[i]);
				}


				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// compute sensitivities
				physicsHeat->myRTOL = 1.0e-8;
				// ierr = physicsHeat->ComputeSensitivities(optHeat); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				// fea::MyGather mygather(optHeat->dfdx);
				// double compliance = optHeat->fx;

				// temperature plot
				VecSet(physicsHeat->U, 0.0);
				ierr = physicsHeat->ComputeCentroidTemperatures(optHeat, true); CHKERRQ(ierr);
				fea::MyGather mygatherTemp(optHeat->DeltaTVec);
				ierr = physicsHeat->ComputeSurfaceConvectionSensitivities(optHeat); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygather(optHeat->dfdx);
				double compliance = optHeat->fx;


				if(rank == 0){
					// Interpolate tempaerature
					std::vector<double> bTemp(boundary.numTriangles, 0.0);
					boundary.InterpolateBoundarySensitivities(bTemp, mygatherTemp.sensiArray,
						volfrac_to_petsc_map, 2.0, 0);
						// output temperature
						{
							std::ofstream txtfile_temp;
							txtfile_temp.open("temp.txt");
							for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
							 txtfile_temp << bTemp[i1] << std::endl;
							}
							txtfile_temp.close();
						}
						boundary.WritePatch();
				}

				// ========================================================================
				// assemble flow variables
				VecAYPX(optFlow->x, 0.0, optHeat->x );
				for(int i = 0; i < numNodes; i++){
					VecAYPX(optFlow->areaToVolume[i], 0.0, optHeat->areaToVolume[i] );
				}
				// solve flow equations
				// compute flux deltaT
				VecAYPX(optFlow->DeltaTVecFlux, 0.0, optHeat->DeltaTVec);
				VecShift(optFlow->DeltaTVecFlux, -optHeat->Tambi);
				VecScale(optFlow->DeltaTVecFlux, -hFlow);
				ierr = physicsFlow->ComputeCentroidTemperatures(optFlow, true); CHKERRQ(ierr);
				ierr = physicsFlow->ComputeSurfaceConvectionSensitivities(optFlow); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygatherFlow(optFlow->DeltaTVec);
				fea::MyGather mygatherPreSens(optFlow->dfdx);
				double pDrop = optFlow->fx;
				// interpolate
				if(rank == 0){
					// Interpolate tempaerature
					std::vector<double> bPres(boundary.numTriangles, 0.0);
					boundary.InterpolateBoundarySensitivities(bPres, mygatherFlow.sensiArray,
						volfrac_to_petsc_map, 2.0, 0);
						// output temperature
						{
							std::ofstream txtfile_temp;
							txtfile_temp.open("pressure.txt");
							for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
							 txtfile_temp << bPres[i1] << std::endl;
							}
							txtfile_temp.close();
						}
				}

				//==========================================================================
				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {
					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> comSens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(comSens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);

							std::vector<double> presSens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(presSens, mygatherPreSens.sensiArray,
								volfrac_to_petsc_map, 2);

								double wTemp = 0.5/50.0, wPdrop = 0.5/4.0;
							for(int i = 0; i < comSens.size(); i++)	comSens[i] = wTemp*comSens[i] - wPdrop*presSens[i];

							// flip the sign
							for(int i = 0; i < presSens.size(); i++)	presSens[i] = - 1.0*presSens[i];

								std::cout << "Completed sensitivity interpolation." << std::endl;
						//===========================================================================
						// Optimize boundary velocities
						 double move_limit = 0.25;
						 // if(itr > 50) move_limit = 0.1;
						 // boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons, 0);

					 	std::vector<double> volSens(boundary.numTriangles, 1.0);

						// constraints
						/*
						int nCons = 2;
						std::vector<std::vector<double>> conSens;
						conSens.resize(nCons);
						std::vector<double> conMaxVals(nCons);
						std::vector<double> conCurVals(nCons);
						// volume constraint
						conSens[0] = volSens;
						conMaxVals[0] = myInit.volCons*nelx*nely*nelz;
						conCurVals[0] = level_set_3d.volume;
						// pressure constraint
						conSens[1] = presSens;
						conMaxVals[1] = -(-21.0);
						conCurVals[1] = -pDrop;
						*/

							boundary.Optimize(comSens, move_limit, level_set_3d.volume, myInit.volCons);

						// boundary.Optimize(comSens,
						//  conSens, conMaxVals, conCurVals,
						//  move_limit, 1);

							std::cout << "Completed boundary velocities optimization. " << std::endl;

							//NOTE: After debugging  comment this
							for(int i = 0; i < boundary.opt_vel.size(); i++)	boundary.opt_vel[i] = 0.3;

					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						// level_set_3d.MakeDomainHoles(); //NOTE: After debugging uncomment this
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl
					 if(fmod( itr , 2 ) == 1)	{
 						 boundary.WriteSTL("TopAfterIter" +
						 	std::to_string(itr%4) + ".stl");

							level_set_3d.WriteSD();
 				 		}
					 std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Temperature = " << compliance <<  "; PDrop = " << pDrop  << "\n"<< std::endl ;

					 txtfile_iterhist <<itr << " " << compliance << " " << pDrop << " " << level_set_3d.volume / nelx / nely / nelz << std::endl;
				 }



				 //==========================================================================
				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			if(rank == 0) txtfile_iterhist.close();

			delete optHeat;
			delete physicsHeat;

			delete optFlow;
			delete physicsFlow;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
