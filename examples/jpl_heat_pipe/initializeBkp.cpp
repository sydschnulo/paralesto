#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(bool isAux){

  nelx = 128; nely = 64; nelz = 64;

    nlvls = 4;

    lx = 0.2; ly = 0.1; lz = 0.1;

    maxItr = 121;

    Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
    Kmax = 3.2; Kmin = 0.001;
    volCons = 0.45;

    double a = lx/nelx;
    double b = ly/nely;
    double c = lz/nelz;
    double eps = 0.1*std::min({a,b,c});//small value
    double infi = 10.0*std::max({lx,ly,lz});


    fixedHeatDof.push_back(HeatLoad( 0.0,
                    0*a, 0*b, 0*c,
                    25*a+eps, 25*b+eps, eps));
    //

    heatLoad.push_back(HeatLoad( 0.0,
                    lx, ly, lz,
                    25*a+eps, 25*b+eps, eps));
    //

}

InitializeOpt::InitializeOpt(){

  #if 1
  nelx = 128; nely = 64; nelz = 64;

    nlvls = 4;

    lx = 0.2; ly = 0.1; lz = 0.1;

    maxItr = 201;

    Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
    Kmax = 205; Kmin = 0.1;
    volCons = 0.2;

    double a = lx/nelx;
    double b = ly/nely;
    double c = lz/nelz;
    double eps = 0.1*std::min({a,b,c});//small value
    double infi = 10.0*std::max({lx,ly,lz});


    fixedHeatDof.push_back(HeatLoad( 20.0,
                    0*a, 0*b, 0*c,
                    25*a+eps, 25*b+eps, eps));
    //

    heatLoad.push_back(HeatLoad( 0.2,
                    lx, ly, lz,
                    25*a+eps, 25*b+eps, eps));
    //

    double hWidthCube = 2;
    static std::vector<PARA_LSM::Cuboid> holesVector;
    static std::vector<PARA_LSM::Cylinder> holesVectorCylinder;
    for(int j = 8; j <= nely; j=j+16){
        for(int k = 8; k <= nelz; k=k+16){
          PARA_LSM::Cuboid hole1(0.0*nelx, j, k,
                      2*nelx,  hWidthCube, hWidthCube);
          //
          PARA_LSM::Cylinder cHole1(0.0*nelx, j, k,
                      2*nelx,  hWidthCube, 0, 0);
          //
          holesVector.push_back(hole1);
          holesVectorCylinder.push_back(cHole1);
        }
    }
    for(int i = 8; i < nelx; i=i+16){
        for(int k = 8; k <= nelz; k=k+16){
          PARA_LSM::Cuboid hole1(i, 0, k,
                      hWidthCube,  2*nely, hWidthCube);
          //
          PARA_LSM::Cylinder cHole1(i, 0, k,
                      2*nely,  hWidthCube, 0, 1);
          //
          holesVector.push_back(hole1);
          holesVectorCylinder.push_back(cHole1);
        }
    }
    for(int i = 8; i < nelx; i=i+16){
        for(int j = 8; j <= nely; j=j+16){

          if(i < 25 && j < 25){
            PARA_LSM::Cuboid hole1(i, j, nelz,
                        hWidthCube,  hWidthCube, nelz-8);
            //
            PARA_LSM::Cylinder cHole1(i, j, nelz,
                        nelz-8,  hWidthCube, 0, 2);
            //
            holesVector.push_back(hole1);
            holesVectorCylinder.push_back(cHole1);
          }
          else if(i > nelx-25 && j > nely-25){
            PARA_LSM::Cuboid hole1(i, j, 0,
                        hWidthCube,  hWidthCube, nelz-8);
            //
            PARA_LSM::Cylinder cHole1(i, j, 0,
                        nelz-8,  hWidthCube, 0, 2);
            //
            holesVector.push_back(hole1);
            holesVectorCylinder.push_back(cHole1);
          }
          else{
            PARA_LSM::Cuboid hole1(i, j, 0,
                        hWidthCube, hWidthCube, 2*nelz);
            //
            PARA_LSM::Cylinder cHole1(i, j, 0,
                        2*nelz,  hWidthCube, 0, 2);
            //
            holesVector.push_back(hole1);
            holesVectorCylinder.push_back(cHole1);
          }

        }
    }

    for(int i = 0; i < holesVector.size(); i++){
        // initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (holesVector[i]));
    }
    for(int i = 0; i < holesVectorCylinder.size(); i++){
      domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (holesVectorCylinder[i]));
      initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (holesVectorCylinder[i]));
    }

    static PARA_LSM::Cuboid cube0(0, 0, 0,
                25,  25, 5);
    //////
    static PARA_LSM::Cuboid cube1(nelx, nely, nely,
                25,  25, 5);
    //////

    fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
    fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));

    #else

    nelx = 128; nely = 32; nelz = 32;

      nlvls = 4;

      lx = 0.08; ly = 0.02; lz = 0.02;

      maxItr = 1;

      Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
      Kmax = 205; Kmin = 0.01;
      volCons = 0.45;

      double a = lx/nelx;
      double b = ly/nely;
      double c = lz/nelz;
      double eps = 0.1*std::min({a,b,c});//small value
      double infi = 10.0*std::max({lx,ly,lz});


      fixedHeatDof.push_back(HeatLoad( 150.0,
                      0*a, ly/2.0, lz/2.0,
                      eps, infi, infi));
      //

      heatLoad.push_back(HeatLoad( 0.0,
                      lx, ly/2.0, lz/2.0,
                      eps, 5*b+eps, 5*c+eps));
      //

      static PARA_LSM::Cylinder cylinderOuter(0.5*nelx, 0.5*nely, 0.5*nelz,
                  nelx/2, nely/2, 0, 0);
      //
      cylinderOuter.flipDistanceSign = -1;

      static PARA_LSM::Cylinder cylinderInner(0.5*nelx, 0.5*nely, 0.5*nelz,
                  nelx/2-5, nely/2-5, 0, 0);
      //

      initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> cylinderOuter);
      // initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> cylinderInner);

    #endif
}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

FixedDof::FixedDof() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA