#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){
  nelx = 256; nely = 128; nelz = 96;
  // nelx = 16; nely = 8; nelz = 6;

  nlvls = 4;

  lx = 0.18; ly = 0.09; lz = 0.0675;

  maxItr = 201;


  Emax = 1.0; Emin = 1.0e-9; nu = 0.3;
  volCons = 0.1;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  // for the cylinder that applies load
  double rFix = 12;
  double sFix = 8;
  double hFix = 6;

  double rHole = 8;
  double sHole = 12;


  loads.push_back(Force( 0.0, 1.0, 1.0,
                  0.5*lx - 32*a, 2*b, lz - (rFix+sFix)*c,
                  1.0*a+eps, 1.0*b+eps, 1.0*c+eps));
  //
  loads.push_back(Force( 0.0, 1.0, 1.0,
                  0.5*lx - 32*a, (2*(rFix+sFix) - 2.0)*b, lz - (rFix+sFix)*c,
                  1.0*a+eps, 1.0*b+eps, 1.0*c+eps));
  //
  loads.push_back(Force( 0.0, 1.0, 1.0,
                  0.5*lx + 32*a, 2*b, lz - (rFix+sFix)*c,
                  1.0*a+eps, 1.0*b+eps, 1.0*c+eps));
  //
  loads.push_back(Force( 0.0, 1.0, 1.0,
                  0.5*lx + 32*a, (2*(rFix+sFix) - 2.0)*b, lz - (rFix+sFix)*c,
                  1.0*a+eps, 1.0*b+eps, 1.0*c+eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  (rHole + sHole)*a, ly - (rHole + sHole)*b, 0,
                  (5.0)*a+eps, (rHole + sHole)*b + eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  (rHole + sHole)*a, 0.675*ly - (rHole + sHole)*b, 0,
                  (5.0)*a+eps, (rHole + sHole)*b + eps, eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx - (rHole + sHole)*a, ly - (rHole + sHole)*b, 0,
                  (5.0)*a+eps, (rHole + sHole)*b + eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx - (rHole + sHole)*a, 0.675*ly - (rHole + sHole)*b, 0,
                  (5.0)*a+eps, (rHole + sHole)*b + eps, eps));
  //


  // Fixed Blobs
  static PARA_LSM::Cylinder cylinderAfix(0.5*nelx-32.0, rFix + sFix, nelz-rFix - sFix,
              hFix, rFix+sFix, rFix,  0);
  // Fixed Blobs
  static PARA_LSM::Cylinder cylinderBfix(0.5*nelx+32.0, rFix + sFix, nelz-rFix - sFix,
              hFix, rFix+sFix, rFix,  0);

  static PARA_LSM::Cylinder cylinderC(0.5*nelx,  rFix + sFix, nelz-rFix - sFix,
              nelx, rFix, 0.0,  0);
  //



  static PARA_LSM::Cylinder cylinder1(0.0*nelx+ rHole + sHole, 1*nely -  rHole - sHole, 5.0,
              5.0, rHole, 0.0, 2);
  //
  static PARA_LSM::Cylinder cylinder2(1.0*nelx - (rHole + sHole), 1*nely -  rHole - sHole, 5.0,
              5.0, rHole,0.0,2);
  //
  static PARA_LSM::Cylinder cylinder3(1.0*nelx - (rHole + sHole), 0.625*nely -  rHole - sHole, 5.0,
              5.0, rHole, 0.0,2);
  //
  static PARA_LSM::Cylinder cylinder4(0.0*nelx + (rHole + sHole), 0.625*nely -  rHole - sHole, 5.0,
              5.0, rHole, 0.0,2);

  //
  double rHolea = 16;
  static PARA_LSM::Cylinder cylinder1a(0.0*nelx+ rHole + sHole, 1*nely -  rHole - sHole, nelz/2,
              nelz/2-6.0, rHolea, 0.0, 2);
  //
  static PARA_LSM::Cylinder cylinder2a(1.0*nelx - (rHole + sHole), 1*nely -  rHole - sHole, nelz/2,
              nelz/2-6.0, rHolea,0.0,2);
  //
  static PARA_LSM::Cylinder cylinder3a(1.0*nelx - (rHole + sHole), 0.625*nely -  rHole - sHole, nelz/2,
              nelz/2-6.0, rHolea, 0.0,2);
  //
  static PARA_LSM::Cylinder cylinder4a(0.0*nelx + (rHole + sHole), 0.625*nely -  rHole - sHole, nelz/2,
              nelz/2-6.0, rHolea, 0.0,2);

  //

  static PARA_LSM::Cylinder cylinder1Fix(0.0*nelx+ rHole + sHole, 1*nely -  rHole - sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);
  //
  static PARA_LSM::Cylinder cylinder2Fix(1.0*nelx - (rHole + sHole), 1*nely -  rHole - sHole, 5.0,
              5.0, rHole+ sHole,rHole,2);
  //
  static PARA_LSM::Cylinder cylinder3Fix(1.0*nelx - (rHole + sHole), 0.625*nely -  rHole - sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);
  //
  static PARA_LSM::Cylinder cylinder4Fix(0.0*nelx + (rHole + sHole), 0.625*nely -  rHole - sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);


  static PARA_LSM::Cuboid cube0(0*nelx, 0*nely, 1.0*nelz,
              90,  nely, 48);
  static PARA_LSM::Cuboid cube1(1*nelx, 0*nely, 1.0*nelz,
              90.0,  nely, 48);
  static PARA_LSM::Cuboid cube2(0.5*nelx, 1*nely, 1.0*nelz,
              26.0,  nely, 48);

  static PARA_LSM::Cuboid cube4(0.5*nelx, 1*nely, 1.0*nelz+20,
              nelx,  2*nely, 20);
  cube4.theta = std::atan2(40.0,nely - 2*rFix - sFix);
  cube4.rotAxis[0] = 1.0;
  cube4.rotOrigin[1] = 2*rFix + sFix;
  cube4.rotOrigin[2] = nelz;
  //
  static PARA_LSM::Cuboid cube5(0.5*nelx, -50, 0.5*nelz,
              nelx,  50, nelz);
  cube5.theta = std::atan2(-32.0,0.5*nelz);
  cube5.rotAxis[0] = 1.0;
  cube5.rotOrigin[1] = 0;
  cube5.rotOrigin[2] = nelz/2;
  //
  static PARA_LSM::Cuboid cube5Fix(0.5*nelx, 0*nely, 48-6,
              26.0+12.0,  0.5*nely, 6);
  //

  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1a));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2a));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3a));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4a));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube4));
  // initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube5));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderC));

  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1a));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2a));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3a));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4a));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube4));
  // domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube5));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderC));

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderAfix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderBfix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1Fix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2Fix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3Fix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4Fix));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube5Fix));

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA