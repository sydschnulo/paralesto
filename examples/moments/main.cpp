#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";


int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;

			// STEP 1: THE OPTIMIZATION PARAMETERS, DATA AND MESH (!!! THE DMDA !!!)
			fea::TopOpt *opt = new fea::TopOpt(myInit);

			// STEP 2: THE PHYSICS
			fea::LinearElasticity *physics = new fea::LinearElasticity(opt, myInit);

			double rhsSum, nSum;
			VecSum(physics->RHS, &rhsSum);
			VecSum(physics->N, &nSum);
			if(rank == 0) std::cout << "rhsSum = " << rhsSum << std::endl;
			if(rank == 0) std::cout << "nSum = " << nSum << std::endl;

			// this code piece is to show how to scatter a vector of vectors stored on
			// process 0 to be copied on other processes

			#if 0

			int nElems= myInit.nelx * myInit.nely * myInit.nelz;
			// Get pointer to the densities
			PetscScalar *xp;
			VecGetArray(opt->x,&xp);

			// Get the FE mesh structure (from the nodal mesh)
			PetscInt nel, nen;
			const PetscInt *necon;
			ierr = physics->DMDAGetElements_3D(opt->da_nodes,&nel,&nen,&necon); CHKERRQ(ierr);

			for (PetscInt i=0;i<nel;i++){
				if(rank == 0){
					std::cout << xp[i] << std::endl;
				}
			}

			if(rank == 0){
				std::cout << "nel = " << nel << "; nElems =  " << nElems << std::endl;
			}


			return 0;
			#endif



			//TODO: bpoint moments
			// each boundary point,
			// - a vector of momnets sensitivities (dm_i/dz)
			// - sensitivity value set to 0
			// bcast this to all processors
			//
			// Make a petsc vector that stores the start and end of the boundary points vector
			// Ex: if an element has no bpoints, start = -1, end = -1
			//     or it it has a boundary point, start = 5, end = 8
			//
			// Now, in the physics class make a new function for bPoint sensitivity
			// visit all mapped volume fraction indices (for the boundary points)
			// set sensitivity value as s = (dm_i/dz)*(dJ/m_i)


			#if 0
			std::vector<std::vector<double>> someVector;
			if(rank == 0){
				someVector.resize(2);
				someVector[0].resize(3,0.0); someVector[0][2] = 9.5;
				someVector[1].resize(2,1.0); someVector[1][0] = -1.5;
			}

			int someVectorLength = someVector.size(); // broadcast the length
			MPI_Bcast(&someVectorLength, 1, MPI_INT, 0, PETSC_COMM_WORLD);

			someVector.resize(someVectorLength);
			for(int i = 0; i < someVectorLength; i++){
				int someVectorLength1 = someVector[i].size(); // broadcast the length
				MPI_Bcast(&someVectorLength1, 1, MPI_INT, 0, PETSC_COMM_WORLD);
					someVector[i].resize(someVectorLength1,0.0);
					for(int j = 0; j < someVectorLength1; j++){
						double someDouble = someVector[i][j];
							MPI_Bcast(&someDouble, 1, MPI_DOUBLE, 0, PETSC_COMM_WORLD);
							someVector[i][j] = someDouble;
					}
			}

			if(rank == 0) std::cout << "someVector = " << someVector[1][1] << std::endl;
			if(rank == 1) std::cout << "someNumber = " << someVector[1][1] << std::endl;
			MPI_Barrier(PETSC_COMM_WORLD);

			return 0;
			#endif

		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = opt->nxyz[0]-1;
			int nely = opt->nxyz[1]-1;
			int nelz = opt->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// create quadrature object
			lsm::MomentQuadrature momQuad(boundary);

			// std::vector<Cuboid> cubes;
			// Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
			std::vector<std::vector<double>> momentPetscForMap;
			int numMomentQuadPoints = 27;
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
				momentPetscForMap.resize(nElements);
				for(int i = 0; i < nElements; i++){
					momentPetscForMap[i].resize(numMomentQuadPoints,0.0);
				}
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(opt, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < opt->maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					// boundary.WriteSTL();
					level_set_3d.WriteSD();
					std::cout << "Finished boundary computation." << std::endl;
					// return 0;

					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					// Compute quadrature
					momQuad.ComputeElementsQuadrature();

					std::cout << "Finished moments computation." << std::endl;

					// check if the triaArray makes sense
					int nBoundaryElements = 0;
					for(int i = 0; i < nElements; i++){
						// if(i == 63){
						// 	std::cout << "quadrature = " << std::endl;
						// 	std::cout << momQuad.elememtsQuadrature[i] << std::endl;
						// 	std::cout << "quad.sum() = " << momQuad.elememtsQuadrature[i].sum() <<  std::endl;
						// }
						if(momQuad.elememtsQuadrature[i].size() > 0) nBoundaryElements++;
					}
					std::cout << "nBoundaryElements = " << nBoundaryElements << std::endl;

					//===================================================================
					std::vector<double> wGauss = {5.0/18.0, 8.0/18.0, 5.0/18.0};
					Eigen::VectorXd elememtsQuadratureGauss(27);
					int count = 0;
					for(int k = 0; k < 3; k++){
						for(int j = 0; j < 3; j++){
							for(int i = 0; i < 3; i++){
								elememtsQuadratureGauss(count) = wGauss[i]*wGauss[j]*wGauss[k];
								count++;
							}
						}
					}
					//===================================================================

					// assign volume fractions to petsc mesh
					// step-1
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
						= std::max(level_set_3d.volFractions[countFEM],0.0);
					}
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(opt->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;

					// assign quadrature to petsc mesh
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						if(momQuad.elememtsQuadrature[countFEM].size() > 0 &&
						(momQuad.elememtsQuadrature[countFEM].sum() > 0.001 && momQuad.elememtsQuadrature[countFEM].sum() < 0.999)
					 ){
							for(int i = 0; i < numMomentQuadPoints; i++){
								momentPetscForMap[volfrac_to_petsc_map[countFEM]][i]
								 = 8.0*momQuad.elememtsQuadrature[countFEM][i];
							}
						}
						else{
							for(int i = 0; i < numMomentQuadPoints; i++){
								momentPetscForMap[volfrac_to_petsc_map[countFEM]][i]
								 = 8.0*elememtsQuadratureGauss(i) * level_set_3d.volFractions[countFEM];
							}
						}

					}
					// step-2
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						for(int i = 0; i < numMomentQuadPoints; i++){
								VecSetValue(opt->momQuad[i], countFEM,
									momentPetscForMap[countFEM][i] , INSERT_VALUES );
						}
					}

					std::cout << "Assigned moments." << std::endl;

					//=================================================================

				}
				VecAssemblyBegin(opt->x);	VecAssemblyEnd(opt->x);

				for(int i = 0; i < numMomentQuadPoints; i++){
					VecAssemblyBegin(opt->momQuad[i]);	VecAssemblyEnd(opt->momQuad[i]);
				}


				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// Compute sensitivities
				// ierr = physics->ComputeSensitivities(opt); CHKERRQ(ierr);
				ierr = physics->ComputeSensitivitiesFromQuadrature(opt); CHKERRQ(ierr);

				//==========================================================================

				//===========================================================================
				// gather senstitivities on to the zeroeth processor
					 fea::MyGather mygather(opt->dfdx);
				//===========================================================================


				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {
					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> bsens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(bsens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);
								// boundary.ComputeAnalyticalSensitivities(bsens, mygather.sensiArray,
								// 	volfrac_to_petsc_map);
							std::cout << "Completed sensitivity interpolation." << std::endl;
						//===========================================================================
						// Optimize boundary velocities
						 double move_limit = 1.0;
						 if(itr > 50) move_limit = 0.75;
						 if(itr > 100) move_limit = 0.5;
						 boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons);
							std::cout << "Completed boundary velocities optimization. " << std::endl;
					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						level_set_3d.MakeDomainHoles();
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl
					 if(fmod( itr , 2 ) == 1)	{
 						 boundary.WriteSTL("TopAfterIter" +
						 	std::to_string(itr%4) + ".stl");
 				 		}
					 std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Compliance = " << opt->fx << "\n"<< std::endl ;
				 }

				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			delete opt;
			delete physics;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
