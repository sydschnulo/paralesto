#include <fstream>
#include <iostream>
// #include <mpi.h> // TODO(Carolina): resolve mpi issue
#include <omp.h>
#include <petsc.h>
#include <sstream>
#include <string>
#include <vector>

#include "paralesto.h"

namespace FEA = PARA_FEA ;
namespace LSM = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";

int main (int argc, char *argv[]) {
  //============================================================================
  // Set up finite element and sensitivity analysis
  //----------------------------------------------------------------------------

  // Error code for debugging
  PetscErrorCode ierr ;

  // Initialize PETSc/MPI and pass input arguments to PETSc
  PetscInitialize (&argc, &argv, PETSC_NULL, help) ;

  // Declare and define rank variable for printing output
  PetscMPIInt rank = 0 ;
  // MPI_Comm_rank (PETSC_COMM_WORLD, &rank) ;

  // Initialize the input file
  FEA::InitializeOpt phys_init ;

  // Create physics (FEA) object
  FEA::PhysicsWrapper physics_solver (phys_init) ;



  // TODO: create sensitivity analysis module
  // //============================================================================
  // // Set up sensitivity analysis
  // //----------------------------------------------------------------------------

  // // Initialize the input file
  // FEA::InitializeSens sens_init ;

  // // Create sensitivity analysis object
  // FEA::SensitivityWrapper sens_analysis (sens_init) ;



  //============================================================================
  // Set up level set method
  //----------------------------------------------------------------------------

  // Initialize the input file
  LSM::InitializeLsm lsm_init ;

  // Create level set object
  LSM::LevelSetWrapper lsm (lsm_init) ;



  //============================================================================
  // Set up optiimization
  //----------------------------------------------------------------------------

  // Define parameters
  int num_cons = 1 ; // single volume constraint
  double total_volume = lsm_init.nelx * lsm_init.nely * lsm_init.nelz ;
  std::vector<double> max_cons_vals = {0.15*total_volume} ; // max volume
  int opt_algo = 0 ; // use Newton-Raphson for optimizer

  // Create level set object
  LSM::OptimizerWrapper opt (num_cons, max_cons_vals, opt_algo) ;



  //============================================================================
  // Define necessary parameters for optimization loop
  //----------------------------------------------------------------------------

  int max_iter      = 101 ; // Maximum # of iterations
  int count_iter    = 0 ;   // counter for current iteration #
  int min_elem_area = 0.1 ; // min element area to compute sensitivity
  double move_limit = 1.0 ; // move limit for the level set
  bool is_print     = 1 ;   // print progress statements during method runtime
  std::vector <double> densities, df_drho, df_bpt, velocities ;

  // Vector for sensitivities of each constraint
  std::vector < std::vector<double> > cons_sens (num_cons) ;

  // Vector for current value of each constraint
  std::vector <double> curr_cons_vals (num_cons) ;

  // Vector of upper and lower limits for boundary point movement
  std::vector <double> upper_lim, lower_lim ;

  PARALESTO::WrapperIO wrapper_io (lsm, physics_solver) ;
  // PARALESTO::WrapperIO wrapper_io (physics_solver, sens_analysis, lsm, opt) ;



  //============================================================================
  // Main body
  //----------------------------------------------------------------------------

  if (rank == 0) std::cout << "\nStarting main...\n" << std::endl ;

  // Optimization loop
  double iter_start_time, iter_end_time ;
  while (count_iter < max_iter) {
    iter_start_time = MPI_Wtime () ;
    count_iter++ ;

    // Get the densities of the level set mesh
    densities = lsm.CalculateElementDensities (is_print) ;

    // // Solve the governing equation
    // physics_solver.Solve () ; // TODO: separate physics from sensitivity

    // Calculate the partial derivatives with respect to element densities
    // partials = sens_analysis.CalculatePartialDerivatives (densities, is_print) ; // TODO: create sensitivity module
    df_drho = physics_solver.CalculatePartialDerivatives (densities, is_print) ;

    // Map element partials to boundary points
    df_bpt = lsm.MapSensitivities (df_drho, is_print) ;
    std::vector<double> dg_drho = lsm.MapVolumeSensitivities () ;
    cons_sens[0] = dg_drho ;

    // Solve the suboptimization problem
    curr_cons_vals[0] = lsm.GetVolume () ; // get current volume
    if (count_iter > 50) move_limit = 0.75 ;
    if (count_iter > 100) move_limit = 0.5 ;
    lsm.GetLimits (upper_lim, lower_lim, move_limit) ; // get limits for each bpt movement
    opt.SetLimits (upper_lim, lower_lim) ; // set limits for the optimizer
    velocities = opt.Solve (df_bpt, cons_sens, curr_cons_vals, is_print) ;

    // Update the topology by advecting the level set
    lsm.Update (velocities, move_limit, is_print) ;

    // Print output
    iter_end_time = MPI_Wtime () ;
    if (rank == 0) {
      double iter_time = iter_end_time - iter_start_time ;
      std::cout << "Completed update. Iteration time = " << iter_time << std::endl ;
      wrapper_io.PrintMultiPhysIter (count_iter) ;
      int stl_iter = count_iter - 1;
      bool print_stl = (stl_iter == 0 || stl_iter == max_iter-1 || stl_iter % 5 == 0) ;
      if (print_stl) wrapper_io.WriteStl (stl_iter) ;
    }

    if (rank == 0) std::cout << "" << std::endl ;
  }

  PetscFinalize () ;

  if (rank == 0) std::cout << "...finished main. :D\n" << std::endl ;
}