PETSC_DIR = /home/carolina/petsc-3.7.7
PETSC_ARCH = arch-linux2-c-debug
COMP = g++
CFLAGS = -I.
FFLAGS =
CPPFLAGS = -std=c++14 -O3 -w
FPPFLAGS =
DFLAGS = -D SAVEMEMORY
LOCDIR =
EXAMPLESC =
EXAMPLESF =
MANSEC =
CLEANFILES =
NP =
EIGEN_DIR = ../../../Vendor
LSM_DIR = ../../lsm
FEA_DIR = ../../fea
IO_DIR = ../../wrapper_io
INCLUDEOTHER = -I. -I $(LSM_DIR)/include -I $(FEA_DIR)/include \
-I $(IO_DIR)/include -I ${PETSC_DIR}/${PETSC_ARCH}/include -I $(EIGEN_DIR)
LIBOTHER = -lglpk -lnlopt
LFLAGOTHER =

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/test

# Directory where you want your .o files stored
OBJ_DIR = ../bin

# List of object .o files
OBJ = $(OBJ_DIR)/levelset.o \
$(OBJ_DIR)/optimize.o \
$(OBJ_DIR)/grid_math.o \
$(OBJ_DIR)/boundary.o \
$(OBJ_DIR)/topopt.o \
$(OBJ_DIR)/linearelasticity.o \
$(OBJ_DIR)/topopt1d.o \
$(OBJ_DIR)/poisson.o \
$(OBJ_DIR)/wrapper.o \
$(OBJ_DIR)/initialize.o \
$(OBJ_DIR)/levelsetwrap.o \
$(OBJ_DIR)/optimizerwrap.o \
$(OBJ_DIR)/initialize_lsm.o \
$(OBJ_DIR)/physics_wrapper.o \
$(OBJ_DIR)/wrapper_io.o

# Compile all the object files
all: $(OBJ)

topopt: $(OBJ) main.o chkopts
	rm -rf topopt.out
	-${CLINKER} -O3 -std=c++14 ${INCLUDEOTHER} -o topopt.out $(OBJ) \
	main.o ${PETSC_SYS_LIB} ${LIBOTHER} ${RPATH}

main.o: main.cpp
	-${CLINKER} ${CPPFLAGS} -c -o main.o main.cpp -I ${PETSC_DIR}/include \
	${INCLUDEOTHER}

# Make commands for each individual object file
$(OBJ_DIR)/topopt.o: $(FEA_DIR)/src/topopt.cpp
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(OBJ_DIR)/topopt.o \
	$(FEA_DIR)/src/topopt.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/topopt1d.o: $(FEA_DIR)/src/topopt1d.cpp
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(OBJ_DIR)/topopt1d.o \
	$(FEA_DIR)/src/topopt1d.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/linearelasticity.o: $(FEA_DIR)/src/linear_elasticity.cpp
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(OBJ_DIR)/linearelasticity.o \
	$(FEA_DIR)/src/linear_elasticity.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/poisson.o: $(FEA_DIR)/src/poisson.cpp
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(OBJ_DIR)/poisson.o \
	$(FEA_DIR)/src/poisson.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/wrapper.o: $(FEA_DIR)/src/wrapper.cpp
	-${CLINKER} ${CPPFLAGS} -c -o $(OBJ_DIR)/wrapper.o $(FEA_DIR)/src/wrapper.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/physics_wrapper.o: $(FEA_DIR)/src/physics_wrapper.cpp
	-${CLINKER} ${CPPFLAGS} -c -o $(OBJ_DIR)/physics_wrapper.o \
	$(FEA_DIR)/src/physics_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/levelset.o: $(LSM_DIR)/src/lsm_3d.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/levelset.o $(LSM_DIR)/src/lsm_3d.cpp \
	${INCLUDEOTHER}

$(OBJ_DIR)/levelsetwrap.o: $(LSM_DIR)/src/level_set_wrapper.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/levelsetwrap.o \
	$(LSM_DIR)/src/level_set_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/optimizerwrap.o: $(LSM_DIR)/src/optimizer_wrapper.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/optimizerwrap.o \
	$(LSM_DIR)/src/optimizer_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/wrapper_io.o: $(IO_DIR)/src/wrapper_io.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/wrapper_io.o $(IO_DIR)/src/wrapper_io.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/optimize.o: $(LSM_DIR)/src/optimize.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/optimize.o $(LSM_DIR)/src/optimize.cpp \
	${INCLUDEOTHER}

$(OBJ_DIR)/boundary.o: $(LSM_DIR)/src/boundary.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/boundary.o $(LSM_DIR)/src/boundary.cpp \
	${INCLUDEOTHER}

$(OBJ_DIR)/grid_math.o: $(LSM_DIR)/src/grid_math.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/grid_math.o $(LSM_DIR)/src/grid_math.cpp \
	${INCLUDEOTHER}

$(OBJ_DIR)/initialize.o: initialize.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/initialize.o initialize.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(OBJ_DIR)/initialize_lsm.o: initialize_lsm.cpp
	$(COMP) $(CPPFLAGS) -c -o $(OBJ_DIR)/initialize_lsm.o initialize_lsm.cpp \
	${INCLUDEOTHER}


.PHONY: myclean

myclean:
	rm -rf *.o *.out

clean_obj:
	rm -f $(OBJ)