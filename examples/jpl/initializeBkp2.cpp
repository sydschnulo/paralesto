#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 96; nely = 96; nelz = 96;

  nlvls = 3;

  lx = 0.16; ly = 0.16; lz = 0.16;

  maxItr = 201;


  Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
  Kmax = 6.7; Kmin = 6.6e-3;
  volCons = 0.1;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});

  // loads.push_back(Force( 1*1.0e3, 1*1.0e3, 1*1.0e3,
  //                 8.0*a, ly/2.0, 2.0*lz/3.0,
  //                 eps, eps, eps));
  // //
  loads.push_back(Force( 1.0e3/4, 1.0e3/4, 1.0e3/4,
                  8.0*a, 16*b, lz/8.0 + 14*c,
                  eps, eps, eps));
  //
  loads.push_back(Force( 1.0e3/4, 1.0e3/4, 1.0e3/4,
                  8.0*a, ly - 16*b, lz/8.0 + 14*c,
                  eps, eps, eps));
  //
  loads.push_back(Force( 1.0e3/4, 1.0e3/4, 1.0e3/4,
                        lx/2.0 - 8.0*a, 16*b, lz - 14*c,
                        eps, eps, eps));
  //
  loads.push_back(Force( 1.0e3/4, 1.0e3/4, 1.0e3/4,
                        lx/2.0 - 8.0*a, ly - 16*b, lz - 14*c,
                        eps, eps, eps));
  //

  adjLoads.push_back(Force( 7.0e6/8.06/0.17, 0.0, -4.0e6/8.06/0.17,
                  8.0*a, 16*b, lz/8.0 + 14*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( 7.0e6/8.06/0.17, 0.0, -4.0e6/8.06/0.17,
                  8.0*a, ly - 16*b, lz/8.0 + 14*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( 7.0e6/8.06/0.17, 0.0, -4.0e6/8.06/0.17,
                        lx/2.0 - 8.0*a, 16*b, lz - 14*c,
                        eps, eps, eps));
  //
  adjLoads.push_back(Force( 7.0e6/8.06/0.17, 0.0, -4.0e6/8.06/0.17,
                        lx/2.0 - 8.0*a, ly - 16*b, lz - 14*c,
                        eps, eps, eps));
  //

  /*
  // Set-1
  adjLoads.push_back(Force( 7.0e6/8.06/0.17/2, 0.0, -4.0e6/8.06/0.17/2,
                  8.0*a-4*a, 16*b, lz/8.0 + 14*c - 7*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( 7.0e6/8.06/0.17/2, 0.0, -4.0e6/8.06/0.17/2,
                  8.0*a+4*a, 16*b, lz/8.0 + 14*c + 7*c,
                  eps, eps, eps));
  //

  // Set-2
  adjLoads.push_back(Force( 7.0e6/8.06/0.17/2, 0.0, -4.0e6/8.06/0.17/2,
                  8.0*a-4*a, ly - 16*b, lz/8.0 + 14*c - 7*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( 7.0e6/8.06/0.17/2, 0.0, -4.0e6/8.06/0.17/2,
                  8.0*a+4*a, ly - 16*b, lz/8.0 + 14*c + 7*c,
                  eps, eps, eps));
  //

  // Set-3
  adjLoads.push_back(Force( -7.0e6/8.06/0.17/2, 0.0, 4.0e6/8.06/0.17/2,
                  lx/2.0 - 8.0*a - 4*a, 16*b, lz - 14*c - 7*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( -7.0e6/8.06/0.17/2, 0.0, 4.0e6/8.06/0.17/2,
                  lx/2.0 - 8.0*a + 4*a, 16*b, lz - 14*c + 7*c,
                  eps, eps, eps));
  //

  // Set-4
  adjLoads.push_back(Force( -7.0e6/8.06/0.17/2, 0.0, 4.0e6/8.06/0.17/2,
                  lx/2.0 - 8.0*a - 4*a, ly - 16*b, lz - 14*c - 7*c,
                  eps, eps, eps));
  //
  adjLoads.push_back(Force( -7.0e6/8.06/0.17/2, 0.0, 4.0e6/8.06/0.17/2,
                  lx/2.0 - 8.0*a + 4*a, ly - 16*b, lz - 14*c + 7*c,
                  eps, eps, eps));
  //
  */

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  8*a, 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  8*a, ly - 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx-8*a, 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx-8*a, ly - 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //

  fixedHeatDof.push_back(HeatLoad( 25.0,
                  8.0*a, 16*b, lz/8.0 + 14*c,
                  4*a+eps, 8*b+eps, 4*c+eps));
  //
  fixedHeatDof.push_back(HeatLoad( 25.0,
                  8.0*a, ly - 16*b, lz/8.0 + 14*c,
                  4*a+eps, 8*b+eps, 4*c+eps));
  //
  fixedHeatDof.push_back(HeatLoad( 25.0,
                  lx/2.0 - 8.0*a, 16*b, lz - 14*c,
                  4*a+eps, 8*b+eps, 4*c+eps));
  //
  fixedHeatDof.push_back(HeatLoad( 25.0,
                  lx/2.0 - 8.0*a, ly - 16*b, lz - 14*c,
                  4*a+eps, 8*b+eps, 4*c+eps));
  //

  fixedHeatDof.push_back(HeatLoad(-5,
                  8*a, 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //
  fixedHeatDof.push_back(HeatLoad(-5,
                  8*a, ly - 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //

  fixedHeatDof.push_back(HeatLoad(-5,
                  lx-8*a, 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //
  fixedHeatDof.push_back(HeatLoad(-5,
                  lx-8*a, ly - 16*b, 0.0,
                  8*a+eps, 8*b+eps, eps));
  //


  static PARA_LSM::CutPlane cutPlane1(-7.0/4.0, 0.0, 1.0, -nelz/8.0, -1.0e5);
  static PARA_LSM::CutPlane cutPlane2(0.5, 0.0, 1.0, -1.25*nelz, -1.0e5);
  static PARA_LSM::CutPlane cutPlane3(2.0, 0.0, 1.0, -19*nelz/8.0, -1.0e5);

  initialVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane1));
  initialVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane2));
  initialVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane3));
  domainVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane1));
  domainVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane2));
  domainVoids.push_back(std::make_unique<PARA_LSM::CutPlane> (cutPlane3));

  double hFix3 = 8.0;
  double hFix4 = 4.0;
  static PARA_LSM::Cuboid cube0(hFix3, 2*hFix3, 0,
              hFix3,  hFix3, hFix4);
  //////
  static PARA_LSM::Cuboid cube1(hFix3, nely - 2*hFix3, 0,
              hFix3,  hFix3, hFix4);
  //////
  static PARA_LSM::Cuboid cube2(nelx-hFix3, 2*hFix3, 0,
              hFix3,  hFix3, hFix4);
  //////
  static PARA_LSM::Cuboid cube3(nelx-hFix3, nely - 2*hFix3, 0,
              hFix3,  hFix3, hFix4);
  //////

  static PARA_LSM::Cuboid cube5(hFix3, 2*hFix3, nelz/8.0 + 14.0,
              hFix3,  hFix3, hFix4);
  cube5.theta = std::atan2(7.0,4.0);
  cube5.rotAxis[1] = 1.0;
  cube5.rotOrigin[0] = hFix3;
  cube5.rotOrigin[2] = nelz/8.0 + 14.0;
  //////
  static PARA_LSM::Cuboid cube6(hFix3, nely - 2*hFix3, nelz/8.0 + 14.0,
              hFix3,  hFix3, hFix4);
  cube6.theta = std::atan2(7.0,4.0);
  cube6.rotAxis[1] = 1.0;
  cube6.rotOrigin[0] = hFix3;
  cube6.rotOrigin[2] = nelz/8.0 + 14.0;
  //////
  static PARA_LSM::Cuboid cube7(nelx/2 - hFix3, nely - 2*hFix3, nelz - 14.0,
              hFix3,  hFix3, hFix4);
  cube7.theta = std::atan2(7.0,4.0);
  cube7.rotAxis[1] = 1.0;
  cube7.rotOrigin[0] = nelx/2 - hFix3;
  cube7.rotOrigin[2] = nelz - 14.0;
  //////
  static PARA_LSM::Cuboid cube8(nelx/2 - hFix3, 2*hFix3, nelz - 14.0,
              hFix3,  hFix3, hFix4);
  cube8.theta = std::atan2(7.0,4.0);
  cube8.rotAxis[1] = 1.0;
  cube8.rotOrigin[0] = nelx/2 - hFix3;
  cube8.rotOrigin[2] = nelz - 14.0;
  //////

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube5));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube6));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube7));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube8));

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube3));


}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

FixedDof::FixedDof() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA