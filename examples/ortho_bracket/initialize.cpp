#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 100; nely = 100; nelz = 100;

  nlvls = 3;

  lx = 2.0; ly = 2.0; lz = 2.0;

  maxItr = 101;


  Emax = 1.0; Emin = 1.0e-9; nu = 0.3;
  volCons = 0.15;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  loads.push_back(Force( 0.0, 0.0, 1.0e-4,
                  lx - 2*a, 2*b, 0.4*lz - 2*c,
                  2.0*a+eps, 2.0*b+eps, 2.0*c+eps));
  //
  loads.push_back(Force( 0.0, 0.0, 1.0e-4,
                  lx - 2*a, 0.4*ly - 2*b, 0.4*lz - 2*c,
                  2.0*a+eps, 2.0*b+eps, 2.0*c+eps));
  //
  loads.push_back(Force( 0.0, 0.0, 1.0e-4,
                  2*a, ly - 2*b, 0.4*lz - 2*c,
                  2.0*a+eps, 2.0*b+eps, 2.0*c+eps));
  //
  loads.push_back(Force( 0.0, 0.0, 1.0e-4,
                  0.4*lx - 2*a, ly - 2*b, 0.4*lz - 2*c,
                  2.0*a+eps, 2.0*b+eps, 2.0*c+eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 0.0*ly, 1.0*lz,
                  0.4*lx+eps, 0.4*ly+eps, eps));
  //
  // Fixed Blobs
  double hFix = 5.0;
  static PARA_LSM::Cuboid cube0(1*nelx-hFix, 0*nely + hFix, 0.4*nelz-hFix,
              hFix,  hFix, hFix);
  static PARA_LSM::Cuboid cube1(1*nelx-hFix, 0.4*nely - hFix, 0.4*nelz-hFix,
              hFix,  hFix, hFix);
  static PARA_LSM::Cuboid cube2(0*nelx+hFix, 1*nely -hFix, 0.4*nelz-hFix,
              hFix,  hFix, hFix);
  static PARA_LSM::Cuboid cube3(0.4*nelx-hFix, 1.0*nely - hFix, 0.4*nelz-hFix,
             hFix,  hFix, hFix);
  //
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube3));



  static PARA_LSM::Cuboid hole0(0.7*nelx, 0.7*nely, 0.0*nelz,
              0.3*nelx,  0.3*nely, 10.0*nelz);

  static PARA_LSM::Cuboid hole1(0.7*nelx, 0.0*nely, 0.7*nelz,
              0.3*nelx,  10.0*nely, 0.3*nelz);

  static PARA_LSM::Cuboid hole2(0.0*nelx, 0.7*nely, 0.7*nelz,
              10.0*nelx,  0.3*nely, 0.3*nelz);


  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole0));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole1));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole2));

  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole0));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole1));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (hole2));

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

}