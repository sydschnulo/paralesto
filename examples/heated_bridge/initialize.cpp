#include "initialize.h"

namespace PARA_FEA {

InitializeOpt :: InitializeOpt () {
  // mesh discretization details
  nelx = 128 ; nely = 64 ; nelz = 64 ;
  // nelx = 64 ; nely = 28 ; nelz = 28 ;

  // number of multigrid levels
  nlvls = 4 ;
  // nlvls = 3 ;

  // physical dimensions of design domain
  lx = 2.0 ; ly = 1.0 ; lz = 1.0 ;

  // maximum iterations
  // maxItr = 101 ;
  maxItr = 10 ;

  // Elastic moduli of material and void, and poisssons ratio
  Emax = 1.0 ; Emin = 1.0e-9 ; nu = 0.3 ;

  //Conductivity coefficient
  Kmax = 6.7 ; Kmin = 6.7*0.005 ;

  // volume constraint in fraction of the total domain volume
  volCons = 0.15 ;

  // some constants
  double a    = lx/nelx ;
  double b    = ly/nely ;
  double c    = lz/nelz ;
  double eps  = 0.1*std::min ({a,b,c}) ; //small value
  double infi = 10.0*std::max ({lx,ly,lz}) ;

  // apply loads on the top
  loads.push_back (Force (0.0, 0.0, -0.001, /*Fx, Fy, Fz*/
                   0.5*lx, 0.5*ly, lz, /*x,y,z location of the force*/
                   infi, infi, eps /*tolerance on the nodes where force is applied*/
                  )) ;

  // Boundary condition on the bottom-left corner
  fixedDofs.push_back (FixedDof (0, 0, 0, /*DOF of node: 0 = fixed; 1 = Free*/
                       0.0*lx, 0.0*ly, 0.0*lz, /*x,y,z location of the node*/
                       3*a, infi, eps /*tolerance on the nodes*/
                      )) ;

  // Symmetry boundary condition on the right side
  fixedDofs.push_back (FixedDof (0, 1, 1, /*DOF of node: 0 = fixed; 1 = Free*/
                       lx, 0.0*ly, 0.0*lz, /*x,y,z location of the node*/
                       eps, infi, infi /*tolerance on the nodes*/
                      )) ;

  fixedHeatDof.push_back (HeatLoad (0.0,
                          0.0*lx, 0.0*ly, 0.0*lz, /*x,y,z location of the node*/
                          3*a, infi, eps /*tolerance on the nodes*/
                         )) ;

  heatLoad.push_back (HeatLoad (1.0e-2,
                                0.5*lx, 0.5*ly, lz, /*x,y,z location of the force*/
                                infi, infi, eps /*tolerance on the nodes where force is applied*/
                               )) ;

  // Fix a cubeoid (the region on top)
  static PARA_LSM::Cuboid cube0 (nelx/2.0, nely/2.0, nelz-2.0, /*Cubeoid center*/
  			               nelx/2.0, nely/2.0, 2.0) ; /*half of the width of the cubeoid*/
  fixedBlobs.push_back (std::make_unique<PARA_LSM::Cuboid> (cube0)) ;
}


Force :: Force () : valx (0), valy (0), valz (0), x (0), y (0), z (0), tolx (0),
    toly (0), tolz (0) {
}


Force :: Force (double valx_, double valy_, double valz_, double x_, double y_,
    double z_, double tolx_, double toly_, double tolz_) : valx (valx_),
    valy (valy_), valz (valz_), x (x_), y (y_), z (z_), tolx (tolx_),
    toly (toly_), tolz (tolz_) {
}


FixedDof :: FixedDof (bool valx_, bool valy_, bool valz_, double x_, double y_,
    double z_, double tolx_, double toly_, double tolz_) : valx (valx_),
    valy (valy_), valz (valz_), x (x_), y (y_), z (z_), tolx (tolx_),
    toly (toly_), tolz (tolz_) {
}


HeatLoad :: HeatLoad () : val (0), x (0), y (0), z (0), tolx (0), toly (0),
    tolz (0) {
}


HeatLoad :: HeatLoad (double val_, double x_, double y_, double z_, double tolx_,
    double toly_, double tolz_) : val (val_), x (x_), y (y_), z (z_),
    tolx (tolx_), toly(toly_), tolz(tolz_) {
}

} // namespace PARA_FEA