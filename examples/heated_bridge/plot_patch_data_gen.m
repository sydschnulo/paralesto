clear;
close all;

% mesh dimensions
nx = 128;
ny = 64;
nz = 64;

% load vertices
vertices = load('patchfile.txt');
colors = load('temp.txt');

% load the vertices and colors as fv structure
fv.vertices = vertices;

num_faces = length(fv.vertices(:,1))/3;
fv.faces = zeros(num_faces,3);
fv.faces(:,1) = [1:3:3*num_faces];
fv.faces(:,2) = [2:3:3*num_faces];
fv.faces(:,3) = [3:3:3*num_faces];

%plot
figure(1);
set(gcf,'color','w');
h = patch(fv,'FaceVertexCData',colors,'FaceColor','flat','EdgeColor','none');
colormap(hot);
%caxis([15 50])
colorbar;
axis equal;
axis off;
axis([0 nx 0 ny 0 nz]);
lighting gouraud;
h.AmbientStrength = 0.3;
h.DiffuseStrength = 0.8;
h.SpecularStrength = 0.9;
h.SpecularExponent = 25;
h.BackFaceLighting = 'unlit';
%box on;
view([25 , 25])
camlight(45,15);
camlight(225,-15);
figure(1);
hold off;


