#include <mpi.h>
#include <omp.h>
#include <petsc.h>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

typedef std::numeric_limits<double> double_limit ; // TODO: delete after testing

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";

int main (int argc, char *argv[]) {
	//===========================================================================
	// Set up Topopt and LinearElasticity classes
	// Error code for debugging
	PetscErrorCode ierr ;

	// Initialize PETSc / MPI and pass input arguments to PETSc
	PetscInitialize (&argc,&argv,PETSC_NULL,help) ;

	// declare petsc int here for rank and size
	PetscMPIInt rank, size ;

	// define rank and size here
	MPI_Comm_rank (PETSC_COMM_WORLD, &rank) ;
	MPI_Comm_size (PETSC_COMM_WORLD, &size) ;

	// Initialize the input file
	fea::InitializeOpt myInit ;

	// Declare linear elasticity and poisson objects
	fea::TopOpt *opt = new fea::TopOpt (myInit) ;
	fea::LinearElasticity *physics = new fea::LinearElasticity (opt, myInit) ;

	fea::TopOpt1D *optHeat = new fea::TopOpt1D (myInit) ;
	fea::Poisson *physicsHeat = new fea::Poisson (optHeat, myInit) ;



	//============================================================================
	// Define  and setup level set parameters

	//mesh size
	int nelx = opt->nxyz[0]-1 ;
	int nely = opt->nxyz[1]-1 ;
	int nelz = opt->nxyz[2]-1 ;

	// size of volumeFraction vector
	int nElements = nelx * nely * nelz ;

	// initialize vector that maps ls grid to petsc grid
	std::vector<int> volfrac_to_petsc_map (1,0) ;

	// Make a box in openvdb
	uint box_x = nelx ;
	uint box_y = nely ;
	uint box_z = nelz ;

	// Define the grid
	lsm::Grid grid ((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz) ;
	//  Create an object of the levelset class
	lsm::LevelSet3D level_set_3d (grid) ;
	// create boundary
	lsm::Boundary boundary (level_set_3d) ;

	// std::vector<Cuboid> cubes;
	// Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
	// 						nelx/2.0,  nely/2.0, 2.0);
	// cubes.push_back(cube0);
	// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
	level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs) ;
	level_set_3d.domainVoids = std::move (myInit.domainVoids) ;
	level_set_3d.initialVoids = std::move (myInit.initialVoids) ;

	std::ofstream txtfile_iterhist ; //io

	// define level set functions on processor number 1
	if (rank == 0) {
		level_set_3d.hWidth = 4 ;

		level_set_3d.MakeBox () ;
		level_set_3d.volFractions.resize (level_set_3d.nCells, 1.0) ;

		txtfile_iterhist.open ("iterhist.txt") ;

		#if 0
		std::ifstream infile;
		infile.open("SDatgridpointsInp.txt");
		for(int k = 0; k <= nelz; k ++){
	     for(int j = 0; j <= nely; j ++){
	       for(int i = 0; i <= nelx; i ++){
					 double tempval;
					 infile >> tempval;
					  level_set_3d.phi[level_set_3d.GridPtToIndex(i,j,k)].val = tempval;
				 }//i
			 }//j
		 }//k
		 #endif
	}//rank

	MPI_Barrier (PETSC_COMM_WORLD) ;

	// actual volume fractions that are sent to petsc
	std::vector<double> volumefraction_vector_for_map (1,0.2) ;
	if (rank == 0) {
		volumefraction_vector_for_map.resize (nElements,0.2) ;
	}

	MPI_Barrier (PETSC_COMM_WORLD) ;

	// Initialize walltime
	double walltime1, walltime2 ;

	// Create a map between ls and petsc here
	PetscInt ni = nElements ;

	// Form a map between volume fractions on level set on rank 0
	// and petsc mesh
	fea::Wrapper wrapper ;
	wrapper.GetMap (opt, volfrac_to_petsc_map, nElements) ;

  // TODO: delete after testing
  std::string dir = "/home/carolina/Projects/paralesto/test_modular/ref_output/" ;

  // TODO: delete after testing
  std::ofstream stiff_file (dir + "element_stiffness.txt") ;
  stiff_file.precision (double_limit::max_digits10) ;
  for (int i = 0 ; i < 24*24 ; i++) {stiff_file << physics->KE[i] << "\n" ;}
  stiff_file.close () ;

	// Optimization loop
	PetscInt itr = 0 ;
  double iter_start_time ;
	while (itr < opt->maxItr) {
		iter_start_time = MPI_Wtime () ;
		// Update iteration counter
		itr++ ;

		if (rank == 0) {
			//=================================================================
			// Compute boundary
			boundary.MarchCubes () ;
			std::cout << "Finished boundary computation." << std::endl ;
			// boundary.WriteSTL();
			// level_set_3d.WriteSD();
			// return 0;
			//=================================================================

			//=================================================================
			// Compute volume fractions and pass them to petsc
			level_set_3d.ComputeVolumeFractions () ;

      // TODO: delete after testing
      std::ofstream v_file (dir + "volfrac_ref" + std::to_string (itr) + ".txt") ;
      v_file.precision (double_limit::max_digits10) ;

			// assign volume fractions to petsc mesh
			// step-1
			double count_rigid = 0 ;
			for (int k = 0 ; k < nelz ; k++) {
				for (int j = 0 ; j < nely ; j++) {
					for (int i = 0 ; i < nelx ; i++) {
						int countFEM = i + j*nelx + k*nelx*nely ;

						volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]] =
              std::max (level_set_3d.volFractions[countFEM],0.0) ;

            // TODO: delete after testing
            v_file << level_set_3d.volFractions[countFEM] << "\n" ;
					}
				}
			}
      v_file.close () ; // TODO: delete after testing
			// step-2
			for (int countFEM = 0 ; countFEM < nElements ; countFEM++) {
				VecSetValue (opt->x, countFEM, volumefraction_vector_for_map[countFEM],
          INSERT_VALUES) ;
				VecSetValue (optHeat->x, countFEM, volumefraction_vector_for_map[countFEM],
          INSERT_VALUES) ;
			}

			std::cout << "Assigned volume fractions." << std::endl ;
			//=================================================================
		}

    VecAssemblyBegin (opt->x) ;	VecAssemblyEnd (opt->x) ;
		VecAssemblyBegin (optHeat->x) ;	VecAssemblyEnd (optHeat->x) ;

		MPI_Barrier (PETSC_COMM_WORLD) ;
		//==========================================================================
		// compute sensitivities
		physics->myRTOL = 1.0e-5 ;
		ierr = physics->ComputeSensitivities (opt) ; CHKERRQ (ierr) ;
		// gather senstitivities on to the zeroeth processor
		fea::MyGather mygather (opt->dfdx) ;
		double compliance = opt->fx ;
		//==========================================================================
		// Compute sensitivities for the other load case
		physicsHeat->myRTOL = 1.0e-6 ;
		ierr = physicsHeat->ComputeSensitivities (optHeat) ; CHKERRQ (ierr) ;
		fea::MyGather mygatherTh (optHeat->dfdx) ;
		double th_compliance = optHeat->fx ;

		// Compute temperatures
		ierr = physicsHeat->ComputeCentroidTemperatures (optHeat) ; CHKERRQ (ierr) ;
		fea::MyGather mygatherTemperature (optHeat->DeltaTVec) ;

    // TODO: delete after testing
    // Print state variables
    int num_nodes = (nelx+1)*(nely+1)*(nelz+1) ;
    std::vector<int> disp_to_petsc_map (1, 0) ;
    std::vector<int> temp_to_petsc_map (1, 0) ;
    fea::petsc_map::GetMap (opt->da_nodes, disp_to_petsc_map, num_nodes*3) ;
    fea::petsc_map::GetMap (optHeat->da_nodes, temp_to_petsc_map, num_nodes) ;
    fea::MyGather gather_disp (physics->U) ;
    fea::MyGather gather_temp (physicsHeat->U) ;
    fea::MyGather gather_force (physics->RHS) ;
    if (rank == 0) {
      std::ofstream disp_file (dir + "disp" + std::to_string(itr) + ".txt") ;
      disp_file.precision (double_limit::max_digits10) ;
      std::ofstream temp_file (dir + "temp" + std::to_string(itr) + ".txt") ;
      temp_file.precision (double_limit::max_digits10) ;
      std::ofstream force_file (dir + "force" + std::to_string(itr) + ".txt") ;
      force_file.precision (double_limit::max_digits10) ;
      for (int i = 0 ; i < num_nodes*3 ; i++) {
        int disp_index = disp_to_petsc_map[i] ;
        disp_file << gather_disp.sensiArray[disp_index] << "\n" ;
        force_file << gather_force.sensiArray[disp_index] << "\n" ;

        if (i%3 == 0) {
          int temp_index = temp_to_petsc_map[i/3] ;
          temp_file << gather_temp.sensiArray[temp_index] << "\n" ;
        }
      }
      disp_file.close () ;
      temp_file.close () ;
      force_file.close () ;

      // TODO: delete after testing
      PetscViewer viewer ;
      PetscViewerCreate (PETSC_COMM_WORLD, &viewer) ;
      // PetscViewerSetType (viewer, PETSCVIEWERASCII) ;
      PetscViewerSetType (viewer, PETSCVIEWERBINARY) ;
      PetscViewerFileSetMode (viewer, FILE_MODE_WRITE) ;
      // PetscViewerASCIIOpen (PETSC_COMM_WORLD,
      //   "/home/carolina/Projects/paralesto/test_modular/ref_output/kmat.txt",
      //   &viewer) ;
      PetscViewerBinaryOpen (PETSC_COMM_WORLD,
        "/home/carolina/Projects/paralesto/test_modular/ref_output/kmat.mat",
        FILE_MODE_WRITE, &viewer) ;
      MatView (physics->K, viewer) ;
      PetscViewerBinaryOpen (PETSC_COMM_WORLD,
        "/home/carolina/Projects/paralesto/test_modular/ref_output/kcmat.mat",
        FILE_MODE_WRITE, &viewer) ;
      MatView (physicsHeat->K, viewer) ;
    }
		//=====================================================================
    // Leasst squares, optimization, and advection on rank 0
    if(rank == 0) {
      //===========================================================================
      // Interpolate sensitivities
      std::vector<double> comSens (boundary.numTriangles, 0.0) ;
      boundary.InterpolateBoundarySensitivities (comSens, mygather.sensiArray,
        volfrac_to_petsc_map, 2) ;

      std::vector<double> thSens (boundary.numTriangles, 0.0) ;
      boundary.InterpolateBoundarySensitivities (thSens, mygatherTh.sensiArray,
        volfrac_to_petsc_map, 2) ;
      double k_mech = 1.0 ;
      double k_temp = 1.0 ;
      for (int i = 0 ; i< comSens.size () ; i++) {
        comSens[i] =  k_mech*comSens[i] + k_temp*thSens[i] ;
      }
      std::cout << "Completed sensitivity interpolation." << std::endl ;
      //===========================================================================
      // Interpolate tempaerature
      std::vector<double> bTemp (boundary.numTriangles, 0.0) ;
      boundary.InterpolateBoundarySensitivities (bTemp, mygatherTemperature.sensiArray, volfrac_to_petsc_map, 2.0, 0) ;
      //===========================================================================
			// Optimize boundary velocities
			double move_limit = 1.0 ;
			if (itr > 50) move_limit = 0.75 ;
			if (itr > 100) move_limit = 0.5 ;

		 	std::vector<double> volSens (boundary.numTriangles, 1.0) ;

			// constraints
			int nCons = 1 ;
			std::vector<std::vector<double>> conSens ;
			conSens.resize (nCons) ;
			std::vector<double> conMaxVals (nCons) ;
			std::vector<double> conCurVals (nCons) ;
			// volume constraint
			conSens[0] = volSens ;
			conMaxVals[0] = myInit.volCons*nelx*nely*nelz ;
			conCurVals[0] = level_set_3d.volume ;

      boundary.Optimize (comSens, conSens, conMaxVals, conCurVals, move_limit, 0) ;

      std::cout << "Completed boundary velocities optimization. " << std::endl ;
      //========================================================
      // output temperature every now and then
      if (fmod (itr, 2) == 1) {
        std::ofstream txtfile_temp ;
        txtfile_temp.open ("temp.txt") ;
        for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++) {
          txtfile_temp << bTemp[i1] << std::endl ;
        }
        txtfile_temp.close () ;
        boundary.WritePatch () ;
      }
      //========================================================
      // Update boundary
      boundary.ExtrapolateVelocities () ;
      level_set_3d.Update () ;
      level_set_3d.MakeDomainHoles () ;


      //========================================================
      double iter_end_time = MPI_Wtime () ;
      double iter_time = iter_end_time - iter_start_time ;
      std::cout << "Completed update. Iteration time = " << iter_time <<
        "\n" << std::endl ;

			// Print out stl
      boundary.WriteSTL("TopAfterIter.stl") ;
      if (fmod (itr, 10) == 1) {
        boundary.WriteSTL("TopAfterIter" + std::to_string (itr) + ".stl") ;
        // level_set_3d.WriteSD();
      }
      std::cout << "Iter = " << itr << "; Vol Cons = " <<
        level_set_3d.volume / nelx / nely / nelz << "; Compliance = " <<
        compliance << "; ThermalCompliance = " << th_compliance<< "\n"<< std::endl ;
		}
		//==========================================================================
		MPI_Barrier (PETSC_COMM_WORLD) ;
	}

  if (rank == 0) txtfile_iterhist.close () ;

	delete opt ;
	delete physics ;

	// Finalize PETSc / MPI
	PetscFinalize () ;
	return 0 ;
}