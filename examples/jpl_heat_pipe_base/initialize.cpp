#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(bool isAux, bool isVapor){

  nelx = 1000; nely = 24; nelz = 24;

    nlvls = 4;

    lx = 1.0; ly = 0.024; lz = 0.024;

    maxItr = 1;

    Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;

    volCons = 0.45;

    double a = lx/nelx;
    double b = ly/nely;
    double c = lz/nelz;
    double eps = 0.1*std::min({a,b,c});//small value
    double infi = 10.0*std::max({lx,ly,lz});

    double holeDiam = 6.0*a;
    Kmax = (holeDiam*holeDiam)/32/8.5e-6; Kmin = Kmax*1.0e-6;


    fixedHeatDof.push_back(HeatLoad( 0.0,
                    0*a, 0*b, lz,
                    10*a+eps, infi, 0.75*lz+1));
    //

    heatLoad.push_back(HeatLoad( 0.0,
                    lx, ly, lz,
                    25*a+eps, 25*b+eps, eps));
    //

}


InitializeOpt::InitializeOpt(bool isAux){

  nelx = 1000; nely = 24; nelz = 24;

    nlvls = 4;

    lx = 1.0; ly = 0.024; lz = 0.024;

    maxItr = 1;

    Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
    Kmax = 2.97e-11/8.9e-4; Kmin = 2.97e-15/8.9e-4;
    volCons = 0.45;

    double a = lx/nelx;
    double b = ly/nely;
    double c = lz/nelz;
    double eps = 0.1*std::min({a,b,c});//small value
    double infi = 10.0*std::max({lx,ly,lz});


    fixedHeatDof.push_back(HeatLoad( 0.0,
                    0*a, 0*b, 0*c,
                    eps, infi, infi));
    //

    heatLoad.push_back(HeatLoad( 0.0,
                    lx, 0.0*ly, 0.0*lz,
                    eps, ly+eps, lz+eps));
    //

}

InitializeOpt::InitializeOpt(){

  #if 1
  nelx = 1000; nely = 24; nelz = 24;

    nlvls = 4;

    lx = 1.0; ly = 0.024; lz = 0.024;

    maxItr = 1;

    Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
    Kmax = 108; Kmin = 0.01;
    volCons = 0.15;

    double a = lx/nelx;
    double b = ly/nely;
    double c = lz/nelz;
    double eps = 0.1*std::min({a,b,c});//small value
    double infi = 10.0*std::max({lx,ly,lz});


    // fixedHeatDof.push_back(HeatLoad( 20.0,
    //                 0*a, 0*b, 0*c,
    //                 eps, infi, infi));
    // //


    fixedHeatDof.push_back(HeatLoad( 20.0,
                    lx, ly/2, 0.0*lz,
                    320*a+eps, ly+eps, eps));
    //
    fixedHeatDof.push_back(HeatLoad( 20.0,
                    lx, ly/2, 1.0*lz,
                    320*a+eps, ly+eps, eps));
    //
    fixedHeatDof.push_back(HeatLoad( 20.0,
                    lx, 0*ly, 0.5*lz,
                    320*a+eps, eps, lz+eps));
    //
    fixedHeatDof.push_back(HeatLoad( 20.0,
                    lx, 1*ly, 0.5*lz,
                    320*a+eps, eps, lz+eps));
    //

    // heatLoad.push_back(HeatLoad( 10.0/(5*81.0*81.0),
    //                 lx, 0.0*ly, 0.0*lz,
    //                 eps, ly+eps, lz+eps));
    // //

    heatLoad.push_back(HeatLoad( 100.0/(4.0*31.0*25.0),
                    0, ly/2, 0.0*lz,
                    30*a+eps, ly+eps, eps));
    //
    heatLoad.push_back(HeatLoad( 100.0/(4.0*31.0*25.0),
                    0, ly/2, 1.0*lz,
                    30*a+eps, ly+eps, eps));
    //
    heatLoad.push_back(HeatLoad( 100.0/(4.0*31.0*25.0),
                    0, 0*ly, 0.5*lz,
                    30*a+eps, eps, lz+eps));
    //
    heatLoad.push_back(HeatLoad( 100.0/(4.0*31.0*25.0),
                    0, 1*ly, 0.5*lz,
                    30*a+eps, eps, lz+eps));
    //

    double q3 = 250;
    heatLoad.push_back(HeatLoad( q3/(4.0*31.0*25.0),
                    0.3*lx, ly/2, 0.0*lz,
                    15*a+eps, ly+eps, eps));
    //
    heatLoad.push_back(HeatLoad( q3/(4.0*31.0*25.0),
                    0.3*lx, ly/2, 1.0*lz,
                    15*a+eps, ly+eps, eps));
    //
    heatLoad.push_back(HeatLoad( q3/(4.0*31.0*25.0),
                    0.3*lx, 0*ly, 0.5*lz,
                    15*a+eps, eps, lz+eps));
    //
    heatLoad.push_back(HeatLoad( q3/(4.0*31.0*25.0),
                    0.3*lx, 1*ly, 0.5*lz,
                    15*a+eps, eps, lz+eps));
    //

    // intial
    #if 0
    double hWidthCube = 3;
    static std::vector<PARA_LSM::Cuboid> holesVector;
    static std::vector<Cylinder> holesVectorCylinder;
    static std::vector<PARA_LSM::Cuboid> domainHolesVectorCylinder;
    for(int j = 8; j <= nely; j=j+16){
        for(int k = 8; k <= nelz; k=k+16){
          PARA_LSM::Cuboid hole1(0.0*nelx, j, k,
                      2*nelx,  hWidthCube, hWidthCube);
          //
          PARA_LSM::Cylinder cHole1(0.5*nelx, j, k,
                      0.4*nelx,  hWidthCube, 0, 0);
          //
          holesVector.push_back(hole1);
          holesVectorCylinder.push_back(cHole1);

          PARA_LSM::Cylinder cHole2(0.7*nelx, j, k,
                      0.2*nelx,  hWidthCube, 0, 0);
          //
          holesVectorCylinder.push_back(cHole2);
        }
    }

    for(int i = 8; i < nelx/2; i=i+16){
        for(int k = 8; k <= nelz; k=k+16){
          PARA_LSM::Cuboid hole1(i, 0, k,
                      hWidthCube,  2*nely, hWidthCube);
          //
          PARA_LSM::Cylinder cHole1(i, 0, k,
                      2*nely,  hWidthCube, 0, 1);
          //
          holesVector.push_back(hole1);
          holesVectorCylinder.push_back(cHole1);

          if(i >= nelx/2){
            PARA_LSM::Cylinder cHole2(i, 0, k,
                            2*nely,  hWidthCube, 0, 1);
            //
            holesVectorCylinder.push_back(cHole2);
          }

        }
    }

    for(int i = 8; i < nelx; i=i+16){
        for(int j = 8; j <= nely; j=j+16){

          PARA_LSM::Cuboid hole1(i, j, 0,
                      hWidthCube, hWidthCube, 2*nelz);
          //
          PARA_LSM::Cylinder cHole1(i, j, 0,
                      2*nelz,  hWidthCube, 0, 2);
          //
          holesVector.push_back(hole1);
          holesVectorCylinder.push_back(cHole1);

          if(i >= nelx/2){
            PARA_LSM::Cylinder cHole2(i, j, 0,
                            2*nelz,  hWidthCube, 0, 2);
            //
            holesVectorCylinder.push_back(cHole2);
          }


        }
    }

    for(int i = 0; i < holesVector.size(); i++){
        // initialVoids.push_back(&holesVector[i]);
    }
    for(int i = 0; i < holesVectorCylinder.size(); i++){
      domainVoids.push_back(&holesVectorCylinder[i]);
      initialVoids.push_back(&holesVectorCylinder[i]);
    }

    static PARA_LSM::Cuboid cube0(0, 0, nelz,
                4,  nely, 0.75*nelz);
    //////
    static PARA_LSM::Cuboid cube1(0.75*nelx, nely, 0.25*nelz,
                0.25*nelx,  4, 0.25*nelx);
    //////
    static PARA_LSM::Cuboid cube2(0, 0, 0,
                0.25*nelx-2, nely, 0.25*nelz-2);
    //////
    static PARA_LSM::Cuboid cube3(nelx, nely, nelz,
                0.5*nelx-2, nely, 0.5*nelz-2);
    //////


    fixedBlobs.push_back(std::move(&cube0));
    fixedBlobs.push_back(std::move(&cube1));

    initialVoids.push_back(std::move(&cube2));
    initialVoids.push_back(std::move(&cube3));

    // domainVoids.push_back(std::move(&cube2));
    // domainVoids.push_back(std::move(&cube3));

    #else

    static PARA_LSM::Cuboid cube0(3*nelx/4, 0, nelz,
                nelx/2,  nely, nelz/2);
    //////
    static PARA_LSM::Cuboid cube1(nelx/8, nely/2, nelz/2,
                nelx/8-10,  nely/2-10, nelz/2-10);
    //////
    static PARA_LSM::Cuboid cube2(nelx/2, nely/2, nelz/4,
                nelx/2-10,  nely/2-10, nelz/4-10);
    //////
    static PARA_LSM::Cylinder cHole1(nelx/2, nely/2, nelz/2,
                nelx/2-5.0,  nely/2-2.0, 0, 0);
    //
    // initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
    // initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
    // initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
    initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cHole1));
    #endif



    #else

    nelx = 128; nely = 32; nelz = 32;

      nlvls = 4;

      lx = 0.08; ly = 0.02; lz = 0.02;

      maxItr = 1;

      Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
      Kmax = 205; Kmin = 0.01;
      volCons = 0.45;

      double a = lx/nelx;
      double b = ly/nely;
      double c = lz/nelz;
      double eps = 0.1*std::min({a,b,c});//small value
      double infi = 10.0*std::max({lx,ly,lz});


      fixedHeatDof.push_back(HeatLoad( 150.0,
                      0*a, ly/2.0, lz/2.0,
                      eps, infi, infi));
      //

      heatLoad.push_back(HeatLoad( 0.0,
                      lx, ly/2.0, lz/2.0,
                      eps, 5*b+eps, 5*c+eps));
      //

      static PARA_LSM::Cylinder cylinderOuter(0.5*nelx, 0.5*nely, 0.5*nelz,
                  nelx/2, nely/2, 0, 0);
      //
      cylinderOuter.flipDistanceSign = -1;

      static PARA_LSM::Cylinder cylinderInner(0.5*nelx, 0.5*nely, 0.5*nelz,
                  nelx/2-5, nely/2-5, 0, 0);
      //

      initialVoids.push_back(&cylinderOuter);
      // initialVoids.push_back(&cylinderInner);

    #endif
}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

FixedDof::FixedDof() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA