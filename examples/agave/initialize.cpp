#include "initialize.h"

namespace PARA_FEA {

InitializeOpt :: InitializeOpt () {
  // mesh discretization details
  nelx = 160 ; nely = 160 ; nelz = 160 ;

  // number of multigrid levels
  nlvls = 3 ;

  // physical dimensions of design domain
  lx = 1.0 ; ly = 1.0 ; lz = 1.0 ;

  // maximum iterations
  maxItr = 201 ;

  // Elastic moduli of material and void, and poisssons ratio
  Emax = 113.8e9 ; Emin = 1.0e-9*Emax ; nu = 0.342 ;

  //Conductivity coefficient
  Kmax = 6.7 ; Kmin = 6.7*0.005 ;

  // volume constraint in fraction of the total domain volume
  volCons = 0.1 ;

  // some constants
  double a = lx/nelx ;
  double b = ly/nely ;
  double c = lz/nelz ;
  double eps = 0.1*std::min ({a,b,c}) ;//small value
  double infi = 10.0*std::max ({lx,ly,lz}) ;

  // apply loads
  fixedHeatDof.push_back (HeatLoad (0.0,
                  lx/2.0, ly/2.0, 0.0*c, // x,y,z location of the node
                  lx/20.0+eps, ly/20.0+eps, 1*c+eps // tolerance on the nodes
                  )) ;

  // apply loads
  heatLoad.push_back (HeatLoad (1.0e-6,
                  0*a, 0*b, lz, // x,y,z location of the node
                  infi, infi, lz-(2*c+eps))) ; // tolerance on the nodes

  static std::vector<PARA_LSM::Cuboid> holesVector ;
  for (int i = 10 ; i < nelx ; i=i+20) {
    for (int j = 10 ; j <= nely ; j=j+20) {
      PARA_LSM::Cuboid hole1 (i, j, nelz,     // Cubeoid center
                              4, 4, nelz/2) ; // half of the width of the cubeoid
      holesVector.push_back (hole1) ;
    }
  }

  for (int i = 0 ; i < holesVector.size () ; i++) {
    initialVoids.push_back (std::make_unique<PARA_LSM::Cuboid> (holesVector[i])) ;
  }

  // Fix a cubeoid (square region at the base of design domain)
  static PARA_LSM::Cuboid cube0 (0.5*nelx, 0.5*nely, 0, // Cubeoid center
    0.05*nelx, 0.05*nely, 3.0) ; // half of the width of the cubeoid
  fixedBlobs.push_back (std::make_unique<PARA_LSM::Cuboid> (cube0)) ;
}


Force :: Force () : valx (0), valy (0), valz (0), x (0), y (0), z (0), tolx (0),
    toly (0), tolz (0) {
}


Force :: Force (double valx_, double valy_, double valz_, double x_, double y_,
    double z_, double tolx_, double toly_, double tolz_) : valx (valx_),
    valy (valy_), valz (valz_), x (x_), y (y_), z (z_), tolx (tolx_),
    toly (toly_), tolz (tolz_) {
}


FixedDof :: FixedDof (bool valx_, bool valy_, bool valz_, double x_, double y_,
    double z_, double tolx_, double toly_, double tolz_) : valx (valx_),
    valy (valy_), valz (valz_), x (x_), y (y_), z (z_), tolx (tolx_),
    toly (toly_), tolz (tolz_) {
}


HeatLoad :: HeatLoad () : val (0), x (0), y (0), z (0), tolx (0), toly (0),
    tolz (0) {
}


HeatLoad :: HeatLoad (double val_, double x_, double y_, double z_, double tolx_,
    double toly_, double tolz_) : val (val_), x (x_), y (y_), z (z_),
    tolx (tolx_), toly(toly_), tolz(tolz_) {
}

} // namespace PARA_FEA