#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include "topopt.h"
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";


bool isRigid(double x, double y, double z, std::vector<double> rigidBars){
	double min_dist2 = 1.0e10;// some large number

	for(int i = 0; i< rigidBars.size()/6; i++){

		double dist1 = std::pow(x - rigidBars[6*i+0], 2)
		+ std::pow(y - rigidBars[6*i+1], 2) + std::pow(z - rigidBars[6*i+2], 2);

		double dist2 = std::pow(x - rigidBars[6*i+3], 2)
		+ std::pow(y - rigidBars[6*i+4], 2) + std::pow(z - rigidBars[6*i+5], 2);

		double dist3 = std::pow(rigidBars[6*i+3] - rigidBars[6*i+0], 2)
		+ std::pow(rigidBars[6*i+4] - rigidBars[6*i+1], 2) + std::pow(rigidBars[6*i+5] - rigidBars[6*i+2], 2);

		min_dist2 = std::min(min_dist2, sqrt(dist1) + sqrt(dist2) - sqrt(dist3));
	}

	return min_dist2 <= 1.0;
}

int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;


			fea::TopOpt *opt = new fea::TopOpt(myInit);
			fea::LinearElasticity *physics = new fea::LinearElasticity(opt, myInit);

			fea::TopOpt1D *optHeat = new fea::TopOpt1D(myInit);
			fea::Poisson *physicsHeat = new fea::Poisson(optHeat, myInit);



		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = opt->nxyz[0]-1;
			int nely = opt->nxyz[1]-1;
			int nelz = opt->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// std::vector<Cuboid> cubes;
			// Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			std::ofstream txtfile_iterhist;//io

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);

				txtfile_iterhist.open("iterhist.txt");

				#if 0
				std::ifstream infile;
				infile.open("SDatgridpointsInp.txt");
				for(int k = 0; k <= nelz; k ++){
			     for(int j = 0; j <= nely; j ++){
			       for(int i = 0; i <= nelx; i ++){
							 double tempval;
							 infile >> tempval;
							  level_set_3d.phi[level_set_3d.GridPtToIndex(i,j,k)].val = tempval;
						 }//i
					 }//j
				 }//k
				 #endif
			}//rank

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(opt, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < opt->maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					std::cout << "Finished boundary computation." << std::endl;
					// boundary.WriteSTL();
					// level_set_3d.WriteSD();
					// return 0;
					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					double dx = 8.0/96.0*nelx;
				  double dy = 16.0/96.0*nelx;
				  double dz = 14.0/96.0*nelx;
					std::vector<double> rigidBars = {dx, dy, nelz/8.0 + dz, nelx/4, nely/2, nelz,
																					 dx, nely - dy, nelz/8.0 + dz, nelx/4, nely/2, nelz,
																				 	nelx/2  - dx, dy, nelz - dz, nelx/4, nely/2, nelz,
																				 	nelx/2 - dx, nely - dy, nelz - dz, nelx/4, nely/2, nelz};

					// assign volume fractions to petsc mesh
					// step-1
					double count_rigid = 0;
					for(int k = 0; k < nelz; k++){
						for(int j = 0; j < nely; j++){
							for(int i = 0; i < nelx; i++){
								int countFEM = i + j*nelx + k*nelx*nely;
								// centroids
								double xc = i+0.5; double yc = j+0.5;	double zc = k+0.5;

								volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
								= std::max(level_set_3d.volFractions[countFEM],0.0);

								// if(isRigid(xc, yc, zc, rigidBars)){
								// // volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]] += 10.0;
								// 	count_rigid++;
								// }
							}
						}
					}
					std::cout << "count_rigid = " << count_rigid << std::endl;
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(opt->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					// step-1
					for(int k = 0; k < nelz; k++){
						for(int j = 0; j < nely; j++){
							for(int i = 0; i < nelx; i++){
								int countFEM = i + j*nelx + k*nelx*nely;
								// centroids
								double xc = i+0.5; double yc = j+0.5;	double zc = k+0.5;

								volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
								= std::max(level_set_3d.volFractions[countFEM],0.0);
							}
						}
					}
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(optHeat->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;
					//=================================================================
					double elemTempMax = 0.0;
					// assign temperature field
					for(int k = 0; k < nelz; k++){
						for(int j = 0; j < nely; j++){
							for(int i = 0; i < nelx; i++){
								int countFEM = i + j*nelx + k*nelx*nely;
								// centroids
								double xc = i+0.5; double yc = j+0.5;	double zc = k+0.5;

								double T0 = 30.0;
								double z0 = nelz/3.0; double z1 = 4.0*nelz/3.0;
								double x0 = 2.0*nelx/3.0;

								// double elemTemp = T0*zc*( 1.0/z0*(1.0 - xc/x0) + 1.0/z1*(xc/x0) );
								double elemTemp = T0*zc/(z0 + sqrt(xc)*sqrt(x0));
								elemTempMax = std::max(elemTempMax, elemTemp*level_set_3d.volFractions[countFEM]);

								// use this vector to fill in element temperatures
								volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]] = elemTemp;

							}
						}
					}
					std::cout << "elemTempMax = " << elemTempMax << std::endl;
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(opt->DeltaTVec, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned temperatures." << std::endl;


					//================================================================

				}
				VecAssemblyBegin(opt->x);	VecAssemblyEnd(opt->x);
				VecAssemblyBegin(opt->DeltaTVec);	VecAssemblyEnd(opt->DeltaTVec);

				VecAssemblyBegin(optHeat->x);	VecAssemblyEnd(optHeat->x);

				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// compute sensitivities
				physics->myRTOL = 1.0e-5;
				VecAYPX(physics->RHS,0.0,physics->RHS1);
				ierr = physics->ComputeSensitivities(opt); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygather(opt->dfdx);
				double compliance = opt->fx;
				VecSet(physics->U, 0.0);

				VecAYPX(physics->RHS,0.0,physics->RHS2);
				ierr = physics->ComputeSensitivities(opt); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygather1(opt->dfdx);
				compliance += opt->fx;

				//==========================================================================
				// Compute sensitivities for the other load case
				bool include_conduction = true;
				if(include_conduction){
					ierr = physicsHeat->ComputeCentroidTemperatures(optHeat); CHKERRQ(ierr);
					// pass in temperature field to the mechanical mesh
					VecAYPX(opt->DeltaTVec,0.0,optHeat->DeltaTVec);
				}else{
					VecAYPX(optHeat->DeltaTVec,0.0,opt->DeltaTVec);
				}

				double thRot = 0.0;
				if(itr <= opt->maxItr){
					physics->myRTOL = 1.0e-9;
					VecSet(physics->U, 0.0);
					ierr = physics->ComputeThRotSensitivities(opt); CHKERRQ(ierr);
					PetscScalar dotp1;
					VecDot(physics->U, physics->adRHS, &dotp1);
					thRot = dotp1;
					if(rank == 0) std::cout << "thRot = " << thRot << std::endl;

					std::vector<double> dirArr = {-7.0/8.0623, 0.0, 4.0/8.0623};
					// std::vector<double> dirArr = {1.0,0.0,0.0};
					ierr = physics->ComputeElemDirDisp(opt, dirArr); CHKERRQ(ierr);
					// ierr = physics->ComputeElemMagDisp(opt); CHKERRQ(ierr);

					VecSet(physics->U, 0.0);
				}

				if(include_conduction){
					// pass in adjoint temperature field to the mechanical mesh
					VecAYPX(optHeat->DeltaTVecAdj,0.0,opt->DeltaTVecAdj);
					// copmute the adjoint sensitivities
					ierr = physicsHeat->ComputeAdjointSensitivities(optHeat); CHKERRQ(ierr);
				}
				//=====================================================================
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygatherTemp(optHeat->DeltaTVec);
				fea::MyGather mygatherThRotPart1(opt->dfdx);
				fea::MyGather mygatherThRotPart2(optHeat->dfdx);

				// displacement
				fea::MyGather mygatherDisp(opt->elemDisp);

				//=====================================================================


				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {
					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> comSens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(comSens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);

							std::vector<double> comSens1(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(comSens1, mygather1.sensiArray,
								volfrac_to_petsc_map, 2);
							for(int i = 0; i< comSens.size(); i++) comSens[i] += comSens1[i] ;

							std::vector<double> rotSens1(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(rotSens1, mygatherThRotPart1.sensiArray,
								volfrac_to_petsc_map, 2);

							std::vector<double> rotSens2(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(rotSens2, mygatherThRotPart2.sensiArray,
								volfrac_to_petsc_map, 2);

							std::vector<double> rotSens(boundary.numTriangles, 0.0);
							for(int i = 0; i< rotSens.size(); i++) rotSens[i] = rotSens1[i] + rotSens2[i] ;

							std::vector<double> rotSensNeg(boundary.numTriangles, 0.0);
							for(int i = 0; i< rotSens.size(); i++) rotSensNeg[i] = -rotSens[i];


							// Interpolate tempaerature
							std::vector<double> bTemp(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(bTemp, mygatherTemp.sensiArray,
								volfrac_to_petsc_map, 2.0, 0);

							// Interpolate displacement
								std::vector<double> bDisp(boundary.numTriangles, 0.0);
								boundary.InterpolateBoundarySensitivities(bDisp, mygatherDisp.sensiArray,
									volfrac_to_petsc_map, 2, 0);

								{
									std::ofstream txtfile_temp;
									txtfile_temp.open("temp.txt");
									for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
									 txtfile_temp << bTemp[i1] << std::endl;
									}
									txtfile_temp.close();
								}
								{
									std::ofstream txtfile_temp;
									txtfile_temp.open("dispDir.txt");
									for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
									 txtfile_temp << bDisp[i1] << std::endl;
									}
									txtfile_temp.close();
								}
								{
									std::ofstream txtfile_temp;
									txtfile_temp.open("comsens.txt");
									for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
									 txtfile_temp << comSens[i1] << std::endl;
									}
									txtfile_temp.close();
								}
								{
									std::ofstream txtfile_temp;
									txtfile_temp.open("rotsens.txt");
									for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
									 txtfile_temp << rotSens[i1] << std::endl;
									}
									txtfile_temp.close();
								}


								boundary.WritePatch();

								std::cout << "Completed sensitivity interpolation." << std::endl;
						//===========================================================================
						// Optimize boundary velocities
						 double move_limit = 0.375;
						 // if(itr > 50) move_limit = 0.1;
						 // boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons, 0);

					 	std::vector<double> volSens(boundary.numTriangles, 1.0);

						// /////////////////////////////////////////////////////////////////
						// // PROBLEM-1: min. compliance s.t. volume constraint and rotation constraints
						// // constraints
						// int nCons = 3;
						// std::vector<std::vector<double>> conSens;
						// conSens.resize(nCons);
						// std::vector<double> conMaxVals(nCons);
						// std::vector<double> conCurVals(nCons);
						// //
						// double positive_rotation_constraint = 17.45;
						// double negative_rotation_constraint = -17.45;
						//
						// // volume constraint
						// conSens[0] = volSens;
						// conMaxVals[0] = myInit.volCons*nelx*nely*nelz;
						// conCurVals[0] = level_set_3d.volume;
						//
						// // rotation constraint
						// conSens[1] = rotSens; // sensitivity of rotation constraint
						// conMaxVals[1] = positive_rotation_constraint;
						// conCurVals[1] = thRot; // current value of rotation
						//
						// conSens[2] = rotSensNeg; // sensitivity of negative rotation constraint
						// conMaxVals[2] = -(negative_rotation_constraint); // negative of the negative rotation constraint
						// conCurVals[2] = -thRot; // negative of the current value of rotation
						//
						//  boundary.Optimize(
						// 	 comSens, /* Objective function sensitivity */
						// 	conSens, conMaxVals, conCurVals, /* Constraint function variables */
						// 	move_limit, /* move limit */
						// 	1 /* algorithm type 1 = NLOPT-MMA */
						// );
						// ///////////////////////////////////////////////////////////////

						/////////////////////////////////////////////////////////////////
						// PROBLEM-2: min. volume s.t. compliance constraint and rotation constraints
						double compliance_constraint = 0.0111299;
						double positive_rotation_constraint = 17.45;
						double negative_rotation_constraint = -17.45;
						int nCons = 3;
						std::vector<std::vector<double>> conSens;
						conSens.resize(nCons);
						std::vector<double> conMaxVals(nCons);
						std::vector<double> conCurVals(nCons);

						// compliance constraint realated variables
						conSens[0] = comSens;
						conMaxVals[0] = compliance_constraint;
						conCurVals[0] = compliance;

						// rotation constraint
						conSens[1] = rotSens; // sensitivity of rotation constraint
						conMaxVals[1] = positive_rotation_constraint;
						conCurVals[1] = thRot; // current value of rotation

						conSens[2] = rotSensNeg; // sensitivity of negative rotation constraint
						conMaxVals[2] = -(negative_rotation_constraint); // negative of the negative rotation constraint
						conCurVals[2] = -thRot; // negative of the current value of rotation

						 boundary.Optimize(
							 volSens, /* Objective function sensitivity */
							conSens, conMaxVals, conCurVals, /* Constraint function variables */
							move_limit, /* move limit */
							1 /* algorithm type 1 = NLOPT-MMA */
						);
						///////////////////////////////////////////////////////////////

						///////////////////////////////////////////////////////////////////
						// // PROBLEM-3: min. rotation squared s.t. compliance constraint and volume constraint
						// // Minimize volume, s.t rotation constraints and compliance constraint
						// double compliance_constraint = 0.0111299;
						// int nCons = ??;
						// std::vector<std::vector<double>> conSens;
						// conSens.resize(nCons);
						// std::vector<double> conMaxVals(nCons);
						// std::vector<double> conCurVals(nCons);
						//
						// // volume constraint realated variables
						// conSens[0] = ??;
						// conMaxVals[0] = ??;
						// conCurVals[0] = ??;
						//
						// // // compliance constraint realated variables
						// conSens[1] = ??; // sensitivity of compliance
						// conMaxVals[1] = ??;
						// conCurVals[1] = ??; // current value of compliance
						//
						// // sensitivities of rotation constraint squared
						// std::vector<double> rotSquaredSens(boundary.numTriangles, 0.0);
						// for(int i = 0; i< rotSquaredSens.size(); i++) rotSquaredSens[i] = 2*thRot*rotSens[i];
						//
						//
						//  boundary.Optimize(
						// 	 ??, /* Objective function sensitivity */
						// 	conSens, conMaxVals, conCurVals, /* Constraint function variables */
						// 	move_limit, /* move limit */
						// 	1 /* algorithm type 1 = NLOPT-MMA */
						// );
						// /////////////////////////////////////////////////////////////////

							std::cout << "Completed boundary velocities optimization. " << std::endl;
					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						level_set_3d.MakeDomainHoles();
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl
					 if(fmod( itr , 2 ) == 1)	{
 						 boundary.WriteSTL("TopAfterIter" +
						 	std::to_string(itr%4) + ".stl");

							level_set_3d.WriteSD();
 				 		}
					 std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Compliance = " << compliance << "; ThermalRotation = " << thRot<< "\n"<< std::endl ;

					 txtfile_iterhist <<itr << " " << compliance << " " << level_set_3d.volume / nelx / nely / nelz << " "<< thRot << std::endl;
				 }



				 //==========================================================================
				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			if(rank == 0) txtfile_iterhist.close();

			delete opt;
			delete physics;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
