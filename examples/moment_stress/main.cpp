#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";


int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;

			// STEP 1: THE OPTIMIZATION PARAMETERS, DATA AND MESH (!!! THE DMDA !!!)
			fea::TopOpt *opt = new fea::TopOpt(myInit);

			// STEP 2: THE PHYSICS
			fea::LinearElasticity *physics = new fea::LinearElasticity(opt, myInit);

		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = opt->nxyz[0]-1;
			int nely = opt->nxyz[1]-1;
			int nelz = opt->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// create quadrature object
			lsm::MomentQuadrature momQuad(boundary);

			// std::vector<Cuboid> cubes;
			// Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);

				// for stress
				level_set_3d.SetIgnoreElementsStress() ;
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
			std::vector<std::vector<double>> momentPetscForMap;
			int numMomentQuadPoints = 27;
			std::vector<bool> ignoreElemMap;
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
				ignoreElemMap.resize(nElements,0);
				momentPetscForMap.resize(nElements);
				for(int i = 0; i < nElements; i++){
					momentPetscForMap[i].resize(numMomentQuadPoints,0.0);
				}
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(opt, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < opt->maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					std::cout << "Finished boundary computation." << std::endl;
					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					// Compute quadrature
					momQuad.ComputeElementsQuadrature();
					std::cout << "Finished moments computation." << std::endl;

					// check if the triaArray makes sense
					int nBoundaryElements = 0;
					for(int i = 0; i < nElements; i++){
						// if(i == 63){
						// 	std::cout << "quadrature = " << std::endl;
						// 	std::cout << momQuad.elememtsQuadrature[i] << std::endl;
						// 	std::cout << "quad.sum() = " << momQuad.elememtsQuadrature[i].sum() <<  std::endl;
						// }
						if(momQuad.elememtsQuadrature[i].size() > 0) nBoundaryElements++;
					}
					std::cout << "nBoundaryElements = " << nBoundaryElements << std::endl;

					//===================================================================
					std::vector<double> wGauss = {5.0/18.0, 8.0/18.0, 5.0/18.0};
					Eigen::VectorXd elememtsQuadratureGauss(27);
					int count = 0;
					for(int k = 0; k < 3; k++){
						for(int j = 0; j < 3; j++){
							for(int i = 0; i < 3; i++){
								elememtsQuadratureGauss(count) = wGauss[i]*wGauss[j]*wGauss[k];
								count++;
							}
						}
					}
					//===================================================================

					// assign volume fractions to petsc mesh
					// step-1
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
						= std::max(level_set_3d.volFractions[countFEM],0.0);

						// for stress
						ignoreElemMap[volfrac_to_petsc_map[countFEM]]
						= level_set_3d.ignoreElemStress[countFEM];
					}
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(opt->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
						// for stress
						VecSetValue(opt->ignoreElemStress, countFEM,
							1.0*ignoreElemMap[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;

					// assign quadrature to petsc mesh
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						if(momQuad.elememtsQuadrature[countFEM].size() > 0 &&
						(momQuad.elememtsQuadrature[countFEM].sum() > 0.001 && momQuad.elememtsQuadrature[countFEM].sum() < 0.999)
					){
							for(int i = 0; i < numMomentQuadPoints; i++){
								momentPetscForMap[volfrac_to_petsc_map[countFEM]][i]
								 = 8.0*momQuad.elememtsQuadrature[countFEM][i];
							}
						}
						else{
							for(int i = 0; i < numMomentQuadPoints; i++){
								momentPetscForMap[volfrac_to_petsc_map[countFEM]][i]
								 = 8.0*elememtsQuadratureGauss(i) * level_set_3d.volFractions[countFEM];
							}
						}

					}
					std::cout << "Computed moments in petsc mesh." << std::endl;
					// step-2
					#pragma openmp parallel for
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						// if(volumefraction_vector_for_map[countFEM] > 0.0 )
						for(int i = 0; i < numMomentQuadPoints; i++){
								VecSetValue(opt->momQuad[i], countFEM,
									momentPetscForMap[countFEM][i] , INSERT_VALUES );
						}
					}

					std::cout << "Assigned moments in petsc mesh." << std::endl;
					//=================================================================

				}
				VecAssemblyBegin(opt->x);	VecAssemblyEnd(opt->x);
				VecAssemblyBegin(opt->ignoreElemStress);	VecAssemblyEnd(opt->ignoreElemStress);

				for(int i = 0; i < numMomentQuadPoints; i++){
					VecAssemblyBegin(opt->momQuad[i]);	VecAssemblyEnd(opt->momQuad[i]);
				}

				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// Compute sensitivities
				// ierr = physics->ComputeSensitivities(opt); CHKERRQ(ierr);
				// ierr = physics->ComputeStressSensitivitiesSandy(opt); CHKERRQ(ierr);
				// ierr = physics->ComputeStress(opt); CHKERRQ(ierr);

				// ierr = physics->ComputeSensitivitiesFromQuadrature(opt); CHKERRQ(ierr);
				ierr = physics->ComputeStressSensitivitiesFromQuadrature(opt); CHKERRQ(ierr);
				// ierr = physics->ComputeStress(opt); CHKERRQ(ierr);
				// VecSet(physics->U,0.0);
				//==========================================================================

				//===========================================================================
				// gather senstitivities on to the zeroeth processor
					 fea::MyGather mygather(opt->dfdx);
					 fea::MyGather mygatherStress(opt->elemMaxStress);
				//===========================================================================


				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {
					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> bsens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(bsens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);
							std::cout << "Completed sensitivity interpolation." << std::endl;
						// Interpolate stress
						std::vector<double> bStress(boundary.numTriangles, 0.0);
						boundary.InterpolateBoundarySensitivities(bStress, mygatherStress.sensiArray,
							volfrac_to_petsc_map, 2.5);

							std::ofstream txtfile_stress;
							txtfile_stress.open("stress.txt");
							for (int i1 = 0 ; i1 < boundary.numTriangles ; i1++){
							 txtfile_stress << bStress[i1] << std::endl;
							}
							txtfile_stress.close();
							boundary.WritePatch();
						//===========================================================================
						// Optimize boundary velocities
						double move_limit = 0.25;
						 boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons);
							std::cout << "Completed boundary velocities optimization. " << std::endl;
					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						level_set_3d.MakeDomainHoles();
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl
					 if(fmod( itr , 2 ) == 1)	{
 						 boundary.WriteSTL("TopAfterIter" +
						 	std::to_string(itr%4) + ".stl");
 				 		}
					 std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Objective = " << opt->fx << "\n"<< std::endl ;

					 level_set_3d.WriteSD();
				 }

				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			delete opt;
			delete physics;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
