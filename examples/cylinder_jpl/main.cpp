#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";

int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;

			fea::TopOpt1D *optHeat = new fea::TopOpt1D(myInit);
			fea::Poisson *physicsHeat = new fea::Poisson(optHeat, myInit);



		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = optHeat->nxyz[0]-1;
			int nely = optHeat->nxyz[1]-1;
			int nelz = optHeat->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// std::vector<Cuboid> cubes;
			// Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			std::ofstream txtfile_iterhist;//io

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);

				txtfile_iterhist.open("iterhist.txt");

				#if 0
				std::ifstream infile;
				infile.open("SDatgridpointsInp.txt");
				for(int k = 0; k <= nelz; k ++){
			     for(int j = 0; j <= nely; j ++){
			       for(int i = 0; i <= nelx; i ++){
							 double tempval;
							 infile >> tempval;
							  level_set_3d.phi[level_set_3d.GridPtToIndex(i,j,k)].val = tempval;
						 }//i
					 }//j
				 }//k
				 #endif
			}//rank

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(optHeat, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < myInit.maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					std::cout << "Finished boundary computation." << std::endl;
					boundary.WriteSTL();
					// level_set_3d.WriteSD();
					return 0;
					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					// assign volume fractions to petsc mesh
					// step-1
					double count_rigid = 0;
					for(int k = 0; k < nelz; k++){
						for(int j = 0; j < nely; j++){
							for(int i = 0; i < nelx; i++){
								int countFEM = i + j*nelx + k*nelx*nely;
								// centroids
								double xc = i+0.5; double yc = j+0.5;	double zc = k+0.5;

								volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
								= std::max(level_set_3d.volFractions[countFEM],0.0);
							}
						}
					}

					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(optHeat->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;
					//=================================================================
				}
				VecAssemblyBegin(optHeat->x);	VecAssemblyEnd(optHeat->x);

				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// compute sensitivities
				physicsHeat->myRTOL = 1.0e-6;
				ierr = physicsHeat->ComputeSensitivities(optHeat); CHKERRQ(ierr);
				// gather senstitivities on to the zeroeth processor
				fea::MyGather mygather(optHeat->dfdx);
				double compliance = optHeat->fx;
				//==========================================================================
				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {

					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> comSens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(comSens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);
								std::cout << "Completed sensitivity interpolation." << std::endl;
						//===========================================================================
						// Optimize boundary velocities
						 double move_limit = 0.5;
						 // if(itr > 50) move_limit = 0.1;
						 // boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons, 0);

					 	std::vector<double> volSens(boundary.numTriangles, 1.0);

						// constraints
						int nCons = 1;
						std::vector<std::vector<double>> conSens;
						conSens.resize(nCons);
						std::vector<double> conMaxVals(nCons);
						std::vector<double> conCurVals(nCons);
						// volume constraint
						conSens[0] = volSens;
						conMaxVals[0] = myInit.volCons*nelx*nely*nelz;
						conCurVals[0] = level_set_3d.volume;
						// // aggregation constraint
						// std::vector<double> aggSensi; double maxVal; int radius = 12;
						// level_set_3d.ComputeAggregationSensitivities(aggSensi, maxVal, radius);
					// std::vector<double> aggSens(boundary.numTriangles, 0.0);
					// double* aggSensiPointer = &aggSensi[0];
					// boundary.InterpolateBoundarySensitivities(aggSens, aggSensiPointer,
					// 	volfrac_to_petsc_map, 2, 0,0);
						// conSens[1] = aggSens;
						// conMaxVals[1] = 0.5;
						// conCurVals[1] = maxVal;
						 // boundary.Optimize(comSens,
							// conSens, conMaxVals, conCurVals,
							// move_limit, 0);


							boundary.Optimize(comSens, move_limit, level_set_3d.volume, myInit.volCons);

							std::cout << "Completed boundary velocities optimization. " << std::endl;
					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						level_set_3d.MakeDomainHoles();
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl
					 if(fmod( itr , 2 ) == 1)	{
 						 boundary.WriteSTL("TopAfterIter" +
						 	std::to_string(itr%4) + ".stl");

							level_set_3d.WriteSD();
 				 		}
					 std:std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Compliance = " << compliance << "\n"<< std::endl ;

					 txtfile_iterhist <<itr << " " << compliance << " " << level_set_3d.volume / nelx / nely / nelz << std::endl;
				 }



				 //==========================================================================
				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			if(rank == 0) txtfile_iterhist.close();

			delete optHeat;
			delete physicsHeat;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
