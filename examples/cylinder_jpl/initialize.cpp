#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 200; nely = 200; nelz = 200;

  nlvls = 4;

  lx = 0.04; ly = 0.04; lz = 0.04;

  maxItr = 201;

  Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
  Kmax = 6.7; Kmin = 6.7*0.1;
  volCons = 0.05;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  fixedHeatDof.push_back(HeatLoad( 0.0,
                  0*lx, ly/2.0, 0*lz,
                  0.45*lx+eps, ly, eps));
  //
  fixedHeatDof.push_back(HeatLoad( 0.0,
                  0*lx, ly/2.0, 0*lz,
                  eps, ly, 0.45*lz+eps));
  //

  fixedHeatDof.push_back(HeatLoad( 0.0,
                  1*lx, ly/2.0, 0*lz,
                  0.45*lx+eps, ly, eps));
  //
  fixedHeatDof.push_back(HeatLoad( 0.0,
                  1*lx, ly/2.0, 0*lz,
                  eps, ly, 0.45*lz+eps));
  //

  fixedHeatDof.push_back(HeatLoad( 0.0,
                  1*lx, ly/2.0, 1*lz,
                  0.45*lx+eps, ly, eps));
  //
  fixedHeatDof.push_back(HeatLoad( 0.0,
                  1*lx, ly/2.0, 1*lz,
                  eps, ly, 0.45*lz+eps));
  //

  fixedHeatDof.push_back(HeatLoad( 0.0,
                  0*lx, ly/2.0, 1*lz,
                  0.45*lx+eps, ly, eps));
  //
  fixedHeatDof.push_back(HeatLoad( 0.0,
                  0*lx, ly/2.0, 1*lz,
                  eps, ly, 0.45*lz+eps));
  //

  heatLoad.push_back(HeatLoad( 1.0e-3,
                  lx/2.0, ly/2.0, lz/2.0,
                  1*a+eps, ly, 1*c+eps));
  //

  static std::vector<PARA_LSM::Cylinder> holesVector;
  int numSmallHoles = 20;
  double rSmall = 0.025*nely;
  double rBig = 0.35*nely;

  /*
  for(int thetaN = 0; thetaN < numSmallHoles; thetaN++){
    double theta = (1.0*thetaN)/(1.0*numSmallHoles);
    double rx =  0.5*nelx + rBig*std::cos(theta*2*M_PI);
    double rz =  0.5*nelx + rBig*std::sin(theta*2*M_PI);

    Cylinder cylinderTemp(rx, 0.5*nely, rz,
                nely, rSmall, 0, 1);
    //
    holesVector.push_back(cylinderTemp);
  }


  for(int i = 10; i < nely; i=i+20){
    Cylinder cylinderTemp(0.5*nelx, i, 0.5*nelz,
                5, rBig+5, rBig-5, 1);
    //
    holesVector.push_back(cylinderTemp);
  }
  */

  for(int i = 0; i < holesVector.size(); i++){
      initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (holesVector[i]));
  }

  double rOuterHole = 0.45*nely;
  double rInnerHole = 0.25*nely;
  static PARA_LSM::Cylinder cylinderInner(0.5*nelx, 0.5*nely, 0.5*nelz,
              nely/2, rInnerHole, 0, 1);
  //
  static PARA_LSM::Cylinder cylinderOuter(0.5*nelx, 0.5*nely, 0.5*nelz,
              nely/2, rOuterHole, 0, 1);
  //
  cylinderOuter.flipDistanceSign = -1;

  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderInner));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderOuter));

  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderInner));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderOuter));

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

FixedDof::FixedDof() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA