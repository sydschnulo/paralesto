#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 128; nely = 64; nelz = 64;

  nlvls = 4;

  lx = 2.0; ly = 1.0; lz = 1.0;

  maxItr = 201;

  Emax = 113.8e9; Emin = 1.0e-9*Emax; nu = 0.342;
  Kmax = 6.7; Kmin = 6.6e-3;
  volCons = 0.45;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  fixedHeatDof.push_back(HeatLoad( 0.0,
                  0*a, 0*b, 0*c,
                  a+eps, b+eps, c+eps));
  //

  heatLoad.push_back(HeatLoad( 1.0,
                  lx, ly, lz,
                  a+eps, b+eps, c+eps));
  //

  static std::vector<PARA_LSM::Cuboid> holesVector;
  for(int j = 8; j <= nely; j=j+16){
      for(int k = 8; k <= nelz; k=k+16){
        PARA_LSM::Cuboid hole1(0.0*nelx, j, k,
                    2*nelx,  5, 5);
        //
        holesVector.push_back(hole1);
      }
  }
  for(int i = 8; i < nelx; i=i+16){
      for(int k = 8; k <= nelz; k=k+16){
        PARA_LSM::Cuboid hole1(i, 0, k,
                    5,  2*nely, 5);
        //
        holesVector.push_back(hole1);
      }
  }
  for(int i = 8; i < nelx; i=i+16){
      for(int j = 8; j <= nely; j=j+16){
        PARA_LSM::Cuboid hole1(i, j, 0,
                    5,  5, 2*nelz);
        //
        holesVector.push_back(hole1);
      }
  }

  for(int i = 0; i < holesVector.size(); i++){
      initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (holesVector[i]));
  }

  static PARA_LSM::Cuboid cube0(0, 0, 0,
              3,  3, 3);
  //////
  static PARA_LSM::Cuboid cube1(nelx, 0, 0,
              3,  3, 3);
  //////

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

FixedDof::FixedDof() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA