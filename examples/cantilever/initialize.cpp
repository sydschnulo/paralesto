#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 160; nely = 80; nelz = 80;

  nlvls = 3;

  lx = 2.0; ly = 1.0; lz = 1.0;

  maxItr = 401;


  Emax = 1.0; Emin = 1.0e-9; nu = 0.3;
  volCons = 0.5;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});

  loads.push_back(Force( 0.0, 1.0, -1.0,
                  lx, 1.0*ly, 0,
                  eps, eps, eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 0.0*ly, 0.0*lz,
                  eps, 5*b, 5*c));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 1.0*ly, 0.0*lz,
                  eps, 5*b, 5*c));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 0.0*ly, 1.0*lz,
                  eps, 5*b, 5*c));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  0.0*lx, 1.0*ly, 1.0*lz,
                  eps, 5*b, 5*c));
  //


  // static Cuboid cube0(nelx, nely/2, 0,
  // 			             5,  nely, 5.0);
  // //
  // fixedBlobs.push_back(&cube0);

  static std::vector<PARA_LSM::Cuboid> holesVector;
  for(int j = 8; j <= nely; j=j+16){
      for(int k = 8; k <= nelz; k=k+16){
        PARA_LSM::Cuboid hole1(0.0*nelx, j, k,
                    2*nelx,  5, 5);
        //
        holesVector.push_back(hole1);
      }
  }
  for(int i = 8; i < nelx; i=i+16){
      for(int k = 8; k <= nelz; k=k+16){
        PARA_LSM::Cuboid hole1(i, 0, k,
                    5,  2*nely, 5);
        //
        holesVector.push_back(hole1);
      }
  }
  for(int i = 8; i < nelx; i=i+16){
      for(int j = 8; j <= nely; j=j+16){
        PARA_LSM::Cuboid hole1(i, j, 0,
                    5,  5, 2*nelz);
        //
        holesVector.push_back(hole1);
      }
  }

  for(int i = 0; i < holesVector.size(); i++){
      initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (holesVector[i]));
  }

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA