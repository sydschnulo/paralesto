#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <sstream>

#include <petsc.h>
#include <mpi.h>
#include <omp.h>

#include "para_fea.h"
#include "para_lsm.h"

namespace fea = PARA_FEA ;
namespace lsm = PARA_LSM ;

static char help[] = "3D LSTO using KSP-MG on PETSc's DMDA (structured grids) \n";


int main(int argc, char *argv[]){


		//===========================================================================
		// Set up Topopt and LinearElasticity classes
		// Error code for debugging
			PetscErrorCode ierr;

			// Initialize PETSc / MPI and pass input arguments to PETSc
			PetscInitialize(&argc,&argv,PETSC_NULL,help);

			// declare petsc int here for rank and size
			PetscMPIInt rank, size;

			// define rank and size here
			MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
			MPI_Comm_size(PETSC_COMM_WORLD,&size);

			// Initialize the input file
			fea::InitializeOpt myInit;

			// STEP 1: THE OPTIMIZATION PARAMETERS, DATA AND MESH (!!! THE DMDA !!!)
			fea::TopOpt *opt = new fea::TopOpt(myInit);

			// STEP 2: THE PHYSICS
			fea::LinearElasticity *physics = new fea::LinearElasticity(opt, myInit);

		//============================================================================
		// Define  and setup level set parameters

			//mesh size
			int nelx = opt->nxyz[0]-1;
			int nely = opt->nxyz[1]-1;
			int nelz = opt->nxyz[2]-1;

			// size of volumeFraction vector
			int nElements = nelx * nely * nelz;

			// initialize vector that maps ls grid to petsc grid
			std::vector<int> volfrac_to_petsc_map(1,0);

			// Make a box in openvdb
			uint box_x = nelx;
			uint box_y = nely;
			uint box_z = nelz;

			// Define the grid
			lsm::Grid grid((0==rank)*nelx, (0==rank)*nely, (0==rank)*nelz);
			//  Create an object of the levelset class
			lsm::LevelSet3D level_set_3d(grid);
			// create boundary
			lsm::Boundary boundary(level_set_3d);

			// std::vector<lsm::Cuboid> cubes;
			// lsm::Cuboid cube0(nelx/2.0, nely/2.0, nelz-2.0,
			// 						nelx/2.0,  nely/2.0, 2.0);
			// cubes.push_back(cube0);
			// level_set_3d.fixedBlobs.push_back(std::move(&cubes[0]));
			level_set_3d.fixedBlobs = std::move (myInit.fixedBlobs);
			level_set_3d.domainVoids = std::move (myInit.domainVoids);
			level_set_3d.initialVoids = std::move (myInit.initialVoids);

			// define level set functions on processor number 1
			if(rank == 0){
				level_set_3d.hWidth = 4;

				level_set_3d.MakeBox() ;
				level_set_3d.volFractions.resize(level_set_3d.nCells, 1.0);
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// actual volume fractions that are sent to petsc
			std::vector<double> volumefraction_vector_for_map(1,0.2);
				if(rank == 0){
				volumefraction_vector_for_map.resize(nElements,0.2);
			}

			MPI_Barrier(PETSC_COMM_WORLD);

			// Initialize walltime
			double walltime1, walltime2;

			// Create a map between ls and petsc here
			PetscInt ni = nElements;

			// Form a map between volume fractions on level set on rank 0
			// and petsc mesh
			fea::Wrapper wrapper;
			wrapper.GetMap(opt, volfrac_to_petsc_map, nElements);

			// Optimization loop
			PetscInt itr = 0;
			 double iter_start_time;
			while (itr < opt->maxItr){

				iter_start_time = MPI_Wtime();
				// Update iteration counter
				itr++;

				if(rank == 0){

					//=================================================================
					// Compute boundary
					boundary.MarchCubes();
					std::cout << "Finished boundary computation." << std::endl;
					//=================================================================

					//=================================================================
					// Compute volume fractions and pass them to petsc
					level_set_3d.ComputeVolumeFractions();

					// assign volume fractions to petsc mesh
					// step-1
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						volumefraction_vector_for_map[ volfrac_to_petsc_map[countFEM]]
						= std::max(level_set_3d.volFractions[countFEM],0.0);
					}
					// step-2
					for(int countFEM = 0; countFEM < nElements; countFEM++){
						VecSetValue(opt->x, countFEM,
							volumefraction_vector_for_map[countFEM] , INSERT_VALUES );
					}
					std::cout << "Assigned volume fractions." << std::endl;
					//=================================================================

				}
				VecAssemblyBegin(opt->x);	VecAssemblyEnd(opt->x);

				MPI_Barrier(PETSC_COMM_WORLD);
				//==========================================================================
				// Compute sensitivities
				ierr = physics->ComputeSensitivities(opt); CHKERRQ(ierr);
				//==========================================================================

				//===========================================================================
				// gather senstitivities on to the zeroeth processor
					 fea::MyGather mygather(opt->dfdx);
					 double compliance = opt->fx;
				//===========================================================================


				 // Leasst squares, optimization, and advection on rank 0
				 if(rank == 0)
				 {
					 //===========================================================================
					 std::vector<double> aggSensi; double maxVal; int radius = 12;
						level_set_3d.ComputeAggregationSensitivities(aggSensi, maxVal, radius);
					 //===========================================================================
					 // Interpolate sensitivities
							std::vector<double> comSens(boundary.numTriangles, 0.0);
							boundary.InterpolateBoundarySensitivities(comSens, mygather.sensiArray,
								volfrac_to_petsc_map, 2);

							std::vector<double> aggSens(boundary.numTriangles, 0.0);
							double* aggSensiPointer = &aggSensi[0];
							boundary.InterpolateBoundarySensitivities(aggSens, aggSensiPointer,
								volfrac_to_petsc_map, 2, 0,0);

								std::cout << "Completed sensitivity interpolation." << std::endl;
						//===========================================================================
						// Optimize boundary velocities
						 double move_limit = 0.2;
						 // if(itr > 50) move_limit = 0.1;
						 // boundary.Optimize(bsens, move_limit, level_set_3d.volume, myInit.volCons, 0);

						std::vector<double> volSens(boundary.numTriangles, 1.0);

						// constraints
						int nCons = 2;
						std::vector<std::vector<double>> conSens;
						conSens.resize(nCons);
						std::vector<double> conMaxVals(nCons);
						std::vector<double> conCurVals(nCons);
						// volume constraint
						conSens[0] = volSens;
						conMaxVals[0] = myInit.volCons*nelx*nely*nelz;
						conCurVals[0] = level_set_3d.volume;
						// aggregation constraint
						conSens[1] = aggSens;
						conMaxVals[1] = 0.5;
						conCurVals[1] = maxVal;
						 boundary.Optimize(comSens,
							conSens, conMaxVals, conCurVals,
							move_limit, 1);

							std::cout << "Completed boundary velocities optimization. " << std::endl;
					 //========================================================
					 // Update boundary
						boundary.ExtrapolateVelocities();
						level_set_3d.Update();
						level_set_3d.MakeDomainHoles();
						//========================================================
					 double iter_end_time = MPI_Wtime();
					 std::cout << "Completed update. Iteration time = " <<
					 iter_end_time - iter_start_time << std::endl;

					 // Print out stl every 2 iterations
					 if(fmod( itr , 4 ) == 0)	{
							 boundary.WriteSTL("Topology0.stl");
							}
						if(fmod( itr , 4 ) == 2)	{
							 boundary.WriteSTL("Topology1.stl");
							}
					 std::cout  <<"Iter = "<< itr <<"; Vol Cons = " <<  level_set_3d.volume / nelx / nely / nelz <<
					 "; Compliance = " << compliance << "\n"<< std::endl ;
				 }

				 MPI_Barrier(PETSC_COMM_WORLD);
			}

			delete opt;
			delete physics;

			// Finalize PETSc / MPI
			PetscFinalize();
			return 0;
}
