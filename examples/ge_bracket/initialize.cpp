#include "initialize.h"

namespace PARA_FEA {

InitializeOpt::InitializeOpt(){

  nelx = 160; nely = 80; nelz = 80;

  nlvls = 4;

  lx = 2.0; ly = 1.0; lz = 1.0;

  maxItr = 401;


  Emax = 1.0; Emin = 1.0e-9; nu = 0.3;
  volCons = 0.15;

  double a = lx/nelx;
  double b = ly/nely;
  double c = lz/nelz;
  double eps = 0.1*std::min({a,b,c});//small value
  double infi = 10.0*std::max({lx,ly,lz});


  loads.push_back(Force( 0.0, 1.0, 0.0,
                  0.5*lx - 20*a, ly - 14*b, lz - 25*c,
                  1.0*a+eps, eps, eps));
  //
  loads.push_back(Force( 0.0, -1.0, 0.0,
                  0.5*lx + 20*a, ly - 14*b, lz - 25*c,
                  1.0*a+eps, eps, eps));
  //
  loads.push_back(Force( 0.0, 1.0, 0.0,
                  0.5*lx - 20*a, ly - 25*b, lz - 14*c,
                  1.0*a+eps, eps, eps));
  //
  loads.push_back(Force( 0.0, -1.0, 0.0,
                  0.5*lx + 20*a, ly - 25*b, lz - 14*c,
                  1.0*a+eps, eps, eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  14*a, 14*a, 0,
                  2*a+eps, 12*a+eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx - 14*a, 14*a, 0,
                  2*a+eps, 12*a+eps, eps));
  //

  fixedDofs.push_back(FixedDof(0, 0, 0,
                  14*a, 0.5*ly+14*b , 0,
                  2*a+eps, 12*a+eps, eps));
  //
  fixedDofs.push_back(FixedDof(0, 0, 0,
                  lx - 14*a, 0.5*ly+14*b, 0,
                  2*a+eps, 12*a+eps, eps));
  //

  double rFix = 9;
  double sFix = 5;
  double hFix = 5;
  // Fixed Blobs
  static PARA_LSM::Cylinder cylinderA(0.5*nelx-20.0, 1*nely -rFix - sFix, nelz-rFix - sFix,
              hFix, rFix+sFix, rFix,  0);
  // Fixed Blobs
  static PARA_LSM::Cylinder cylinderB(0.5*nelx+20.0, 1*nely -rFix - sFix, nelz-rFix - sFix,
              hFix, rFix+sFix, rFix,  0);

  static PARA_LSM::Cylinder cylinderC(0.5*nelx, 1*nely -rFix - sFix, nelz-rFix - sFix,
              40.0, rFix, 0.0,  0);

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderA));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderB));

  double rHole = 9;
  double sHole = 5;
  static PARA_LSM::Cylinder cylinder1(0.0*nelx+ rHole + sHole, 0*nely +  rHole + sHole, 5.0,
              5.0, rHole, 0.0, 2);
  //
  static PARA_LSM::Cylinder cylinder2(1.0*nelx - (rHole + sHole), 0*nely +  rHole + sHole, 5.0,
              5.0, rHole,0.0,2);
  //
  static PARA_LSM::Cylinder cylinder3(1.0*nelx - (rHole + sHole), 0.5*nely +  rHole + sHole, 5.0,
              5.0, rHole, 0.0,2);
  //
  static PARA_LSM::Cylinder cylinder4(0.0*nelx + (rHole + sHole), 0.5*nely +  rHole + sHole, 5.0,
              5.0, rHole, 0.0,2);



  static PARA_LSM::Cylinder cylinder5(0.0*nelx+ rHole + sHole, 0*nely +  rHole + sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);
  //
  static PARA_LSM::Cylinder cylinder6(1.0*nelx - (rHole + sHole), 0*nely +  rHole + sHole, 5.0,
              5.0, rHole+ sHole,rHole,2);
  //
  static PARA_LSM::Cylinder cylinder7(1.0*nelx - (rHole + sHole), 0.5*nely +  rHole + sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);
  //
  static PARA_LSM::Cylinder cylinder8(0.0*nelx + (rHole + sHole), 0.5*nely +  rHole + sHole, 5.0,
              5.0, rHole+ sHole, rHole,2);


  static PARA_LSM::Cuboid cube0(0*nelx, 0*nely, 1.0*nelz,
              40.0,  80.0, 40);
  static PARA_LSM::Cuboid cube1(1*nelx, 0*nely, 1.0*nelz,
              40.0,  80.0, 40);
  static PARA_LSM::Cuboid cube2(0.5*nelx, 1*nely, 1.0*nelz,
              15.0,  80.0, 50);
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  initialVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderC));

  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder1));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder2));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder3));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder4));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube0));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube1));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cuboid> (cube2));
  domainVoids.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinderC));

  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder5));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder6));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder7));
  fixedBlobs.push_back(std::make_unique<PARA_LSM::Cylinder> (cylinder8));

}


Force::Force() :
valx(0), valy(0), valz(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

Force::Force(double valx_, double valy_, double valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}


FixedDof::FixedDof(bool valx_, bool valy_, bool valz_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
valx(valx_), valy(valy_), valz(valz_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

HeatLoad::HeatLoad() :
val(0),
x(0), y(0), z(0),
tolx(0), toly(0), tolz(0) {

}

HeatLoad::HeatLoad(double val_,
  double x_, double y_, double z_,
double tolx_, double toly_, double tolz_) :
val(val_),
x(x_), y(y_), z(z_),
tolx(tolx_), toly(toly_), tolz(tolz_) {

}

} // namespace PARA_FEA