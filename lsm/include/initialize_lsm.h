//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef INITIALIZELSM_H
#define INITIALIZELSM_H

#include <memory>
#include <vector>

#include "lsm_3d.h"

/*! \file initialize_lsm.h
    \brief A file that contains the class needed for declaring level set objects
*/

namespace PARA_LSM {

/*! \class InitializeLsm
    \brief Serves as an input file type object for the level set method settings
    and defining voids in the level set.
*/
class InitializeLsm {
  public:
    using BlobPtr = std::unique_ptr<Blob> ;

    //! Default constructor
    InitializeLsm () ;

    /*! \name Dimensionality*/
    ///\{
    int nelx ; //!< number of elements in the x direction
    int nely ; //!< number of elements in the y direction
    int nelz ; //!< number of elements in the z direction
    ///\}

    /*! \name Initialization of voids*/
    ///\{
    std::vector<BlobPtr> initialVoids ; //!< vector of pointers to the initial holes
    std::vector<BlobPtr> domainVoids ;  //!< vector of pointers to the domain holes
    std::vector<BlobPtr> fixedBlobs ;   //!< vector of pointers to the fixed holes
    ///\}

    /*! \name Optimization parameters*/
    ///\{
    //! If true, applies a volume constraint for a multi-constrained problem
    bool is_volume_con ;
    double volume_con ; //!< volume constraint value (between 0 and 1)
    std::vector<double> max_val_con ; //!< max constraint values (excluding volume)
    double perturbation ; //!< Size of perturbation for sensitivity calculation

    //! Optimization algorithm.
    /*! 0 for Newton Raphson, 1 for MMA (Method of Moving Asymptotes) using the
        NLopt library, 2 for Simplex.*/
    int opt_algo ;
    ///\}
} ;

} // namespace PARA_LSM

#endif