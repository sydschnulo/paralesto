//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "optimizer_wrapper.h"

#include <vector>

#include "optimize.h"

namespace PARA_LSM {

OptimizerWrapper :: OptimizerWrapper (int num_cons_,
    std::vector<double> max_cons_vals_, int opt_algo_) : num_cons (num_cons_),
    max_cons_vals (max_cons_vals_), opt_algo (opt_algo_) {
}


OptimizerWrapper :: ~OptimizerWrapper () {
}


std::vector<double> OptimizerWrapper :: Solve (std::vector<double> obj_sens,
    std::vector< std::vector<double> > cons_sens,
    std::vector<double> curr_cons_vals, bool is_print) {
  // Initiate myOptimizer
  int num_des_var = obj_sens.size () ;
  MyOptimizer my_opt (num_des_var, num_cons) ;

  // set bounds
  for (int i = 0 ; i < num_des_var ; i++) {
    my_opt.z_lo[i] = lower_lim[i] ;
    my_opt.z_up[i] = upper_lim[i] ;
    my_opt.z[i]    = (0.5*my_opt.z_lo[i] + 0.5*my_opt.z_up[i]) ;
  }

  // Objective gradient
  bool nroptFlag = (opt_algo == 0) ;
  if (nroptFlag) {
    for (int i = 0 ; i < num_des_var ; i++) my_opt.objFunGrad[i] = obj_sens[i] ;
  } else {
    for (int i = 0 ; i < num_des_var ; i++) my_opt.objFunGrad[i] = obj_sens[i] ;
  }

  // Constraint gradients
  for (int j = 0 ; j < num_cons ; j++) {
    for (int i = 0 ; i < num_des_var ; i++) {
      my_opt.conFunGrad[j][i] = cons_sens[j][i] ;
    }
    my_opt.conMaxVals[j] = max_cons_vals[j] - curr_cons_vals[j] ;
  }

  // Solve the optimization problem
  if (opt_algo == 0) my_opt.SolveWithNR () ;
  if (opt_algo == 1) my_opt.SolveWithNlopt () ;
  if (opt_algo == 2) my_opt.SolveWithSimplex () ;

  // Assign optimum velocity values
  std::vector<double> opt_vel (num_des_var, 0.0) ;
  for (int i = 0 ; i < num_des_var ; i++) {
    opt_vel[i] = my_opt.z[i] ;
  }

  return opt_vel ;
}


void OptimizerWrapper :: SetLimits (std::vector<double> upper_lim_,
    std::vector<double> lower_lim_) {
  upper_lim = upper_lim_ ;
  lower_lim = lower_lim_ ;
}

} // namepsace PARA_LSM