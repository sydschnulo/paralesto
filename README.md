# ParaLeSTO
A parallel level set topology optimization code

(ParaLeSTO): Parallel Level Set Topology Optimization using PETSc

This code is built using the following libraries
1. [PETSc](https://www.mcs.anl.gov/petsc/download/index.html)
2. [TopOpt in PETSc](https://github.com/topopt/TopOpt_in_PETSc)
3. [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)
4. [NLOPT](https://nlopt.readthedocs.io/en/latest/)
5. [GLKP](https://www.gnu.org/software/glpk/)

----------------------------------
Instructions for installing PETsc
----------------------------------
1. Unzip petsc-3.7.7.tar.gz in the ParaLeSTO folder
2. Configure petsc: navigate to petsc-3.7.7 folder and install petsc using the following command 
```
    ./configure --with-cc=gcc --with-cxx=g++ --with-fc=gfortran --download-mpich --download-fblaslapack
```
3. Follow instructions on the terminal that appear after configuring petsc for installation
 
----------------------------------------
Instructions for preparing the makefile 
----------------------------------------
1. Navigate to paralesto/examples/agave folder 
2. Set up PETSC_DIR where petsc is installed
3. Set up EIGEN_DIR where Eigen is installed
4. Set up LIBOTHER where NLOPT and GLPK are installed

-----------------------------------
Instructions for running ParaLeSTO
-----------------------------------
1. Navigate to `paralesto/examples/agave` folder 
2. Make the executive using the following command
```
    make topopt
```
3. Run the program on let's say 8 cores using the following command (replace PETSC_DIR with the appropriate path)
```
    [PETSC_DIR]/arch-linux2-c-debug/bin/mpiexec -n 8 ./topopt.out
```

-----------------------------------------
Instructions for generating Doxygen files
-----------------------------------------
1. Install Doxygen. See the [Doxygen Installation](https://www.doxygen.nl/manual/install.html) guide for in-depth instructions.
2. Execute the following command
```
    doxygen Doxyfile
```
    This will generate a folder `doc` that contains Doxygen html and LaTeX files