//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "wrapper_io.h"

#include <iostream>
#include <sstream>
#include <string>

namespace PARALESTO {

WrapperIO :: WrapperIO (PARA_LSM::LevelSetWrapper& lsm_wrap_,
    PARA_FEA::PhysicsWrapper& phys_wrap_) : lsm_wrap (lsm_wrap_),
    phys_wrap (phys_wrap_) {
}

void WrapperIO :: PrintMultiPhysIter (int curr_iter) {
  double vol_cons = lsm_wrap.level_set_ptr->volume /
                    double (lsm_wrap.nelx*lsm_wrap.nely*lsm_wrap.nelz) ;
  double compliance = phys_wrap.opt_ptr->fx ;
  double th_compliance = phys_wrap.opt_poisson_ptr->fx ;
  std::cout << "Iter = " << curr_iter << "; Vol Cons = " << vol_cons <<
    "; Compliance = " << compliance << "; ThermalCompliance = " <<
    th_compliance << std::endl ;
}

void WrapperIO :: WriteStl (int curr_iter, std::string file_name,
    std::string file_path) {
  std::ostringstream path, num ;
  num.str ("") ;
  num.width (4) ;
  num.fill ('0') ;
  num << std::right << curr_iter ;
  path.str ("") ;
  if (file_path.empty ()) path << file_name << num.str () << ".stl" ;
  else path << file_path << "/" << file_name << num.str () << ".stl" ;

  lsm_wrap.boundary_ptr->WriteSTL (path.str ()) ;
}

} // namespace PARALESTO