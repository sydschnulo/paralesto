/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef TOPOPT1D_H
#define TOPOPT1D_H

#include <petsc.h>
//#include <petsc-private/dmdaimpl.h>
#include <petsc/private/dmdaimpl.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include "initialize.h"

/*! \file topopt1d.h
    \brief A file that has the class for formulating 1D topology optimization
    problems? /todo TODO(Carolina): check this description
*/

namespace PARA_FEA {

/*! \class TopOpt1D
    \brief Parameter container for the topology optimization problem

        min_x fx
        s.t. gx_j <= 0, j=1..m
             xmin_i <= x_i <= xmax_i, i=1..n

    with filtering and a volume constraint
*/
class TopOpt1D {
  public:
  	//! Constructor
    /*! \param myInit
          Initialization object for the input file
    */
  	TopOpt1D (InitializeOpt &myInit) ;

    //! Destructor
    ~TopOpt1D () ;

  	/*! \name Physical domain variables*/
    ///\{
  	PetscScalar xc[6]; //!< Domain coordinates
  	PetscScalar dx ;   //!< Element size in the x direction
    PetscScalar dy ;   //!< Element size in the y direction
    PetscScalar dz ;   //!< Element size in the z direction
  	PetscInt nxyz[3] ; //!< Number of nodes in each direction
  	PetscInt nlvls ;   //!< Number of multigrid levels
  	PetscScalar nu ;   //!< Poisson's ratio
  	DM da_nodes ;      //!< Nodal mesh (contains physics)
  	DM da_elem ;       //!< element mesh (contains design)
    ///\}

  	/*! \name Optimization parameters*/
    ///\{
  	PetscInt n ;          //!< Total number of design variables
  	PetscInt nloc ;       //!< Local number of local nodes?
  	PetscInt m ;          //!< Number of constraints
  	PetscInt itr ;        //!< Iteration number
  	PetscScalar fx ;      //!< Objective value
  	PetscScalar fscale ;  //!< Scaling factor for objective
  	PetscScalar *gx ;     //!< Array with constraint values
  	PetscScalar Xmin ;    //!< Min. value of design variables
  	PetscScalar Xmax ;    //!< Max. value of design variables
  	PetscScalar movlim ;  //!< Max. change of design variables
  	PetscScalar volfrac ; //!< Volume fraction
  	PetscScalar penal ;   //!< Penalization parameter
  	PetscScalar rmin ;    //!< filter radius
  	PetscInt maxItr ;     //!< Max iterations
  	PetscInt filter ;     //!< Filter type
  	Vec x ;               //!< Design variables
  	Vec dfdx ;            //!< Sensitivities of objective
    ///\}

    PetscScalar Kmin ; // Minimum coefficient of conductivity
    PetscScalar Kmax ; // Maximum coefficient of conductivity

  	/*! \name Thermal variables*/
    ///\{
  	Vec DeltaTVec ;    //!< delta t at element centroids
  	Vec DeltaTVecAdj ; //!< adjoint delta t at element centroids
  	Vec DeltaTVecFlux; //!< delta t at element centroids for flux computation
  	double hSurfConv ; //!< Surface convection
  	double Tambi ;     //!< Ambient temperature? \todo TODO(Carolina): check description
    ///\}

  	//! Helps to convert area based surface convection to a volume based load
    std::vector<Vec> areaToVolume ;

  private:
  	//! Allocate and set default values
  	PetscErrorCode SetUp (InitializeOpt &myInit) ;

  	//! \todo TODO(Carolina): add description
    PetscErrorCode SetUpMESH () ;

    //! \todo TODO(Carolina): add description
  	PetscErrorCode SetUpOPT () ;
} ;

} // namespace PARA_FEA

#endif