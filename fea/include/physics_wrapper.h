//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef PHYSICSWRAPPER_H
#define PHYSICSWRAPPER_H

#include <vector>
#include <memory>

#include "initialize.h"
#include "linear_elasticity.h"
#include "petsc.h"
#include "poisson.h"
#include "topopt.h"
#include "topopt1d.h"
#include "wrapper.h"

/*! \file physics_wrapper.h
    \brief A file that contains the class for wrapping the finite element
    analysis of the topology optimization framework into a single object.
*/

namespace PARALESTO {
class WrapperIO ; // forward declare wrapper class in appropriate namespace
} // namespace PARALESTO

namespace PARA_FEA {

/*! \class PhysicsWrapper
    \brief Wrapper for all the physics modeling (finite element analysis and
    sensitivity calculation) of the topology optimization framework into a
    single object
*/
class PhysicsWrapper {
  friend class PARALESTO::WrapperIO ;

  public:
    using TopOptPtr     = std::unique_ptr<TopOpt> ;
    using LinElasticPtr = std::unique_ptr<LinearElasticity> ;
    using TopOpt1DPtr   = std::unique_ptr<TopOpt1D> ;
    using PoissonPtr    = std::unique_ptr<Poisson> ;
    using WrapperPtr    = std::unique_ptr<Wrapper> ;

    //! Constructor
    /*! \param phys_init
          Initialization object for the physics (FEA) input file.
    */
    PhysicsWrapper (InitializeOpt &phys_init) ;

    //! Destructor
    ~PhysicsWrapper () ;

    //! Calculate the derivative of the objective with respect to density.
    /*! \param densities
          Vector of element densities.

        \param is_print
          Prints out progress statements during method runtime. Useful for
          debugging.

        \return
          Vector of sensitivities (df/drho) for each element.

        \note This method currently assumes that the partials are for compliance
    */
    std::vector<double> CalculatePartialDerivatives (
      std::vector<double> densities, bool is_print = false) ;

  private:
    //! Assigns volume fractions to PETSc mesh from provided vector
    /*! \param densities
          Vector of element densities
    */
    void AssignVolumeFractions (std::vector<double> densities) ;

    //! Calculate density sensitivities (partials) for multi-physics problem
    /*! This method couples heat conduction and linear elasticity. It assumes
        two separate load cases (strucutral and thermal) and computes the
        compliance sensitivities for each load case and adds them together.

        \param elem_sens
          Vector of compliance sensitivities with respect to element density
    */
    PetscErrorCode CalculateMultiPhysicsSeparateLoads (
        std::vector<double>& elem_sens) ;

    int nelem ; //!< number of elements in the mesh

    //! Variable for storing rank of the calling process in the communicator
    PetscMPIInt rank ;

    /*! \name PETSc volume fraction mapping */
    ///\{
    //! Volume fractions for PETSc mesh
    std::vector<double> petsc_volfrac = std::vector<double> (1, 0.2) ;

    //! Map between volume fractions on the level set grid and PETSc mesh
    std::vector<int> lsm_to_petsc_map = std::vector<int> (1, 0) ;
    ///\}

    /*! \name Pointers to finite element analysis objects */
    ///\{
    TopOptPtr opt_ptr ;           //!< Pointer to (linear elastic) optimization object
    LinElasticPtr phys_lin_ptr ;  //!< Pointer to linear elastic physics object
    TopOpt1DPtr opt_poisson_ptr ; //!< Pointer to (poisson) optimization object
    PoissonPtr phys_poisson_ptr ; //!< Pointer to poisson physics object
    WrapperPtr wrapper_ptr ;      //!< Pointer to PETSc wrapper object
    ///\}
} ;

} // namespace PARA_FEA

#endif