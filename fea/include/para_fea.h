//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef PARAFEA_H
#define PARAFEA_H

#include "initialize.h"
#include "linear_elasticity.h"
#include "physics_wrapper.h"
#include "poisson.h"
#include "topopt.h"
#include "topopt1d.h"
#include "wrapper.h"

#endif