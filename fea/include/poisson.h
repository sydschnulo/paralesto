/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef POISSON_H
#define POISSON_H

#include <petsc.h>
//#include <petsc-private/dmdaimpl.h>
#include <petsc/private/dmdaimpl.h>
#include <iostream>
#include <math.h>
#include "topopt1d.h"
#include <vector>
#include "initialize.h"

/*! \file poisson.h
    \brief A file that has the class for poisson problem physics
*/

namespace PARA_FEA {

/*! \class Poisson
    \brief Defines poisson problem physics
*/
class Poisson {
  public:
    //! Constructor
    /*! \param opt
          Optimization object

        \param myInit
          Initialization object for the input file
    */
    Poisson (TopOpt1D *opt, InitializeOpt &myInit) ;

    //! Destructor
    ~Poisson () ;

    //! Compute objective and constraints and sensitivities at once:
    /*! GOOD FOR SELF_ADJOINT PROBLEMS

        \param opt
          Optimization object
    */
    PetscErrorCode ComputeSensitivities (TopOpt1D *opt) ;

    //! \todo TODO(Carolina): add description
    /*! \param opt
          Optimization object

        \param withConvection
          Whether heat problem uses convection. False by default. \todo
          TODO(Carolina): check this description
    */
    PetscErrorCode ComputeCentroidTemperatures (TopOpt1D *opt,
      bool withConvection = false) ;

    //! Compute sensitivites for the adjoint solution
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeAdjointSensitivities (TopOpt1D *opt) ;

    //! Compute the adjoint load
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeAdjointLoadFromCentroids (TopOpt1D *opt) ;

    //! \todo TODO(Caroina): add description
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeSurfaceConvectionSensitivities (TopOpt1D *opt) ;

    //! Solve the FE problem
    /*! \param opt
          Optimization object

        \param withConvection
          Whether heat problem uses convection. False by default. \todo
          TODO(Carolina): check this description
    */
    PetscErrorCode SolveState (TopOpt1D *opt, bool withConvection = false) ;

    /*! \name For solving [K][U] = [RHS]*/
    ///\{
    PetscScalar KE[8*8] ; //!< Element stiffness matrix
    Mat K ;        //!< Global stiffness matrix
    Vec U ;        //!< Temperature vector
    Vec RHS ;      //!< Load vector
    Vec N ;        //!< Dirichlet vector (used when imposing BCs)
    KSP ksp ;      //!< Pointer to the KSP object i.e. the linear solver+preconditioner
    Vec HeatLoad ; //!< heat load vector
    Vec Tgiven;    //!< heat load vector
    ///\}

    /*! \name For stress and other adjoint related objective functions*/
    ///\{
    Vec adU ;   //!< adjoint temp vector
    Vec adRHS ; //!< adjoint load vector
    ///\}

    PetscScalar myRTOL = 1.0e-9 ;

  private:
    //! Set up the FE mesh and data structures
    /*! \param opt
          Optimization object

        \param myInit
          Initialization object for the input file
    */
    PetscErrorCode SetUpLoadAndBC (TopOpt1D *opt, InitializeOpt &myInit) ;

    //! Solve the adjoint FE problem
    /*! \param opt
          Optimization object
    */
    PetscErrorCode SolveAdjointState (TopOpt1D *opt) ;

    //! Assemble the stiffness matrix
    /*! \param opt
          Optimization object
    */
    PetscErrorCode AssembleStiffnessMatrix (TopOpt1D *opt) ;

    //! Assemble surface convection matrices
    /*! Assembles stiffness matrix and forces considering surface convection

        \param opt
          Optimization object
    */
    PetscErrorCode AssembleSurfaceConvection (TopOpt1D *opt) ;

    //! Start the solver
    /*! \param opt
          Optimization object
    */
    PetscErrorCode SetUpSolver (TopOpt1D *opt) ;

    //! Routine that doesn't change the element type upon repeated calls
    /*! \param dm

        \param nel

        \param nen

        \param e
    */
    PetscErrorCode DMDAGetElements_3D (DM dm, PetscInt *nel, PetscInt *nen,
      const PetscInt *e[]) ;

    // Methods used to assemble the element stiffness matrix
    /*! \param X

        \param Y

        \param Z

        \param nu

        \param redInt

        \param ke
    */
    PetscInt Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z,
      PetscScalar nu, PetscInt redInt, PetscScalar *ke) ;

    //! \todo TODO(Carolina): add description
    /*! \param v1
        \param v2
        \param l
    */
    PetscScalar Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l) ;

    //! \todo TODO(Carolina): add description
    /*! \param xi

        \param eta

        \param zeta

        \param dNdxi

        \param dNdeta

        \param dNdzeta
    */
    void DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta,
      PetscScalar zeta, PetscScalar *dNdxi, PetscScalar *dNdeta,
      PetscScalar *dNdzeta) ;

    //! \todo TODO(Carolina): add description
    /*! \param J

        \param invJ
    */
    PetscScalar Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]) ;

    //! Bmatrix at centroid
    /*! \note Small enough to be allocated on stack
    */
    PetscScalar Bmatrix[3][8] ;

    /*! \name Auxiliary variables*/
    ///\{
    PetscInt dim = 1 ;       //!< dimensionality for degrees of freedom per node
    PetscInt spacedim = 3 ;  //!< dimensionality of the problem, i.e. 3D
    double areaScaleFactor ;
    ///\}
} ;

} // namespace PARA_FEA

#endif