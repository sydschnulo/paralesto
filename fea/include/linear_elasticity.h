/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef LINEAR_ELASTICITY_H
#define LINEAR_ELASTICITY_H

#include <petsc.h>
//#include <petsc-private/dmdaimpl.h>
#include <petsc/private/dmdaimpl.h>
#include <iostream>
#include <math.h>
#include "topopt.h"
#include<vector>
#include "initialize.h"

/*! \file linear_elasticity.h
    \brief A file that has the class linear elastic physics
*/

namespace PARA_FEA {

/*! \class LinearElasticity
    \brief Defines linear elastic physics
*/
class LinearElasticity {
  public:
    //! Constructor
    /*! \param opt
          Optimization object

        \param myInit
          Initialization object for the input file
    */
    LinearElasticity (TopOpt *opt, InitializeOpt &myInit) ;

    //! Destructor
    ~LinearElasticity () ;

    //! Compute objective and constraints and sensitivities at once
    /*! GOOD FOR SELF_ADJOINT PROBLEMS

        \param opt
          Optimization object
    */
    PetscErrorCode ComputeSensitivities (TopOpt *opt) ;

    //! Compute stress
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeStress (TopOpt *opt) ;

    //! Stress sensitivities
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeStressSensitivitiesSandy (TopOpt *opt) ;

    //! Stress sensitivities from custom quadrature
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeStressSensitivitiesFromQuadrature (TopOpt *opt) ;

    //! Thermal rotation sensitivities
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeThRotSensitivities (TopOpt *opt) ;

    //! Solve state problem if only the thermal load is applied /todo
    //! TODO(Carolina): double check the description
    /*! \param opt
          Optimization object
    */
    PetscErrorCode SolveThermalOnlyState (TopOpt *opt) ;

    //! Solve adjoint FE problem
    /*! \param opt
          Optimization object
    */
    PetscErrorCode SolveAdjointState (TopOpt *opt) ;

    //! Compute element displacements
    /*! \param opt
          Optimization object

        \param dirArr
    */
    PetscErrorCode ComputeElemDirDisp (TopOpt *opt, std::vector<double> dirArr) ;

    //! Compute element displacements
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeElemMagDisp (TopOpt *opt) ;

    //! Compute objective and constraints and sensitivities using custom quadrature
    /*! \param opt
          Optimization object
    */
    PetscErrorCode ComputeSensitivitiesFromQuadrature (TopOpt *opt) ;

    //! Routine that doesn't change the element type upon repeated calls
    /*! \param dm

        \param nel

        \param nen

        \param e
    */
    PetscErrorCode DMDAGetElements_3D (DM dm, PetscInt *nel, PetscInt *nen,
      const PetscInt *e[]) ;

    /*! \name For solving [K][U] = [RHS]*/
    ///\{
    PetscScalar KE[24*24] ; //!< Element stiffness matrix
    Mat K ;   //!< Global stiffness matrix
    Vec U ;   //!< Displacement vector
    Vec RHS ; //!< Load vector
    Vec N ;   //!< Dirichlet vector (used when imposing BCs)
    KSP ksp ;	//!< Pointer to the KSP object i.e. the linear solver+preconditioner
    ///\}

    /*! \name For stress and other adjoint related objective functions*/
    ///\{
    Vec adU ;   //!< Adjoint solution
    Vec adRHS ; //!< Adjoint load
    ///\}

    /*! \name temporary*/
    ///\{
    Vec RHS1 ; //!< load vector
    Vec RHS2 ; //!< load vector
    ///\}

    PetscScalar myRTOL = 1.0e-5 ;

  private:
    //! Set up the FE mesh and data structures
    /*! \param opt
          Optimization object

        \param myInit
          Initialization object for the input file
    */
    PetscErrorCode SetUpLoadAndBC (TopOpt *opt, InitializeOpt &myInit) ;

    //! Solve the FE problem
    /*! \param opt
          Optimization object

        \param useQuadrature
    */
    PetscErrorCode SolveState(TopOpt *opt, bool useQuadrature = false);

    //! Assemble the stiffness matrix
    /*! \param opt
          Optimization object
    */
    PetscErrorCode AssembleStiffnessMatrix (TopOpt *opt) ;

    // Assemble Stiffness matrix from quadrature
    /*! \param opt
          Optimization object
    */
    PetscErrorCode AssembleStiffnessMatrixFromQuadrature (TopOpt *opt) ;

    //! Start the solver
    /*! \param opt
          Optimization object
    */
    PetscErrorCode SetUpSolver (TopOpt *opt) ;

    /*! \name Methods used to assemble the element stiffness matrix*/
    ///\{

    //! Calculates a HEX8 isoparametric element stiffness matrix
    /*! Note that due to the fact that the mesh is structured and the elements
        are linear, the element stiffness matrix is constant for all elements.
        The element stiffness matrix is computed as:
          ke = int(int(int(B^T*C*B,x),y),z)
        For an isoparameteric element this integral becomes:
          ke = int(int(int(B^T*C*B*det(J),xi=-1..1),eta=-1..1),zeta=-1..1)
        where B is the more complicated expression:
          B = [d_x*alpha1 + d_y*alpha2 + d_z*alpha3]*N
        where
          d_x = [invJ11 invJ12 invJ13]*[d_xi d_eta d_zeta]
          d_y = [invJ21 invJ22 invJ23]*[d_xi d_eta d_zeta]
          d_z = [invJ31 invJ32 invJ33]*[d_xi d_eta d_zeta]

        \note The elasticity modulus is left out of the calculation, because it
        is multiplied afterwards (the aim is topology optimization).

        \param X
          Vector containing the local x coordinates of the eight nodes (x0, x1,
          x2, x3, x4, x5, x6, x7). Where node 0 is the lower left corner, node 1
          is the next node in the counter-clockwise direction, etc. Node 0, 1,
          2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
          manner, but at the top of the element.

        \param Y
          Vector containing the local y coordinates of the eight nodes (y0, y1,
          y2, y3, y4, y5, y6, y7). Where node 0 is the lower left corner, node 1
          is the next node in the counter-clockwise direction, etc. Node 0, 1,
          2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
          manner, but at the top of the element.

        \param Z
          Vector containing the local z coordinates of the eight nodes (z0, z1,
          z2, z3, z4, z5, z6, z7). Where node 0 is the lower left corner, node 1
          is the next node in the counter-clockwise direction, etc. Node 0, 1,
          2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
          manner, but at the top of the element.

        \param nu
          Poisson's ratio.

        \param redInt
          Reduced integration option. If 0, use full integration. If 1, use
          reduced integration.

        \param ke
          Element stiffness matrix. \note Needs to be multiplied with elasticity
          modulus.
    */
    PetscInt Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z,
      PetscScalar nu, PetscInt redInt, PetscScalar *ke) ;

    //! \todo TODO(Carolina): add description
    /*! \param X

        \param Y

        \param Z

        \param nu

        \param redInt

        \param ke
    */
    std::vector<double> Hex8Isoparametric(PetscScalar *X, PetscScalar *Y,
      PetscScalar *Z, PetscScalar nu, PetscInt redInt,
      std::vector<double> quadratureWeights) ;

    //! Returns the dot product of two PetscScalar arrays
    /*! \note the two arrays much have the same length

        \param v1
          Array of length l

        \param v2
          Array of length l

        \param l
          Length of arrays v1 and v2

        \return
          Dot product of the two arrays
    */
    PetscScalar Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l) ;

    //! Computes differentiated shape functions at the point given by (xi, eta,
    //! zeta)
    /*! \param xi
          Isoparametric coordinate

        \param eta
          Isoparametric coordinate

        \param zeta
          Isoparametric coordinate

        \param dNdxi
          Derivative of shape function with respect to xi

        \param dNdeta
          Derivative of shape function with respect to eta

        \param dNdzeta
          Derivative of shape function with respect to zeta
    */
    void DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta,
      PetscScalar zeta, PetscScalar *dNdxi, PetscScalar *dNdeta,
      PetscScalar *dNdzeta) ;

    //! Calculates the inverse of a 3x3 matrix and returns its determinant.
    /*! \param J
          3x3 matrix.

        \param invJ
          Inverse of the 3x3 matrix J.

        \return
          Determinant of the matrix J.
    */
    PetscScalar Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]) ;
    ///\}

    /*! \name Added by Sandy*/
    ///\{
    PetscScalar BMatrix[6][24] ; //!< B-Matrix for stress computations
    std::vector<std::vector<double>> CPrivate ; //!< Constitutive matrix
    bool isBMatrixVectorComputed = false ;
    bool isVCBVectorComputed = false ;

    //! B-Matrix for stress computations
    std::vector< std::vector<std::vector<double>> > BMatrixVector ;

    //! VCB for stress computations
    std::vector< std::vector<std::vector<double>> > VCBVector ;
    ///\}

    /*! \name Thermal stuff added by Sandy*/
    ///\{
    std::vector<double> fThhermalElem ; //!< elemental thermal force
    Vec RHSthermal ;                    //!< Assembled thermal force
    ///\}
} ;

} // namespace PARA_FEA

#endif