//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef INITIALIZE_H
#define INITIALIZE_H

#include <vector>

#include "lsm_3d.h"

/*! \file initialize.h
    \brief A file that contains classes for declaring forces and boundary
    conditions.
*/

namespace PARA_FEA {

/*! \class Force
    \brief A class for applying structural (mechanical) loads
*/
class Force {
  public:
    //! Default constructor
    Force () ;

    //! Constructor
    /*! \param valx_
          Value of the applied load in the x direction

        \param valy_
          Value of the applied load in the y direction

        \param valz_
          Value of the applied load in the z direction

        \param x_
          Location of the applied load in the x direction

        \param y_
          Location of the applied load in the y direction

        \param z_
          Location of the applied load in the z direction

        \param tolx_
          Half-width of the length over which the loading is applied in the x
          direction. Assign a small (close to zero) value for a point load.

        \param toly_
          Half-width of the length over which the loading is applied in the y
          direction. Assign a small (close to zero) value for a point load.

        \param tolz_
          Half-width of the length over which the loading is applied in the z
          direction. Assign a small (close to zero) value for a point load.
    */
    Force (double valx_, double valy_, double valz_, double x_, double y_,
      double z_, double tolx_, double toly_, double tolz_) ;

    double valx ; //!< Value of the applied load in the x direction
    double valy ; //!< Value of the applied load in the y direction
    double valz ; //!< Value of the applied load in the z direction
    double x ;    //!< x coordinate of the applied load
    double y ;    //!< y coordinate of the applied load
    double z ;    //!< z coordinate of the applied load
    double tolx ; //!< Half-width of the applied load in the x direction
    double toly ; //!< Half-width of the applied load in the y direction
    double tolz ; //!< Half-width of the applied load in the z direction
} ;


/*! \class FixedDof
    \brief A class for applying boundary conditions
*/
class FixedDof {
  public:
    //! Default constructor
    FixedDof () ;

    //! Constructor
    /*! \param valx_
          If the degree of freedom is fixed or free in the x direction. False
          (0) for fixed and True (1) for free.

        \param valy_
          If the degree of freedom is fixed or free in the y direction. False
          (0) for fixed and True (1) for free.

        \param valz_
          If the degree of freedom is fixed or free in the z direction. False
          (0) for fixed and True (1) for free.

        \param x_
          Location of the boundary condition in the x direction

        \param y_
          Location of the boundary condition in the y direction

        \param z_
          Location of the boundary condition in the z direction

        \param tolx_
          Half-width of the length over which the boundary condition is applied
          in the x direction. Assign a small (close to zero) value for selecting
          a single node.

        \param toly_
          Half-width of the length over which the boundary condition is applied
          in the x direction. Assign a small (close to zero) value for selecting
          a single node.

        \param tolz_
          Half-width of the length over which the boundary condition is applied
          in the x direction. Assign a small (close to zero) value for selecting
          a single node.
    */
    FixedDof (bool valx_, bool valy_, bool valz_, double x_, double y_,
      double z_, double tolx_, double toly_, double tolz_) ;

    bool valx ;   //!< If the dof is fixed (0) or free (1) in the x direction
    bool valy ;   //!< If the dof is fixed (0) or free (1) in the y direction
    bool valz ;   //!< If the dof is fixed (0) or free (1) in the z direction
    double x ;    //!< x coordinate of the applied load
    double y ;    //!< y coordinate of the applied load
    double z ;    //!< z coordinate of the applied load
    double tolx ; //!< Half-width of the applied load in the x direction
    double toly ; //!< Half-width of the applied load in the y direction
    double tolz ; //!< Half-width of the applied load in the z direction
} ;


/*! \class HeatLoad
    \brief A class for applying heat loads
*/
class HeatLoad {
  public:
    //! Default constructor
    HeatLoad () ;

    //! Constructor
    /*! \param val_
          Value of the applied heat load

        \param x_
          Location of the applied heat load in the x direction

        \param y_
          Location of the applied heat load in the y direction

        \param z_
          Location of the applied heat load in the z direction

        \param tolx_
          Half-width of the length over which the loading is applied in the x
          direction. Assign a small (close to zero) value for a point load.

        \param toly_
          Half-width of the length over which the loading is applied in the y
          direction. Assign a small (close to zero) value for a point load.

        \param tolz_
          Half-width of the length over which the loading is applied in the z
          direction. Assign a small (close to zero) value for a point load.
    */
    HeatLoad (double val_, double x_, double y_, double z_, double tolx_,
      double toly_, double tolz_) ;

    double val ;  //!< Value of the applied heat load
    double x ;    //!< x coordinate of the applied heat load
    double y ;    //!< y coordinate of the applied heat load
    double z ;    //!< z coordinate of the applied heat load
    double tolx ; //!< Half-width of the applied heat load in the x direction
    double toly ; //!< Half-width of the applied heat load in the y direction
    double tolz ; //!< Half-width of the applied heat load in the z direction
} ;


/*! \class InitializeOpt
    \brief Serves as an input file type object for the finite element analysis
    settings and defining voids in the level set.
*/
class InitializeOpt {
  public:
    using BlobPtr = std::unique_ptr<PARA_LSM::Blob> ;

    //! Default constructor
    InitializeOpt () ;

    //! Constructor
    /*! \param isAux
    */
    InitializeOpt (bool isAux) ;

    //! Constructor
    /*! \param isAux

        \param isVapor
    */
    InitializeOpt (bool isAux, bool isVapor) ;

    /*! \name Dimensionality*/
    ///\{
    int nelx ;  //!< number of elements in the x direction
    int nely ;  //!< number of elements in the y direction
    int nelz ;  //!< number of elements in the z direction
    double lx ; //!< length of domain in x direction
    double ly ; //!< length of domain in y direction
    double lz ; //!< length of domain in z direction
    int nlvls ; //!< number of mulitgrid levels
    ///\}

    /*! \name Material properties*/
    ///\{
    double Emax ; //!< Elastic modulus of fully solid elements
    double Emin ; //!< Minimum elastic modulus for void elements
    double nu ;   //!< Poisson's ratio
    double Kmax ; //!< Conductivity coefficient of fully solid elements
    double Kmin ; //!< Minimum conductivity coefficient for void elements
    ///\}

    /*! \name Loading and Boundary Conditions*/
    ///\{
    std::vector<Force> loads ; //!< structural (mechanical) loads
    std::vector<Force> adjLoads ; //!< adjoint loads
    std::vector<FixedDof> fixedDofs ; //!< fixed structural dofs
    std::vector<HeatLoad> heatLoad ; //!< heat loads
    std::vector<HeatLoad> fixedHeatDof ; //!< fixed heat dofs
    ///\}

    /*! \name Initialization of voids*/
    ///\{
    //! \todo TODO(Carolina): remove these data members. Should be in initialize_lsm.h
    std::vector<BlobPtr> initialVoids ; //!< vector of pointers to the initial holes
    std::vector<BlobPtr> domainVoids ;  //!< vector of pointers to the domain holes
    std::vector<BlobPtr> fixedBlobs ;   //!< vector of pointers to the fixed holes
    ///\}

    int maxItr ;     //!< maximum number of iterations for optimization
    double volCons ; //!< volume constraint
} ;

} // namespace PARA_FEA

#endif