/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef TOPOPT_H
#define TOPOPT_H

#include <petsc.h>
//#include <petsc-private/dmdaimpl.h>
#include <petsc/private/dmdaimpl.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

#include "initialize.h"

/*! \file topopt.h
    \brief A file that has the class for formulating topology optimization
    problems
*/

namespace PARA_FEA {

/*! \class TopOpt
    \brief Parameter container for the topology optimization problem

        min_x fx
        s.t. gx_j <= 0, j=1..m
             xmin_i <= x_i <= xmax_i, i=1..n

    with filtering and a volume constraint
*/
class TopOpt {
  public:
  	//! Constructor
    /*! \param myInit
          Initialization object for the input file
    */
  	TopOpt (InitializeOpt &myInit) ;

    //! Deconstructor
    ~TopOpt () ;

  	/*! \name Domain size variables*/
    ///\{
  	PetscScalar xc[6]; //!< Domain coordinates
  	PetscScalar dx ;   //!< Element size in the x direction
    PetscScalar dy ;   //!< Element size in the y direction
    PetscScalar dz ;   //!< Element size in the z direction
  	PetscInt nxyz[3] ; //!< Number of nodes in each direction
    ///\}

    /*! Mesh variables */
    ///\{
    DM da_nodes ;      //!< Nodal mesh (contains physics)
    DM da_elem ;       //!< element mesh (contains design)
    ///\}

    /*! Material properties */
    ///\{
    PetscScalar Emin ; //!< Modified SIMP, minimum Young's modulus value
    PetscScalar Emax ; //!< Modified SIMP, maximum Young's modulus value
    PetscScalar nu ;   //!< Poisson's ratio
    ///\}

  	/*! \name Optimization parameters*/
    ///\{
  	PetscInt n ;          //!< Total number of design variables
  	PetscInt nloc	;       //!< Local number of local nodes?
  	PetscInt m ;          //!< Number of constraints
  	PetscInt itr ;        //!< Iteration number
  	PetscScalar fx ;      //!< Objective value
  	PetscScalar fscale ;  //!< Scaling factor for objective
  	PetscScalar *gx ;     //!< Array with constraint values
  	PetscScalar Xmin ;    //!< Min. value of design variables
  	PetscScalar Xmax ;    //!< Max. value of design variables
  	PetscScalar movlim ;  //!< Max. change of design variables
  	PetscScalar volfrac ; //!< Volume fraction
  	PetscScalar penal ;   //!< Penalization parameter
  	PetscScalar rmin ;    //!< filter radius
  	PetscInt maxItr ;     //!< Max iterations
  	PetscInt filter ;     //!< Filter type
  	Vec x ;               //!< Design variables
  	Vec dfdx ;            //!< Sensitivities of objective
    ///\}

    PetscInt nlvls ; //!< Number of multigrid levels for solver

  	/*! \name Stress Related variables*/
    ///\{
  	Vec ignoreElemStress ; //!< = 0 or 1, ignores the element stress
  	Vec elemMaxStress ;    //!< Maximum sensitivities of an element
    ///\}

  	/*! \name Thermal variables*/
    ///\{
  	Vec DeltaTVec ;      //!< delta t at element centroids
  	PetscScalar alphaT ; //!< CTE
  	Vec DeltaTVecAdj ;   //!< delta t at element centroids
    ///\}

  	// IO
  	Vec elemDisp ;

  	/*! \name Quadrature*/
    ///\{
  	std::vector<Vec> momQuad ; //!< Moment quadrature?
  	int numQuadPoints = 27 ;   //!< Number of quadrature points per element
    ///\}

  private:
  	//! Allocate and set default values
    /*! \param myInit
          Initialization object for the input file
    */
  	PetscErrorCode SetUp (InitializeOpt &myInit) ;

  	//! Creates mesh (nodes, elements) for the finite element analysis
    PetscErrorCode SetUpMESH () ;

    //! Creates mesh (nodes, elements) for the finite element analysis
    PetscErrorCode SetUpOPT () ;
} ;

} // namespace PARA_FEA

#endif