//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "physics_wrapper.h"

#include <algorithm>
#include <memory>
#include <vector>

#include "linear_elasticity.h"
#include "petsc.h"
#include "poisson.h"
#include "topopt.h"
#include "topopt1d.h"
#include "wrapper.h"

namespace PARA_FEA {

PhysicsWrapper :: PhysicsWrapper (InitializeOpt &phys_init) {
  nelem = phys_init.nelx * phys_init.nely * phys_init.nelz ;
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank) ;

  // Create objects for pointers
  opt_ptr          = std::make_unique<TopOpt> (phys_init) ;
  phys_lin_ptr     = std::make_unique<LinearElasticity> (opt_ptr.get (), phys_init) ;
  opt_poisson_ptr  = std::make_unique<TopOpt1D> (phys_init) ;
  phys_poisson_ptr = std::make_unique<Poisson> (opt_poisson_ptr.get (), phys_init) ;
  wrapper_ptr      = std::make_unique<Wrapper> () ;

  // Resize PETSc volume fraction vector
  if (rank == 0) {petsc_volfrac.resize (nelem, 0.2) ;}

  // Form map between level set to PETSc volume fractions
  wrapper_ptr->GetMap (opt_ptr.get (), lsm_to_petsc_map, nelem) ;
}


PhysicsWrapper :: ~PhysicsWrapper () {
  opt_ptr.release () ;
  phys_lin_ptr.release () ;
  opt_poisson_ptr.release () ;
  phys_poisson_ptr.release () ;
}


std::vector<double> PhysicsWrapper :: CalculatePartialDerivatives (
    std::vector<double> densities, bool is_print) {

  if (rank == 0 && is_print)
    std::cout << "Starting CalculatePartialDerivatives... " << std::endl ;

  // Assign volume fractions to PETSc mesh
  AssignVolumeFractions (densities) ;
  if (rank == 0 && is_print)
    std::cout << "   completed assign volume fraction" << std::endl ;
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Calculate sensitivities
  std::vector<double> elem_sens (nelem, 0.0) ;
  CalculateMultiPhysicsSeparateLoads (elem_sens) ;
  if (rank == 0 && is_print)
    std::cout << "   completed sensitivity calculation" << std::endl ;

  return elem_sens ;
}


void PhysicsWrapper :: AssignVolumeFractions (std::vector<double> densities) {
  if (rank == 0) {
    for (int i = 0 ; i < nelem ; i++) {
      petsc_volfrac[lsm_to_petsc_map[i]] = std::max (densities[i], 0.0) ;
    }
    for (int i = 0 ; i < nelem ; i++) {
      VecSetValue (opt_ptr->x, i, petsc_volfrac[i], INSERT_VALUES) ;
      VecSetValue (opt_poisson_ptr->x, i, petsc_volfrac[i], INSERT_VALUES) ;
    }
  }

  VecAssemblyBegin (opt_ptr->x) ; VecAssemblyEnd (opt_ptr->x) ;
  VecAssemblyBegin (opt_poisson_ptr->x) ; VecAssemblyEnd (opt_poisson_ptr->x) ;
}


PetscErrorCode PhysicsWrapper :: CalculateMultiPhysicsSeparateLoads (
    std::vector<double>& elem_sens) {
  //! Error code for debugging
  PetscErrorCode ierr ;

  // Compute compliance sensitivities for structural (linear elastic) load case
  phys_lin_ptr->myRTOL = 1.0e-5 ;
  ierr = phys_lin_ptr->ComputeSensitivities (opt_ptr.get ()) ;
  CHKERRQ (ierr) ;
  MyGather my_gather (opt_ptr->dfdx) ; // gather sensitivities to zeroeth processor

  // Compute compliance sensitivities for thermal (heat conduction) load case
  phys_poisson_ptr->myRTOL = 1.0e-6 ;
  ierr = phys_poisson_ptr->ComputeSensitivities (opt_poisson_ptr.get ()) ;
  CHKERRQ (ierr) ;
  MyGather my_gather_th (opt_poisson_ptr->dfdx) ; // gather sensitivities

  // Add sensitivities and assign them to a vector
  if (rank == 0) {
    for (int i = 0 ; i < nelem ; i++) {
      int index = lsm_to_petsc_map[i] ;
      double sens_val = 0.5*my_gather.sensiArray[index] +
                        0.5*my_gather_th.sensiArray[index] ;
      elem_sens[i] = sens_val ;
    }
  }

  return ierr ;
}

} // namespace PARA_LSM