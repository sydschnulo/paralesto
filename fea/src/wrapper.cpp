//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "wrapper.h"

namespace PARA_FEA {

void Wrapper :: GetMap (TopOpt *opt, std::vector<int> &volfrac_to_petsc_map,
    int size_of_elements) {
	// declare petsc int here for rank and size
	PetscMPIInt rank, size ;

	// define rank and size here
	MPI_Comm_rank (PETSC_COMM_WORLD,&rank) ;
	MPI_Comm_size (PETSC_COMM_WORLD,&size) ;

	//=============================================================================
	// Form the map between level set numbering and petsc numbering
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // create a natural vector
  Vec  my_natural_vector ;
	DMDACreateNaturalVector (opt->da_elem, &my_natural_vector) ;
	VecDuplicate (opt->x, &my_natural_vector) ;

  // Assign values
  if (rank == 0) {
    for (int i = 0 ; i < size_of_elements ; i++) {
      // vector , global id , map, insert
      VecSetValue (opt->x, i, 1.0*i, INSERT_VALUES) ;
    }
  }
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Assemble vector
  VecAssemblyBegin (opt->x) ;
  VecAssemblyEnd (opt->x) ;

  // Copy vector to the natural vector
  VecCopy (opt->x, my_natural_vector) ;

	// Convert Global to natural
  DMDAGlobalToNaturalBegin (opt->da_elem,opt->x,INSERT_VALUES,my_natural_vector) ;
  DMDAGlobalToNaturalEnd (opt->da_elem,opt->x,INSERT_VALUES,my_natural_vector) ;

  // Scatter to the zeroeth processor
  VecScatter ctx1 ;
  Vec vout_scatter ;
  VecScatterCreateToZero (my_natural_vector, &ctx1, &vout_scatter) ;
  VecScatterBegin (ctx1,my_natural_vector,vout_scatter,INSERT_VALUES,SCATTER_FORWARD) ;
  VecScatterEnd (ctx1,my_natural_vector,vout_scatter,INSERT_VALUES,SCATTER_FORWARD) ;
  VecScatterDestroy (&ctx1) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Copy values onto the vector volfrac_to_petsc_map
  if (rank == 0) {
    volfrac_to_petsc_map.resize (size_of_elements,0) ;

    for (int i = 0 ; i < size_of_elements ; i++) {
      PetscInt ni_get_vals = 1 ;
      PetscInt ix_get_vals[1] ;
      ix_get_vals[0] = i ;
      PetscScalar y_get_vals[1] ;

      VecGetValues (vout_scatter, ni_get_vals, ix_get_vals, y_get_vals) ;
      volfrac_to_petsc_map[i] = int (y_get_vals[0]) ;
    }
  }

  VecDestroy (&vout_scatter) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;
	//==============================================================================

	return ;
}


void Wrapper :: GetMap (TopOpt1D *opt, std::vector<int> &volfrac_to_petsc_map,
    int size_of_elements) {
	// declare petsc int here for rank and size
	PetscMPIInt rank, size ;

	// define rank and size here
	MPI_Comm_rank (PETSC_COMM_WORLD, &rank) ;
	MPI_Comm_size (PETSC_COMM_WORLD,&size) ;

	//=============================================================================
	// Form the map between level set numbering and petsc numbering
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // create a natural vector
  Vec  my_natural_vector ;
  DMDACreateNaturalVector (opt->da_elem, &my_natural_vector) ;
  VecDuplicate (opt->x, &my_natural_vector) ;

  // Assign values
  if (rank == 0) {
    for (int i = 0 ; i < size_of_elements ; i++) {
      // vector , global id , map, insert
      VecSetValue (opt->x, i, 1.0*i, INSERT_VALUES) ;
    }
  }
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Assemble vector
  VecAssemblyBegin (opt->x) ;
  VecAssemblyEnd (opt->x) ;

  // Copy vector to the natural vector
  VecCopy (opt->x, my_natural_vector) ;

  // Convert Global to natural
  DMDAGlobalToNaturalBegin (opt->da_elem,opt->x,INSERT_VALUES,my_natural_vector) ;
  DMDAGlobalToNaturalEnd (opt->da_elem,opt->x,INSERT_VALUES,my_natural_vector) ;

  // Scatter to the zeroeth processor
  VecScatter ctx1 ;
  Vec vout_scatter ;
  VecScatterCreateToZero (my_natural_vector, &ctx1, &vout_scatter) ;
  VecScatterBegin (ctx1,my_natural_vector,vout_scatter,INSERT_VALUES,SCATTER_FORWARD) ;
  VecScatterEnd (ctx1,my_natural_vector,vout_scatter,INSERT_VALUES,SCATTER_FORWARD) ;
  VecScatterDestroy (&ctx1) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Copy values onto the vector volfrac_to_petsc_map
  if (rank == 0) {
    volfrac_to_petsc_map.resize (size_of_elements,0) ;

    for (int i = 0 ; i < size_of_elements ; i++) {
      PetscInt ni_get_vals = 1 ;
      PetscInt ix_get_vals[1] ;
      ix_get_vals[0] = i ;
      PetscScalar y_get_vals[1] ;

      VecGetValues (vout_scatter, ni_get_vals, ix_get_vals, y_get_vals) ;
      volfrac_to_petsc_map[i] = int (y_get_vals[0]) ;
    }
  }

  VecDestroy (&vout_scatter) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;
	//==============================================================================

	return ;
}


MyGather :: MyGather (Vec vin) {
	// create a scatter
	VecScatter ctx ;

	//scatter to 0th procesoor
	VecScatterCreateToZero (vin, &ctx, &vout) ;
	VecScatterBegin (ctx,vin,vout,INSERT_VALUES,SCATTER_FORWARD) ;
	VecScatterEnd (ctx,vin,vout,INSERT_VALUES,SCATTER_FORWARD) ;

	// clean up scatter
	VecScatterDestroy (&ctx) ;

	// get the array
	VecGetArray (vout, &sensiArray) ;
}


MyGather :: ~MyGather () {
	// clean up Sensitivity array
	VecRestoreArray (vout,&sensiArray) ;
	VecDestroy (&vout) ;
}


void petsc_map :: GetMap (DM da, std::vector<int> &lsm_to_petsc_map,
    int vec_size) {
  // define rank here
  PetscMPIInt rank ;
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank) ;

  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Form the map between level set numbering and petsc numbering

  // Create a vector that has PETSc ordering
  Vec dummy_vector ;
  DMCreateGlobalVector (da, &dummy_vector) ;

  // create a natural vector
  Vec natural_vector ;
  DMDACreateNaturalVector (da, &natural_vector) ;
  VecDuplicate (dummy_vector, &natural_vector) ;

  // Assign values
  if (rank == 0) {
    for (int i = 0 ; i < vec_size ; i++) {
      // vector , global id , map, insert
      VecSetValue (dummy_vector, i, 1.0*i, INSERT_VALUES) ;
    }
  }
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Assemble vector
  VecAssemblyBegin (dummy_vector) ;
  VecAssemblyEnd (dummy_vector) ;

  // Copy vector to the natural vector
  VecCopy (dummy_vector, natural_vector) ;

  // Convert Global to natural
  DMDAGlobalToNaturalBegin (da, dummy_vector, INSERT_VALUES, natural_vector) ;
  DMDAGlobalToNaturalEnd (da, dummy_vector, INSERT_VALUES, natural_vector) ;

  // Scatter to the zeroeth processor
  VecScatter ctx1 ;
  Vec vout_scatter ;
  VecScatterCreateToZero (natural_vector, &ctx1, &vout_scatter) ;
  VecScatterBegin (ctx1, natural_vector, vout_scatter, INSERT_VALUES, SCATTER_FORWARD) ;
  VecScatterEnd (ctx1, natural_vector, vout_scatter, INSERT_VALUES, SCATTER_FORWARD) ;
  VecScatterDestroy (&ctx1) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;

  // Copy values onto the vector lsm_to_petsc_map
  if (rank == 0) {
    lsm_to_petsc_map.resize (vec_size, 0) ;

    for (int i = 0 ; i < vec_size ; i++) {
      PetscInt ni_get_vals = 1 ;
      PetscInt ix_get_vals[1] ;
      ix_get_vals[0] = i ;
      PetscScalar y_get_vals[1] ;

      VecGetValues (vout_scatter, ni_get_vals, ix_get_vals, y_get_vals) ;
      lsm_to_petsc_map[i] = int (y_get_vals[0]) ;
    }
  }

  VecDestroy (&vout_scatter) ;
  MPI_Barrier (PETSC_COMM_WORLD) ;

  return ;
}

} // namespace PARA_FEA