/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#include "topopt1d.h"

#include <cmath>

namespace PARA_FEA {

TopOpt1D :: TopOpt1D (InitializeOpt &myInit) {
  x = NULL ;
  dfdx = NULL ;
  da_nodes = NULL ;
  da_elem = NULL ;

  SetUp (myInit) ;
}


TopOpt1D :: ~TopOpt1D () {
  // Delete vectors
  if (x != NULL) {VecDestroy (&x) ;}
  if (dfdx != NULL) {VecDestroy (&dfdx) ;}

  if (da_nodes != NULL) {DMDestroy (&(da_nodes)) ;}
  if (da_elem != NULL) {DMDestroy (&(da_elem)) ;}
}


// NO METHODS !
//PetscErrorCode TopOpt::SetUp(Vec CRAPPY_VEC){
PetscErrorCode TopOpt1D :: SetUp (InitializeOpt &myInit) {
	PetscErrorCode ierr ;

	// SET DEFAULTS for FE mesh and levels for MG solver
  nxyz[0] = myInit.nelx+1 ;
  nxyz[1] = myInit.nely+1 ;
  nxyz[2] = myInit.nelz+1 ;
  xc[0] = 0.0 ;
  xc[1] = myInit.lx ;
  xc[2] = 0.0 ;
  xc[3] = myInit.ly ;
  xc[4] = 0.0 ;
  xc[5] = myInit.lz ;
  nu = 0.0 ;
  nlvls = myInit.nlvls ;

	// SET DEFAULTS for optimization problems
	volfrac = 0.1 ;
	maxItr = myInit.maxItr ;
  rmin = 0.02 ;
  penal = 1.0 ;
  Kmin = myInit.Kmin ;
  Kmax = myInit.Kmax ;
  filter = 0 ; // 0=sens,1=dens,2=PDE - other val == no filtering
  m = 1 ; // volume constraint
  Xmin = 0.0 ;
  Xmax = 1.0 ;
  movlim = 0.2 ;

  // surface convection
  hSurfConv = 1.62e4*1 + 120.0*0 ;
  Tambi = 25.0 ; // deg C

	ierr = SetUpMESH () ; CHKERRQ (ierr) ;
	ierr = SetUpOPT () ; CHKERRQ (ierr) ;

	return (ierr) ;
}


PetscErrorCode TopOpt1D :: SetUpMESH () {
	PetscErrorCode ierr ;

	// Read input from arguments
	PetscBool flg ;

	// Physics parameters
	PetscOptionsGetInt (NULL,NULL,"-nx",&(nxyz[0]),&flg) ;
	PetscOptionsGetInt (NULL,NULL,"-ny",&(nxyz[1]),&flg) ;
	PetscOptionsGetInt (NULL,NULL,"-nz",&(nxyz[2]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-xcmin",&(xc[0]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-xcmax",&(xc[1]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-ycmin",&(xc[2]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-ycmax",&(xc[3]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-zcmin",&(xc[4]),&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-zcmax",&(xc[5]),&flg) ;
  PetscOptionsGetReal (NULL,NULL,"-penal",&penal,&flg) ;
	PetscOptionsGetInt (NULL,NULL,"-nlvls",&nlvls,&flg) ;

	// Write parameters for the physics _ OWNED BY TOPOPT
	PetscPrintf (PETSC_COMM_WORLD,
    "########################################################################\n") ;
	PetscPrintf (PETSC_COMM_WORLD,
    "############################ FEM settings ##############################\n") ;
	PetscPrintf (PETSC_COMM_WORLD,
    "# Number of nodes: (-nx,-ny,-nz):        (%i,%i,%i) \n",nxyz[0],nxyz[1],nxyz[2]) ;
  PetscPrintf (PETSC_COMM_WORLD,
    "# Number of degree of freedom:           %i \n",3*nxyz[0]*nxyz[1]*nxyz[2]) ;
	PetscPrintf (PETSC_COMM_WORLD,
    "# Number of elements:                    (%i,%i,%i) \n",nxyz[0]-1,nxyz[1]-1,nxyz[2]-1) ;
	PetscPrintf (PETSC_COMM_WORLD,
    "# Dimensions: (-xcmin,-xcmax,..,-zcmax): (%f,%f,%f)\n",xc[1]-xc[0],xc[3]-xc[2],xc[5]-xc[4]) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -nlvls: %i\n",nlvls) ;
  PetscPrintf (PETSC_COMM_WORLD,
    "########################################################################\n") ;

	// Check if the mesh supports the chosen number of MG levels
	PetscScalar divisor = PetscPowScalar (2.0,(PetscScalar)nlvls-1.0) ;
	// x - dir
	if (std::floor ((PetscScalar)(nxyz[0]-1)/divisor) != (nxyz[0]-1.0)/((PetscInt)divisor)) {
    PetscPrintf (PETSC_COMM_WORLD,
      "MESH DIMENSION NOT COMPATIBLE WITH NUMBER OF MULTIGRID LEVELS!\n") ;
    PetscPrintf (PETSC_COMM_WORLD,
      "X - number of nodes %i is cannot be halfened %i times\n",nxyz[0],nlvls-1) ;
		exit (0) ;
	}
	// y - dir
  if (std::floor ((PetscScalar)(nxyz[1]-1)/divisor) != (nxyz[1]-1.0)/((PetscInt)divisor)) {
    PetscPrintf (PETSC_COMM_WORLD,
      "MESH DIMENSION NOT COMPATIBLE WITH NUMBER OF MULTIGRID LEVELS!\n") ;
    PetscPrintf (PETSC_COMM_WORLD,
      "Y - number of nodes %i is cannot be halfened %i times\n",nxyz[1],nlvls-1) ;
		exit (0) ;
  }
	// z - dir
  if (std::floor ((PetscScalar)(nxyz[2]-1)/divisor) != (nxyz[2]-1.0)/((PetscInt)divisor) ) {
    PetscPrintf (PETSC_COMM_WORLD,
      "MESH DIMENSION NOT COMPATIBLE WITH NUMBER OF MULTIGRID LEVELS!\n") ;
    PetscPrintf (PETSC_COMM_WORLD,
      "Z - number of nodes %i is cannot be halfened %i times\n",nxyz[2],nlvls-1) ;
		exit (0) ;
  }

	// Start setting up the FE problem
	// Boundary types: DMDA_BOUNDARY_NONE, DMDA_BOUNDARY_GHOSTED, DMDA_BOUNDARY_PERIODIC
	DMBoundaryType bx = DM_BOUNDARY_NONE ;
	DMBoundaryType by = DM_BOUNDARY_NONE ;
	DMBoundaryType bz = DM_BOUNDARY_NONE ;

	// Stencil type - box since this is closest to FEM (i.e. STAR is FV/FD)
	DMDAStencilType  stype = DMDA_STENCIL_BOX ;

	// Discretization: nodes:
	// For standard FE - number must be odd
	// FOr periodic: Number must be even
	PetscInt nx = nxyz[0] ;
	PetscInt ny = nxyz[1] ;
	PetscInt nz = nxyz[2] ;

	// number of nodal dofs
	PetscInt numnodaldof = 1 ;

	// Stencil width: each node connects to a box around it - linear elements
	PetscInt stencilwidth = 1 ;

	// Coordinates and element sizes: note that dx,dy,dz are half the element size
	PetscReal xmin=xc[0], xmax=xc[1] ;
  PetscReal ymin=xc[2], ymax=xc[3] ;
  PetscReal zmin=xc[4], zmax=xc[5] ;
	dx = (xc[1]-xc[0])/(PetscScalar (nxyz[0]-1)) ;
	dy = (xc[3]-xc[2])/(PetscScalar (nxyz[1]-1)) ;
	dz = (xc[5]-xc[4])/(PetscScalar (nxyz[2]-1)) ;

	// Create the nodal mesh
	ierr = DMDACreate3d (PETSC_COMM_WORLD,bx,by,bz,stype,nx,ny,nz,PETSC_DECIDE,
    PETSC_DECIDE,PETSC_DECIDE,numnodaldof,stencilwidth,0,0,0,&(da_nodes)) ;
	CHKERRQ (ierr) ;

	// Set the coordinates
	ierr = DMDASetUniformCoordinates (da_nodes, xmin,xmax, ymin,ymax, zmin,zmax) ;
	CHKERRQ (ierr) ;

	// Set the element type to Q1: Otherwise calls to GetElements will change to P1 !
	// STILL DOESN*T WORK !!!!
	ierr = DMDASetElementType (da_nodes, DMDA_ELEMENT_Q1) ;
	CHKERRQ (ierr) ;

	// Create the element mesh: NOTE THIS DOES NOT INCLUDE THE FILTER !!!
	// find the geometric partitioning of the nodal mesh, so the element mesh will coincide
	// with the nodal mesh
	PetscInt md,nd,pd ;
	ierr = DMDAGetInfo (da_nodes,NULL,NULL,NULL,NULL,&md,&nd,&pd,NULL,NULL,NULL,
    NULL,NULL,NULL) ;
	CHKERRQ (ierr) ;

	// vectors with partitioning information
	PetscInt *Lx=new PetscInt[md] ;
	PetscInt *Ly=new PetscInt[nd] ;
	PetscInt *Lz=new PetscInt[pd] ;

	// get number of nodes for each partition
	const PetscInt *LxCorrect, *LyCorrect, *LzCorrect ;
	ierr = DMDAGetOwnershipRanges(da_nodes, &LxCorrect, &LyCorrect, &LzCorrect) ;
	CHKERRQ (ierr) ;

	// subtract one from the lower left corner.
	for (int i = 0 ; i < md ; i++) {
		Lx[i] = LxCorrect[i] ;
		if (i==0) {Lx[i] = Lx[i]-1 ;}
	}
	for (int i = 0 ; i < nd ; i++) {
		Ly[i] = LyCorrect[i] ;
		if (i==0) {Ly[i] = Ly[i]-1 ;}
	}
	for (int i = 0 ; i < pd ; i++) {
		Lz[i] = LzCorrect[i] ;
		if (i==0) {Lz[i] = Lz[i]-1 ;}
	}

	// Create the element grid: NOTE CONNECTIVITY IS 0
	PetscInt conn = 0 ;
	ierr = DMDACreate3d (PETSC_COMM_WORLD,bx,by,bz,stype,nx-1,ny-1,nz-1,md,nd,pd,
    1,conn,Lx,Ly,Lz,&(da_elem)) ;
	CHKERRQ (ierr) ;

	// Set element center coordinates
	ierr = DMDASetUniformCoordinates (da_elem, xmin+dx/2.0, xmax-dx/2.0,
    ymin+dy/2.0, ymax-dy/2.0, zmin+dz/2.0, zmax-dz/2.0) ;
	CHKERRQ (ierr) ;

	// Clean up
	delete [] Lx ;
	delete [] Ly ;
	delete [] Lz ;

  return (ierr) ;
}


PetscErrorCode TopOpt1D :: SetUpOPT () {
	PetscErrorCode ierr ;

	//ierr = VecDuplicate(CRAPPY_VEC,&xPhys); CHKERRQ(ierr);
	ierr = DMCreateGlobalVector (da_elem,&x) ; CHKERRQ (ierr) ;
	// Total number of design variables
	VecGetSize (x,&n) ;

	PetscBool flg ;

	// Optimization paramteres
	PetscOptionsGetReal (NULL,NULL,"-Kmin",&Kmin,&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-Kmax",&Kmax,&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-volfrac",&volfrac,&flg) ;
  PetscOptionsGetReal (NULL,NULL,"-penal",&penal,&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-rmin",&rmin,&flg) ;
	PetscOptionsGetInt (NULL,NULL,"-maxItr",&maxItr,&flg) ;
	PetscOptionsGetInt (NULL,NULL,"-filter",&filter,&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-Xmin",&Xmin,&flg) ;
  PetscOptionsGetReal (NULL,NULL,"-Xmax",&Xmax,&flg) ;
	PetscOptionsGetReal (NULL,NULL,"-movlim",&movlim,&flg) ;

	PetscPrintf (PETSC_COMM_WORLD,
    "################### Optimization settings ####################\n") ;
	PetscPrintf (PETSC_COMM_WORLD,"# Problem size: n= %i, m= %i\n",n,m) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -filter: %i  (0=sens., 1=dens, 2=PDE)\n",filter) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -rmin: %f\n",rmin) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -volfrac: %f\n",volfrac) ;
  PetscPrintf (PETSC_COMM_WORLD,"# -penal: %f\n",penal) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -Kmin/-Kmax: %e - %e \n",Kmin,Kmax) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -maxItr: %i\n",maxItr) ;
	PetscPrintf (PETSC_COMM_WORLD,"# -movlim: %f\n",movlim) ;
  PetscPrintf (PETSC_COMM_WORLD,
    "##############################################################\n") ;

  // Allocate after input
  gx = new PetscScalar[m] ;
	if (filter==0) {
		Xmin = 0.001 ; // Prevent division by zero in filter
	}

	// Allocate the optimization vectors
	VecSet (x,0.0) ; // Initialize to 0.0 !

	// Sensitivity vectors
	ierr = VecDuplicate (x,&dfdx) ; CHKERRQ (ierr) ;

  // element temperatures
  VecDuplicate (x,&DeltaTVec) ;
  VecSet (DeltaTVec,0.0) ; // Initialize to 0.0 !

  #ifndef SAVEMEMORY
  // element adjoint temperatures
  VecDuplicate (x,&DeltaTVecAdj) ;
  VecSet (DeltaTVecAdj,0.0) ; // Initialize to 0.0 !
  // element adjoint temperatures
  VecDuplicate (x,&DeltaTVecFlux) ;
  VecSet (DeltaTVecFlux,0.0) ; // Initialize to 0.0 !

  // define moments
  areaToVolume.resize (8) ;
  for (int i = 0 ; i < 8 ; i++) {
    VecDuplicate (x,&areaToVolume[i]) ;
    VecSet (areaToVolume[i],0.0) ; // Initialize to 0.0 !
  }

  #endif

	return (ierr) ;
}

} // namespace PARA_FEA