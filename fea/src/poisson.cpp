/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#include "poisson.h"

namespace PARA_FEA {

Poisson :: Poisson (TopOpt1D *opt, InitializeOpt &myInit) {
	// Set pointers to null
	K = NULL ;
	U = NULL ;
	RHS = NULL ;
	HeatLoad = NULL ;
	Tgiven = NULL ;
	N = NULL ;
	adU = NULL ;
	ksp = NULL ;
	adRHS = NULL ;

	// Setup sitffness matrix, load vector and bcs (Dirichlet) for the design problem
	SetUpLoadAndBC (opt, myInit) ;
}


Poisson :: ~Poisson () {
	// Deallocate
	VecDestroy (&(U)) ;
	VecDestroy (&(adU)) ;
	VecDestroy (&(RHS)) ;
	VecDestroy (&(HeatLoad)) ;
	VecDestroy (&(Tgiven)) ;
	VecDestroy (&(N)) ;
	VecDestroy (&(adRHS)) ;
	MatDestroy (&(K)) ;
	KSPDestroy (&(ksp)) ;
}


PetscErrorCode Poisson :: SetUpLoadAndBC (TopOpt1D *opt, InitializeOpt &myInit) {
	PetscErrorCode ierr ;

	// Allocate matrix and the RHS and Solution vector and Dirichlet vector
	ierr = DMCreateMatrix (opt->da_nodes,&(K)) ; CHKERRQ (ierr) ;
	ierr = DMCreateGlobalVector (opt->da_nodes,&(U)) ; CHKERRQ (ierr) ;
	VecDuplicate (U,&(HeatLoad)) ;
	VecDuplicate (U,&(N)) ;
	VecDuplicate (U,&(Tgiven)) ;
	VecDuplicate (U,&(RHS)) ;
	VecDuplicate (U,&(adRHS)) ;
	VecDuplicate (U,&(adU)) ;

	// Set the local stiffness matrix
	PetscScalar a = opt->dx ; // x-side length
	PetscScalar b = opt->dy ; // y-side length
	PetscScalar c = opt->dz ; // z-side length
	PetscScalar X[8] = {0.0, a, a , 0.0, 0.0, a, a , 0.0} ;
	PetscScalar Y[8] = {0.0, 0.0, b, b, 0.0, 0.0, b, b} ;
	PetscScalar Z[8] = {0.0,0.0,0.0,0.0,c,c,c,c} ;
	areaScaleFactor = a*b ;

	// Compute the element stiffnes matrix - constant due to structured grid
	Hex8Isoparametric (X, Y, Z, 0.0, false, KE) ;

	// Set the RHS and Dirichlet vector
	VecSet (N,1.0) ;
	VecSet (HeatLoad,0.0) ;
	VecSet (Tgiven,0.0) ;
	VecSet (RHS,0.0) ;
	VecSet (adU,0.0) ;
	VecSet (adRHS,0.0) ;

	// Global coordinates and a pointer
	Vec lcoor ; // borrowed ref - do not destroy!
	PetscScalar *lcoorp ;

	// Get local coordinates in local node numbering including ghosts
	ierr = DMGetCoordinatesLocal (opt->da_nodes,&lcoor) ; CHKERRQ (ierr) ;
	VecGetArray (lcoor,&lcoorp) ;

	// Get local dof number
	PetscInt nn ;
	VecGetSize (lcoor,&nn) ;

	// Compute epsilon parameter for finding points in space:
	PetscScalar epsi = PetscMin (a*0.05, PetscMin (b*0.05,c*0.05)) ;
	// PetscScalar epsi = 1.0e-6;

	for (PetscInt i = 0 ; i < nn ; i++) {
		if (i % 3 == 0) {
			double xNode = lcoorp[i] ;
      double yNode = lcoorp[i+1] ;
      double zNode = lcoorp[i+2] ;

			// boundary conditions
			for (int bc = 0 ; bc < myInit.fixedHeatDof.size (); bc++) {
				// see if at least one is fixed
				if (true) {
          // check for tolerance
          if (std::abs (xNode-myInit.fixedHeatDof[bc].x) <= myInit.fixedHeatDof[bc].tolx &&
            std::abs (yNode-myInit.fixedHeatDof[bc].y) <= myInit.fixedHeatDof[bc].toly &&
            std::abs (zNode-myInit.fixedHeatDof[bc].z) <= myInit.fixedHeatDof[bc].tolz) {

            // assign boundary conditions
            VecSetValueLocal (N,i/3, 0.0, INSERT_VALUES) ;
            VecSetValueLocal (Tgiven,i/3, 1.0*myInit.fixedHeatDof[bc].val, INSERT_VALUES) ;
          }// if tolerence
				}// if fixed
			} // bc loop

			// loading conditions
			// boundary conditions
			for (int load = 0 ; load < myInit.heatLoad.size () ; load++) {
				// check for tolerance
				if (std::abs (xNode-myInit.heatLoad[load].x) <= myInit.heatLoad[load].tolx &&
          std::abs (yNode-myInit.heatLoad[load].y) <= myInit.heatLoad[load].toly &&
          std::abs (zNode-myInit.heatLoad[load].z) <= myInit.heatLoad[load].tolz) {

          VecSetValueLocal (HeatLoad,i/3, 1.0*myInit.heatLoad[load].val, INSERT_VALUES) ;
				}// if tolerence
			} // load loop
		}
	}

	VecAssemblyBegin (N) ;
	VecAssemblyBegin (HeatLoad) ;
	VecAssemblyBegin (Tgiven) ;
	VecAssemblyEnd (N) ;
	VecAssemblyEnd (HeatLoad) ;
	VecAssemblyEnd (Tgiven) ;

	VecRestoreArray (lcoor,&lcoorp) ;

	return ierr ;
}


PetscErrorCode Poisson :: SolveState (TopOpt1D *opt, bool withConvection) {
	PetscErrorCode ierr ;

	double t1, t2 ;
	t1 = MPI_Wtime () ;

	// Assemble the stiffness matrix
	if (withConvection) {
		ierr = AssembleSurfaceConvection (opt) ;
		CHKERRQ (ierr) ;
	}	else {
		ierr = AssembleStiffnessMatrix (opt) ;
		CHKERRQ (ierr) ;
	}

	// Setup the solver
	if (ksp==NULL) {
		ierr = SetUpSolver (opt) ;
		CHKERRQ (ierr) ;
	} else {
		ierr = KSPSetOperators (ksp,K,K) ;
		CHKERRQ (ierr) ;
		KSPSetUp (ksp) ;
	}

	ierr = KSPSetTolerances (ksp,myRTOL,1.0e-50,1.0e3,200) ;

	// Solve
	ierr = KSPSolve (ksp,RHS,U) ; CHKERRQ (ierr) ;
	CHKERRQ (ierr) ;

	// DEBUG
	// Get iteration number and residual from KSP
	PetscInt niter ;
	PetscScalar rnorm ;
	KSPGetIterationNumber (ksp,&niter) ;
	KSPGetResidualNorm (ksp,&rnorm) ;
	PetscReal RHSnorm ;
  ierr = VecNorm (RHS,NORM_2,&RHSnorm) ; CHKERRQ (ierr) ;
	rnorm = rnorm/RHSnorm ;

	t2 = MPI_Wtime () ;
	PetscPrintf (PETSC_COMM_WORLD,"State solver:  iter: %i, rerr.: %e, time: %f\n",
    niter,rnorm,t2-t1) ;

	// T = T + Tgiven
	VecAXPY (U , 1.0 , Tgiven) ;

	return ierr ;
}


PetscErrorCode Poisson :: SolveAdjointState (TopOpt1D *opt) {
	PetscErrorCode ierr ;

	double t1, t2 ;
	t1 = MPI_Wtime () ;

	// Solve
	ierr = KSPSolve (ksp,adRHS,adU) ; CHKERRQ (ierr) ;
	CHKERRQ (ierr) ;

	// DEBUG
	// Get iteration number and residual from KSP
	PetscInt niter ;
	PetscScalar rnorm ;
	 KSPGetIterationNumber (ksp,&niter) ;
	 KSPGetResidualNorm (ksp,&rnorm) ;
	PetscReal RHSnorm ;
  ierr = VecNorm (adRHS,NORM_2,&RHSnorm) ; CHKERRQ (ierr) ;
	rnorm = rnorm/RHSnorm ;

	t2 = MPI_Wtime () ;
	PetscPrintf (PETSC_COMM_WORLD,
    "Adjoint State solver:  iter: %i, rerr.: %e, time: %f\n",niter,rnorm,t2-t1) ;

	return ierr ;
}


PetscErrorCode Poisson :: ComputeSensitivities (TopOpt1D *opt) {
	// Errorcode
	PetscErrorCode ierr ;

	// Solve state eqs
	ierr = SolveState (opt) ; CHKERRQ (ierr) ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ; CHKERRQ (ierr) ;
	//DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype change !

	// Get pointer to the densities
	PetscScalar *xp ;
	VecGetArray (opt->x,&xp) ;

	// // Get pointer to xloc yloc and zloc
	// PetscScalar *xlocp;
	// VecGetArray(opt->xloc,&xlocp);
	// PetscScalar *ylocp;
	// VecGetArray(opt->yloc,&ylocp);
	// PetscScalar *zlocp;
	// VecGetArray(opt->zloc,&zlocp);

	// Get Solution
	Vec Uloc ;
	DMCreateLocalVector (opt->da_nodes,&Uloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,U,INSERT_VALUES,Uloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,U,INSERT_VALUES,Uloc) ;

	// get pointer to local vector
	PetscScalar *up ;
	VecGetArray (Uloc,&up) ;

	// Get dfdx
	PetscScalar *df ;
	VecGetArray (opt->dfdx,&df) ;

	// Edof array
	PetscInt edof[8] ;

	opt->fx = 0.0 ;
	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}
		// Use SIMP for stiffness interpolation
		PetscScalar uKu = 0.0 ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			for (PetscInt h = 0 ; h < 8 ; h++) {
				uKu += up[edof[k]]*KE[k*8+h]*up[edof[h]] ;
				// uKu += up[edof[k]]*KE_local[k*24+h]*up[edof[h]];
			}
		}
		// Add to objective
		opt->fx += (opt->Kmin + PetscPowScalar (xp[i],opt->penal)*(opt->Kmax - opt->Kmin))*uKu ;
		// Set the Senstivity
		df[i]= -1.0 * opt->penal*PetscPowScalar (xp[i],opt->penal-1)*(opt->Kmax - opt->Kmin)*uKu ;
	}

	// Allreduce fx[0]
	PetscScalar tmp=opt->fx ;
	opt->fx=0.0 ;
	MPI_Allreduce (&tmp,&(opt->fx),1,MPIU_SCALAR,MPI_SUM,PETSC_COMM_WORLD) ;

	VecRestoreArray (opt->x,&xp) ;
	VecRestoreArray (Uloc,&up) ;
	VecRestoreArray (opt->dfdx,&df) ;
	VecDestroy (&Uloc) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return (ierr) ;
}


PetscErrorCode Poisson :: ComputeAdjointSensitivities (TopOpt1D *opt) {
	// Errorcode
	PetscErrorCode ierr ;

	// assemble adjoint load from the element centroid Adjoint load.
	ierr = ComputeAdjointLoadFromCentroids (opt) ; CHKERRQ (ierr) ;

	// Solve adjoint state eqs
	ierr = SolveAdjointState (opt) ; CHKERRQ (ierr) ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ; CHKERRQ (ierr) ;
	//DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype change !

	// Get pointer to the densities
	PetscScalar *xp ;
	VecGetArray (opt->x,&xp) ;

	// // Get pointer to xloc yloc and zloc
	// PetscScalar *xlocp;
	// VecGetArray(opt->xloc,&xlocp);
	// PetscScalar *ylocp;
	// VecGetArray(opt->yloc,&ylocp);
	// PetscScalar *zlocp;
	// VecGetArray(opt->zloc,&zlocp);

	// Get Solution
	Vec Uloc ;
	DMCreateLocalVector (opt->da_nodes,&Uloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,U,INSERT_VALUES,Uloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,U,INSERT_VALUES,Uloc) ;

	// Get Adjoint Solution
	Vec adUloc ;
	DMCreateLocalVector (opt->da_nodes,&adUloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,adU,INSERT_VALUES,adUloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,adU,INSERT_VALUES,adUloc) ;

	// get pointer to local vector
	PetscScalar *up ;
	VecGetArray (Uloc,&up) ;

	// get pointer to local adjoint vector
	PetscScalar *upAd ;
	VecGetArray (adUloc,&upAd) ;

	// Get dfdx
	PetscScalar *df ;
	VecGetArray (opt->dfdx,&df) ;

	// Edof array
	PetscInt edof[8] ;

	opt->fx = 0.0 ;
	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}
		// Use SIMP for stiffness interpolation
		PetscScalar lamKu = 0.0 ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			for (PetscInt h = 0 ; h < 8 ; h++) {
				lamKu += upAd[edof[k]]*KE[k*8+h]*up[edof[h]] ;
				// uKu += up[edof[k]]*KE_local[k*24+h]*up[edof[h]];
			}
		}
		// Set the Senstivity
		df[i]= -1.0*opt->penal*PetscPowScalar (xp[i],opt->penal-1)*(opt->Kmax - opt->Kmin)*lamKu ;
	}

	VecRestoreArray (opt->x,&xp) ;
	VecRestoreArray (Uloc,&up) ;
	VecRestoreArray (adUloc,&upAd) ;
	VecRestoreArray (opt->dfdx,&df) ;
	VecDestroy (&Uloc) ;
	VecDestroy (&adUloc) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return (ierr) ;
}


PetscErrorCode Poisson :: ComputeSurfaceConvectionSensitivities (TopOpt1D *opt) {
	// Errorcode
	PetscErrorCode ierr ;

	// assemble adjoint load just once
	double vecSumRHS ; VecSum (adRHS, &vecSumRHS) ;
	if (vecSumRHS < 0.01) {
		PetscScalar epsi = 0.05*opt->dx ; // x-side length

		// Global coordinates and a pointer
		Vec lcoor ; // borrowed ref - do not destroy!
		PetscScalar *lcoorp ;

		// Get local coordinates in local node numbering including ghosts
		ierr = DMGetCoordinatesLocal (opt->da_nodes,&lcoor) ; CHKERRQ (ierr) ;
		VecGetArray (lcoor,&lcoorp) ;

		// Get local dof number
		PetscInt nn ;
		VecGetSize (lcoor,&nn) ;

		// selet one node in the corner
		for (PetscInt i = 0 ; i < nn ; i++) {
			if (i % 3 == 0) {
				double xNode = lcoorp[i] ;
        double yNode = lcoorp[i+1] ;
        double zNode = lcoorp[i+2] ;
        // for the cpu problem
        // check for tolerance
        if (std::abs (xNode-0.75*opt->xc[1]) <= 0.2*opt->xc[1] + epsi &&
          std::abs (yNode-opt->xc[3]) <= epsi &&
          std::abs (zNode-0.25*opt->xc[5]) <= 0.2*opt->xc[5] + epsi) {

          VecSetValueLocal (adRHS, i/3,
            1.0/std::pow ((0.4*opt->xc[5]/opt->dz), 2.0), INSERT_VALUES) ;
        }// if tolerence

        // for the simple problem
        // if( std::abs(xNode - 1.0*opt->xc[1] ) <=   epsi &&
        // 		std::abs(yNode - 1.0*opt->xc[3] ) <=  epsi &&
        // 		std::abs(zNode - 1.0*opt->xc[5] ) <=   epsi ){
        // 			VecSetValueLocal(adRHS,i/3, 1.0, INSERT_VALUES);
        // }// if tolerence
			}
		}

		VecAssemblyBegin (adRHS) ;
		VecAssemblyEnd (adRHS) ;
		VecRestoreArray (lcoor,&lcoorp) ;
	}

	// Solve adjoint state eqs
	ierr = SolveAdjointState (opt) ; CHKERRQ (ierr) ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ; CHKERRQ (ierr) ;
	//DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype change !

	// Get pointer to the densities
	PetscScalar *xp ;
	VecGetArray (opt->x,&xp) ;

	// // Get pointer to xloc yloc and zloc
	// PetscScalar *xlocp;
	// VecGetArray(opt->xloc,&xlocp);
	// PetscScalar *ylocp;
	// VecGetArray(opt->yloc,&ylocp);
	// PetscScalar *zlocp;
	// VecGetArray(opt->zloc,&zlocp);

	// Get Solution
	Vec Uloc ;
	DMCreateLocalVector (opt->da_nodes,&Uloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,U,INSERT_VALUES,Uloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,U,INSERT_VALUES,Uloc) ;

	// Get Adjoint Solution
	Vec adUloc ;
	DMCreateLocalVector (opt->da_nodes,&adUloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,adU,INSERT_VALUES,adUloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,adU,INSERT_VALUES,adUloc) ;

	// get pointer to local vector
	PetscScalar *up ;
	VecGetArray (Uloc,&up) ;

	// get pointer to local adjoint vector
	PetscScalar *upAd ;
	VecGetArray (adUloc,&upAd) ;

	// Get dfdx
	PetscScalar *df ;
	VecGetArray (opt->dfdx,&df) ;

	// Edof array
	PetscInt edof[8] ;

	opt->fx = 0.0 ;
	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}
		// Use SIMP for stiffness interpolation
		PetscScalar lamKu = 0.0 ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			for (PetscInt h = 0 ; h < 8 ; h++) {
				lamKu += upAd[edof[k]]*KE[k*8+h]*up[edof[h]] ;
				// uKu += up[edof[k]]*KE_local[k*24+h]*up[edof[h]];
			}
		}
		// Set the Senstivity
		df[i]= -1.0*opt->penal*PetscPowScalar (xp[i],opt->penal-1)*(opt->Kmax - opt->Kmin)*lamKu ;
	}

	VecRestoreArray (opt->x,&xp) ;
	VecRestoreArray (Uloc,&up) ;
	VecRestoreArray (adUloc,&upAd) ;
	VecRestoreArray (opt->dfdx,&df) ;
	VecDestroy (&Uloc) ;
	VecDestroy (&adUloc) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	//compute objective book
	double dotp ; VecDot (adRHS, U, &dotp) ;
	opt->fx = dotp ;

	return (ierr) ;
}


PetscErrorCode Poisson :: ComputeCentroidTemperatures (TopOpt1D *opt,
    bool withConvection) {
	// Errorcode
	PetscErrorCode ierr ;

	// Solve state eqs
	ierr = SolveState (opt, withConvection) ; CHKERRQ (ierr) ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ; CHKERRQ (ierr) ;
	//DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype change !

	// Get pointer to the elemental temperature
	PetscScalar *elem_temp_p ;
	VecGetArray (opt->DeltaTVec,&elem_temp_p) ;

	// Get Solution
	Vec Uloc ;
	DMCreateLocalVector (opt->da_nodes,&Uloc) ;
	DMGlobalToLocalBegin (opt->da_nodes,U,INSERT_VALUES,Uloc) ;
	DMGlobalToLocalEnd (opt->da_nodes,U,INSERT_VALUES,Uloc) ;

	// get pointer to local vector
	PetscScalar *up ;
	VecGetArray (Uloc,&up) ;

	// Edof array
	PetscInt edof[8] ;

	opt->fx = 0.0 ;
	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}
		// Use SIMP for stiffness interpolation
		double temp_centroid = 0.0 ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			temp_centroid += 0.125*up[edof[k]] ;
		}
		elem_temp_p[i] = temp_centroid ;
	}

	VecRestoreArray (opt->DeltaTVec,&elem_temp_p) ;
	VecRestoreArray (Uloc,&up) ;
	VecDestroy (&Uloc) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return (ierr) ;
}


PetscErrorCode Poisson :: ComputeAdjointLoadFromCentroids (TopOpt1D *opt) {
	// Errorcode
	PetscErrorCode ierr ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ; CHKERRQ (ierr) ;
	//DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype change !

	// Get pointer to the elemental temperature
	PetscScalar *elem_temp_p ;
	VecGetArray (opt->DeltaTVecAdj,&elem_temp_p) ;

	// Zeroize adjoint load
	VecSet (adRHS,0.0) ;

	// Edof array
	PetscInt edof[8] ;

	opt->fx = 0.0 ;
	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}
		// Use SIMP for stiffness interpolation
		PetscScalar adRHSe[8] ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			adRHSe[k] = 0.125*elem_temp_p[i] ;
		}

		// Assemble into adRHS vector
		ierr = VecSetValuesLocal (adRHS,8,edof,adRHSe,ADD_VALUES) ; CHKERRQ (ierr) ;
	}

	// assemble
	VecAssemblyBegin (adRHS) ;
	VecAssemblyEnd (adRHS) ;

	// Zero out possible loads in the adRHS that coincide with Dirichlet conditions
	VecPointwiseMult (adRHS,adRHS,N) ;
	VecRestoreArray (opt->DeltaTVecAdj,&elem_temp_p) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return (ierr) ;
}


//##################################################################
//##################################################################
//##################################################################
// ######################## PRIVATE ################################
//##################################################################
//##################################################################

PetscErrorCode Poisson :: AssembleStiffnessMatrix (TopOpt1D *opt) {
	PetscErrorCode ierr ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ;
	CHKERRQ (ierr) ;

	// Get pointer to the densities
	PetscScalar *xp ;
	VecGetArray (opt->x,&xp) ;

	// Zero the matrix
	MatZeroEntries (K) ;

	// Edof array
	PetscInt edof[8] ;
	PetscScalar ke[8*8] ;

	// Loop over elements
	for (PetscInt i = 0 ; i < nel ; i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}

		// Use SIMP for stiffness interpolation
		PetscScalar dens = opt->Kmin + PetscPowScalar (xp[i],opt->penal)*(opt->Kmax-opt->Kmin) ;
		for (PetscInt k = 0 ; k < 8*8 ; k++) {
			ke[k]=KE[k]*dens ;
		}

		// Add values to the sparse matrix
		ierr = MatSetValuesLocal (K,8,edof,8,edof,ke,ADD_VALUES) ;
		CHKERRQ (ierr) ;
	}

	MatAssemblyBegin (K, MAT_FINAL_ASSEMBLY) ;
	MatAssemblyEnd (K, MAT_FINAL_ASSEMBLY) ;

	// multiply K * Tgiven = Fgiven
	Vec FTgiven ;
	VecDuplicate (U,&FTgiven) ;
	VecSet (FTgiven,0.0) ;
	MatMult (K,Tgiven,FTgiven)  ;
	// RHSTemp = HeatLoad- FTgiven
	VecSet (RHS,0.0) ;
	VecAXPY (RHS, 1.0, HeatLoad) ;
	VecAXPY (RHS, -1.0, FTgiven) ;
	VecDestroy (&FTgiven) ;

	// Impose the dirichlet conditions, i.e. K = N'*K*N - (N-I)
	// 1.: K = N'*K*N
	MatDiagonalScale (K,N,N) ;
	// 2. Add ones, i.e. K = K + NI, NI = I - N
	Vec NI ;
	VecDuplicate (N,&NI) ;
	VecSet (NI,1.0) ;
	VecAXPY (NI,-1.0,N) ;
	MatDiagonalSet (K,NI,ADD_VALUES) ;

	// Zero out possible loads in the RHS that coincide
	// with Dirichlet conditions
	VecPointwiseMult (RHS,RHS,N) ;
	VecDestroy (&NI) ;
	VecRestoreArray (opt->x,&xp) ;

  DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return ierr ;
}


PetscErrorCode Poisson :: AssembleSurfaceConvection (TopOpt1D *opt) {
	PetscErrorCode ierr ;

	// Get the FE mesh structure (from the nodal mesh)
	PetscInt nel, nen ;
	const PetscInt *necon ;
	ierr = DMDAGetElements_3D (opt->da_nodes,&nel,&nen,&necon) ;
	CHKERRQ (ierr) ;

	// Get pointer to the densities
	PetscScalar *xp ;
	VecGetArray (opt->x,&xp) ;

	// Get pointer to adjoint
	PetscScalar *deltaTfluxPointer ;
	VecGetArray (opt->DeltaTVecFlux,&deltaTfluxPointer) ;

	// Get pointer to quadrature
	std::vector<PetscScalar*> areaToVolumePointer (8) ;
	for (int i = 0 ; i < 8 ; i++) {
		VecGetArray (opt->areaToVolume[i], &areaToVolumePointer[i]) ;
	}

	// Zero the matrix
	MatZeroEntries (K) ;

	// Edof array
	PetscInt edof[8] ;
	PetscScalar ke[8*8] ;

	Vec SurfConvLoad ;
	VecDuplicate (U,&(SurfConvLoad)) ;
	VecSet (SurfConvLoad, 0.0) ;

	// Loop over elements
	for (PetscInt i=0;i<nel;i++) {
		// loop over element nodes
		for (PetscInt j = 0 ; j < nen ; j++) {
			// Get local dofs
			for (PetscInt k = 0 ; k < 1 ; k++) {
				edof[j*1+k] = 1*necon[i*nen+j]+k ;
			}
		}

		// Use SIMP for stiffness interpolation
		PetscScalar dens = opt->Kmin + PetscPowScalar (xp[i],opt->penal)*(opt->Kmax-opt->Kmin) ;
		for (PetscInt k = 0 ; k < 8*8 ; k++) {
			ke[k]=KE[k]*dens ;
		}

		// add convection related stiffness
		PetscScalar elemConvection[8] ;
		for (PetscInt k = 0 ; k < 8 ; k++) {
			ke[8*k+k] += (opt->hSurfConv*areaToVolumePointer[k][i]*areaScaleFactor) ;
			elemConvection[k] = (opt->hSurfConv*areaToVolumePointer[k][i]*areaScaleFactor*opt->Tambi) ;
		}

		// add the auxiliary heat flux
		for (PetscInt k = 0 ; k < 8 ; k++) {
			elemConvection[k] += (deltaTfluxPointer[i]*areaToVolumePointer[k][i]*areaScaleFactor) ;
		}

		// Add values to the sparse matrix
		ierr = MatSetValuesLocal (K,8,edof,8,edof,ke,ADD_VALUES) ;
		CHKERRQ (ierr) ;

		// Add convection
		ierr = VecSetValuesLocal (SurfConvLoad,8,edof,elemConvection,ADD_VALUES) ; CHKERRQ (ierr) ;
	}

	MatAssemblyBegin (K, MAT_FINAL_ASSEMBLY) ;
	MatAssemblyEnd (K, MAT_FINAL_ASSEMBLY) ;

	// Assemble surfrace convection load
	VecAssemblyBegin (SurfConvLoad) ;
	VecAssemblyEnd (SurfConvLoad) ;

	// multiply K * Tgiven = Fgiven
	Vec FTgiven ;
	VecDuplicate (U,&FTgiven) ;
	VecSet (FTgiven,0.0) ;
	MatMult (K,Tgiven,FTgiven) ;
	// RHSTemp = HeatLoad- FTgiven
	VecSet (RHS,0.0) ;
	VecAXPY (RHS, 1.0, HeatLoad) ;
	VecAXPY (RHS, 1.0, SurfConvLoad) ;
	VecAXPY (RHS, -1.0, FTgiven) ;
	VecDestroy (&FTgiven) ;

	// Impose the dirichlet conditions, i.e. K = N'*K*N - (N-I)
	// 1.: K = N'*K*N
	MatDiagonalScale (K,N,N) ;
	// 2. Add ones, i.e. K = K + NI, NI = I - N
	Vec NI ;
	VecDuplicate (N,&NI) ;
	VecSet (NI,1.0) ;
	VecAXPY (NI,-1.0,N) ;
	MatDiagonalSet (K,NI,ADD_VALUES) ;

	// Zero out possible loads in the RHS that coincide
	// with Dirichlet conditions
	VecPointwiseMult (RHS,RHS,N) ;

	VecDestroy (&NI) ;
	VecRestoreArray (opt->x,&xp) ;
	VecRestoreArray (opt->DeltaTVecFlux,&deltaTfluxPointer) ;

	for (int i = 0 ; i < 8 ; i++) {
		VecRestoreArray (opt->areaToVolume[i], &areaToVolumePointer[i]) ;
	}

	VecDestroy (&SurfConvLoad) ;

	DMDARestoreElements (opt->da_nodes,&nel,&nen,&necon) ;

	return ierr ;
}


PetscErrorCode Poisson :: SetUpSolver (TopOpt1D *opt) {
	PetscErrorCode ierr ;
	PC pc ;

	// The fine grid Krylov method
	KSPCreate (PETSC_COMM_WORLD,&(ksp)) ;

	// SET THE DEFAULT SOLVER PARAMETERS
	// The fine grid solver settings
	PetscScalar rtol = myRTOL ;
	PetscScalar atol = 1.0e-50 ;
	PetscScalar dtol = 1.0e3 ;
	PetscInt restart = 100 ;
	PetscInt maxitsGlobal = 200 ;

	// Coarsegrid solver
	PetscScalar coarse_rtol = 1.0e-8 ;
	PetscScalar coarse_atol = 1.0e-50 ;
	PetscScalar coarse_dtol = 1e3 ;
	PetscInt coarse_maxits = 30 ;
	PetscInt coarse_restart = 30 ;

	// Number of smoothening iterations per up/down smooth_sweeps
	PetscInt smooth_sweeps = 4 ;

	// Set up the solver
	ierr = KSPSetType (ksp,KSPFGMRES) ; // KSPCG, KSPGMRES
	CHKERRQ (ierr) ;

	ierr = KSPGMRESSetRestart (ksp,restart) ;
	CHKERRQ (ierr) ;

	ierr = KSPSetTolerances (ksp,rtol,atol,dtol,maxitsGlobal) ;
	CHKERRQ (ierr) ;

	ierr = KSPSetInitialGuessNonzero (ksp,PETSC_TRUE) ;
	CHKERRQ (ierr) ;

	ierr = KSPSetOperators (ksp,K,K) ;
	CHKERRQ (ierr) ;

	// The preconditinoer
	KSPGetPC (ksp,&pc) ;
	// Make PCMG the default solver
	PCSetType (pc,PCMG) ;

	// Set solver from options
	KSPSetFromOptions (ksp) ;

	// Get the prec again - check if it has changed
	KSPGetPC (ksp,&pc) ;

	// Flag for pcmg pc
	PetscBool pcmg_flag = PETSC_TRUE ;
	PetscObjectTypeCompare ((PetscObject)pc,PCMG,&pcmg_flag) ;

	// Only if PCMG is used
	if (pcmg_flag) {
		// DMs for grid hierachy
		DM  *da_list,*daclist ;
		Mat R ;

		PetscMalloc (sizeof (DM)*opt->nlvls,&da_list) ;
		for (PetscInt k = 0 ; k < opt->nlvls ; k++) da_list[k] = NULL ;
		PetscMalloc (sizeof (DM)*opt->nlvls,&daclist) ;
		for (PetscInt k = 0 ; k < opt->nlvls ; k++) daclist[k] = NULL ;

		// Set 0 to the finest level
		daclist[0] = opt->da_nodes ;

		// Coordinates
		PetscReal xmin=opt->xc[0], xmax=opt->xc[1] ;
    PetscReal ymin=opt->xc[2], ymax=opt->xc[3] ;
    PetscReal zmin=opt->xc[4], zmax=opt->xc[5] ;

		// Set up the coarse meshes
		DMCoarsenHierarchy (opt->da_nodes,opt->nlvls-1,&daclist[1]) ;
		for (PetscInt k = 0 ; k < opt->nlvls ; k++) {
			// NOTE: finest grid is nlevels - 1: PCMG MUST USE THIS ORDER ???
			da_list[k] = daclist[opt->nlvls-1-k] ;
			// THIS SHOULD NOT BE NECESSARY
			DMDASetUniformCoordinates (da_list[k],xmin,xmax,ymin,ymax,zmin,zmax) ;
		}

		// the PCMG specific options
		PCMGSetLevels (pc,opt->nlvls,NULL) ;
		PCMGSetType (pc,PC_MG_MULTIPLICATIVE) ; // Default
		ierr = PCMGSetCycleType (pc,PC_MG_CYCLE_V) ; CHKERRQ (ierr) ;
		PCMGSetGalerkin (pc,PETSC_TRUE) ;
		for (PetscInt k = 1 ; k < opt->nlvls ; k++) {
			DMCreateInterpolation (da_list[k-1],da_list[k],&R,NULL) ;
			PCMGSetInterpolation (pc,k,R) ;
			MatDestroy (&R) ;
		}

		// tidy up
		for (PetscInt k = 1 ; k < opt->nlvls ; k++) { // DO NOT DESTROY LEVEL 0
			DMDestroy (&daclist[k]) ;
		}
		PetscFree (da_list) ;
		PetscFree (daclist) ;

		// AVOID THE DEFAULT FOR THE MG PART
		{
			// SET the coarse grid solver:
			// i.e. get a pointer to the ksp and change its settings
			KSP cksp ;
			PCMGGetCoarseSolve (pc,&cksp) ;
			// The solver
			ierr = KSPSetType (cksp,KSPGMRES) ; // KSPCG, KSPFGMRES
			ierr = KSPGMRESSetRestart (cksp,coarse_restart) ;
			ierr = KSPSetTolerances (cksp,coarse_rtol,coarse_atol,coarse_dtol,coarse_maxits) ;
			// The preconditioner
			PC cpc ;
			KSPGetPC (cksp,&cpc) ;
			PCSetType (cpc,PCSOR) ; // PCSOR, PCSPAI (NEEDS TO BE COMPILED), PCJACOBI

			// Set smoothers on all levels (except for coarse grid):
			for (PetscInt k = 1 ; k < opt->nlvls ; k++) {
				KSP dksp ;
				PCMGGetSmoother (pc,k,&dksp) ;
				PC dpc ;
				KSPGetPC (dksp,&dpc) ;
				ierr = KSPSetType (dksp,KSPGMRES) ; // KSPCG, KSPGMRES, KSPCHEBYSHEV (VERY GOOD FOR SPD)

				ierr = KSPGMRESSetRestart (dksp,smooth_sweeps) ;
				// NOTE maxitr=restart;
        ierr = KSPSetTolerances(dksp,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT,smooth_sweeps) ;
				PCSetType (dpc,PCSOR) ;// PCJACOBI, PCSOR for KSPCHEBYSHEV very good
			}
		}
	}

	// Write check to screen:
	// Check the overall Krylov solver
	KSPType ksptype ;
	KSPGetType (ksp,&ksptype) ;
	PCType pctype ;
	PCGetType (pc,&pctype) ;
	PetscInt mmax ;
	KSPGetTolerances (ksp,NULL,NULL,NULL,&mmax) ;
	PetscPrintf (PETSC_COMM_WORLD,
    "##############################################################\n") ;
	PetscPrintf (PETSC_COMM_WORLD,
    "################# Linear solver settings #####################\n") ;
	PetscPrintf (PETSC_COMM_WORLD,
    "# Main solver: %s, prec.: %s, maxiter.: %i \n",ksptype,pctype,mmax) ;

	// Only if pcmg is used
	if (pcmg_flag) {
		// Check the smoothers and coarse grid solver:
		for (PetscInt k=0;k<opt->nlvls;k++) {
			KSP dksp ;
			PC dpc ;
			KSPType dksptype ;
			PCMGGetSmoother (pc,k,&dksp) ;
			KSPGetType (dksp,&dksptype) ;
			KSPGetPC (dksp,&dpc) ;
			PCType dpctype ;
			PCGetType (dpc,&dpctype) ;
			PetscInt mmax ;
			KSPGetTolerances (dksp,NULL,NULL,NULL,&mmax) ;
			PetscPrintf (PETSC_COMM_WORLD,
        "# Level %i smoother: %s, prec.: %s, sweep: %i \n",k,dksptype,dpctype,mmax) ;
		}
	}
	PetscPrintf (PETSC_COMM_WORLD,
    "##############################################################\n") ;

	return (ierr) ;
}


PetscErrorCode Poisson :: DMDAGetElements_3D (DM dm, PetscInt *nel,
    PetscInt *nen, const PetscInt *e[]) {
	PetscErrorCode ierr ;
	DM_DA          *da = (DM_DA*)dm->data ;
	PetscInt       i,xs,xe,Xs,Xe ;
	PetscInt       j,ys,ye,Ys,Ye ;
	PetscInt       k,zs,ze,Zs,Ze ;
	PetscInt       cnt=0, cell[8], ns=1, nn=8 ;
	PetscInt       c ;
	if (!da->e) {
		if (da->elementtype == DMDA_ELEMENT_Q1) {ns=1; nn=8;}
		ierr = DMDAGetCorners (dm,&xs,&ys,&zs,&xe,&ye,&ze) ;
		CHKERRQ (ierr) ;
		ierr = DMDAGetGhostCorners (dm,&Xs,&Ys,&Zs,&Xe,&Ye,&Ze) ;
		CHKERRQ (ierr) ;
		xe += xs ; Xe += Xs ; if (xs != Xs) xs -= 1 ;
		ye += ys ; Ye += Ys ; if (ys != Ys) ys -= 1 ;
		ze += zs ; Ze += Zs ; if (zs != Zs) zs -= 1 ;
		da->ne = ns*(xe - xs - 1)*(ye - ys - 1)*(ze - zs - 1) ;
		PetscMalloc ((1 + nn*da->ne)*sizeof (PetscInt),&da->e) ;
		for (k = zs ; k < ze-1 ; k++) {
			for (j = ys ; j < ye-1 ; j++) {
				for (i = xs ; i < xe-1 ; i++) {
					cell[0] = (i-Xs  ) + (j-Ys  )*(Xe-Xs) + (k-Zs  )*(Xe-Xs)*(Ye-Ys) ;
					cell[1] = (i-Xs+1) + (j-Ys  )*(Xe-Xs) + (k-Zs  )*(Xe-Xs)*(Ye-Ys) ;
					cell[2] = (i-Xs+1) + (j-Ys+1)*(Xe-Xs) + (k-Zs  )*(Xe-Xs)*(Ye-Ys) ;
					cell[3] = (i-Xs  ) + (j-Ys+1)*(Xe-Xs) + (k-Zs  )*(Xe-Xs)*(Ye-Ys) ;
					cell[4] = (i-Xs  ) + (j-Ys  )*(Xe-Xs) + (k-Zs+1)*(Xe-Xs)*(Ye-Ys) ;
					cell[5] = (i-Xs+1) + (j-Ys  )*(Xe-Xs) + (k-Zs+1)*(Xe-Xs)*(Ye-Ys) ;
					cell[6] = (i-Xs+1) + (j-Ys+1)*(Xe-Xs) + (k-Zs+1)*(Xe-Xs)*(Ye-Ys) ;
					cell[7] = (i-Xs  ) + (j-Ys+1)*(Xe-Xs) + (k-Zs+1)*(Xe-Xs)*(Ye-Ys) ;
					if (da->elementtype == DMDA_ELEMENT_Q1) {
						for (c = 0 ; c < ns*nn ; c++) da->e[cnt++] = cell[c] ;
					}
				}
			}
		}
	}
	*nel = da->ne ;
	*nen = nn ;
	*e   = da->e ;
	return (0) ;
}


PetscInt Poisson :: Hex8Isoparametric (PetscScalar *X, PetscScalar *Y,
    PetscScalar *Z, PetscScalar nu, PetscInt redInt, PetscScalar *ke) {
	// Modified by Sandy for conduction problems
	// HEX8_ISOPARAMETRIC - Computes HEX8 isoparametric element matrices
	// The element stiffness matrix is computed as:
	//
	//       ke = int(int(int(B^T*C*B,x),y),z)
	//
	// For an isoparameteric element this integral becomes:
	//
	//       ke = int(int(int(B^T*C*B*det(J),xi=-1..1),eta=-1..1),zeta=-1..1)
	//
	// where B is the more complicated expression:
	// B = [dx*alpha1 + dy*alpha2 + dz*alpha3]*N
	// where
	// dx = [invJ11 invJ12 invJ13]*[dxi deta dzeta]
	// dy = [invJ21 invJ22 invJ23]*[dxi deta dzeta]
	// dy = [invJ31 invJ32 invJ33]*[dxi deta dzeta]
	//
	// Remark: The elasticity modulus is left out in the below
	// computations, because we multiply with it afterwards (the aim is
	// topology optimization).
	// Furthermore, this is not the most efficient code, but it is readable.
	//
	/////////////////////////////////////////////////////////////////////////////////
	//////// INPUT:
	// X, Y, Z  = Vectors containing the coordinates of the eight nodes
	//               (x1,y1,z1,x2,y2,z2,...,x8,y8,z8). Where node 1 is in the lower
	//               left corner, and node 2 is the next node counterclockwise
	//               (looking in the negative z-dir).
	//               Finish the x-y-plane and then move in the positive z-dir.
	// redInt   = Reduced integration option boolean (here an integer).
	//           	redInt == 0 (false): Full integration
	//           	redInt == 1 (true): Reduced integration
	// nu 		= Poisson's ratio.
	//
	//////// OUTPUT:
	// ke  = Element stiffness matrix. Needs to be multiplied with elasticity modulus
	//
	//   Written 2013 at
	//   Department of Mechanical Engineering
	//   Technical University of Denmark (DTU).
	/////////////////////////////////////////////////////////////////////////////////

	//// COMPUTE ELEMENT STIFFNESS MATRIX
	// Lame's parameters (with E=1.0):
	PetscScalar lambda = nu/((1.0+nu)*(1.0-2.0*nu)) ;
	PetscScalar mu = 1.0/(2.0*(1.0+nu)) ;
	// Constitutive matrix
	PetscScalar C[3][3] = {{lambda+2.0*mu, lambda, lambda}, // SLS hard coded spacedim array size osx compiler error
		{lambda, lambda+2.0*mu, lambda},
		{lambda, lambda, lambda+2.0*mu},} ;
	// Gauss points (GP) and weigths
	// Two Gauss points in all directions (total of eight)
	PetscScalar GP[2] = {-0.577350269189626, 0.577350269189626} ;
	// Corresponding weights
	PetscScalar W[2] = {1.0, 1.0} ;
	// If reduced integration only use one GP
	if (redInt) {
		GP[1] = 0.0 ;
		W[1] = 2.0 ;
	}
	// Matrices that help when we gather the strain-displacement matrix:
	PetscScalar alpha1[3][1] ; PetscScalar alpha2[3][1] ; PetscScalar alpha3[3][1] ;
	memset (alpha1, 0, sizeof (alpha1[0][0])*3*1) ; // zero out
	memset (alpha2, 0, sizeof (alpha2[0][0])*3*1) ; // zero out
	memset (alpha3, 0, sizeof (alpha3[0][0])*3*1) ; // zero out
	alpha1[0][0] = 1.0 ;
	alpha2[1][0] = 1.0 ;
	alpha3[2][0] = 1.0 ;
	PetscScalar dNdxi[8] ; PetscScalar dNdeta[8] ; PetscScalar dNdzeta[8] ;
	PetscScalar J[3][3] ;
	PetscScalar invJ[3][3] ;
	PetscScalar beta[3][1] ;
	PetscScalar B[3][8] ; // Note: Small enough to be allocated on stack
	PetscScalar *dN ;
	// Make sure the stiffness matrix is zeroed out:
	memset (ke, 0, sizeof (ke[0])*8*8) ;
	// Perform the numerical integration
	for (PetscInt ii = 0 ; ii < 2-redInt ; ii++) {
		for (PetscInt jj = 0 ; jj < 2-redInt ; jj++) {
			for (PetscInt kk = 0 ; kk < 2-redInt ; kk++) {
				// Integration point
				PetscScalar xi = GP[ii] ;
				PetscScalar eta = GP[jj] ;
				PetscScalar zeta = GP[kk] ;
				// Differentiated shape functions
				DifferentiatedShapeFunctions (xi, eta, zeta, dNdxi, dNdeta, dNdzeta) ;
				// Jacobian
				J[0][0] = Dot (dNdxi,X,8) ; J[0][1] = Dot (dNdxi,Y,8) ; J[0][2] = Dot (dNdxi,Z,8) ;
				J[1][0] = Dot (dNdeta,X,8) ; J[1][1] = Dot (dNdeta,Y,8) ; J[1][2] = Dot (dNdeta,Z,8) ;
				J[2][0] = Dot (dNdzeta,X,8) ; J[2][1] = Dot (dNdzeta,Y,8) ; J[2][2] = Dot (dNdzeta,Z,8) ;
				// Inverse and determinant
				PetscScalar detJ = Inverse3M (J, invJ) ;
				// Weight factor at this point
				PetscScalar weight = W[ii]*W[jj]*W[kk]*detJ ;
				// Strain-displacement matrix
				memset (B, 0, sizeof (B[0][0])*3*8) ; // zero out
				for (PetscInt ll = 0 ; ll < 3 ; ll++) {
					// Add contributions from the different derivatives
					if (ll==0) {dN = dNdxi ;}
					if (ll==1) {dN = dNdeta ;}
					if (ll==2) {dN = dNdzeta ;}
					// Assemble strain operator
					for (PetscInt i = 0 ; i < 3 ; i++) {
						for (PetscInt j = 0 ; j < 1 ; j++) {
							beta[i][j] = invJ[0][ll]*alpha1[i][j] + invJ[1][ll]*alpha2[i][j] +
                invJ[2][ll]*alpha3[i][j] ;
						}
					}
					// Add contributions to strain-displacement matrix
					for (PetscInt i = 0 ; i < 3 ; i++) {
						for (PetscInt j = 0 ; j < 8 ; j++) {
							B[i][j] = B[i][j] + beta[i][0]*dN[j] ;
						}
					}
				}
				// Finally, add to the element matrix
				for (PetscInt i = 0 ; i < 8 ; i++) {
					for (PetscInt j = 0 ; j < 8 ; j++) {
						for (PetscInt k = 0 ; k < 3 ; k++) {
							for (PetscInt l = 0 ; l < 3 ; l++) {
								ke[j+8*i] = ke[j+8*i] + weight*(B[k][i] * C[k][l] * B[l][j]) ;
							}
						}
					}
				}
			}
		}
	}

	// compute Bmatrix at the centroid
	// redefine gauss points
	GP[1] = 0.0 ;
	W[1] = 2.0 ;
	// Perform the numerical integration
	for (PetscInt ii = 0 ; ii < 1 ; ii++) {
		for (PetscInt jj = 0 ; jj < 1 ; jj++) {
			for (PetscInt kk = 0 ; kk < 1 ; kk++) {
				// Integration point
				PetscScalar xi = GP[ii] ;
				PetscScalar eta = GP[jj] ;
				PetscScalar zeta = GP[kk] ;
				// Differentiated shape functions
				DifferentiatedShapeFunctions (xi, eta, zeta, dNdxi, dNdeta, dNdzeta) ;
				// Jacobian
				J[0][0] = Dot (dNdxi,X,8) ; J[0][1] = Dot (dNdxi,Y,8) ; J[0][2] = Dot (dNdxi,Z,8) ;
				J[1][0] = Dot (dNdeta,X,8) ; J[1][1] = Dot (dNdeta,Y,8) ; J[1][2] = Dot (dNdeta,Z,8) ;
				J[2][0] = Dot (dNdzeta,X,8) ; J[2][1] = Dot (dNdzeta,Y,8) ; J[2][2] = Dot (dNdzeta,Z,8) ;
				// Inverse and determinant
				PetscScalar detJ = Inverse3M (J, invJ) ;
				// Weight factor at this point
				PetscScalar weight = W[ii]*W[jj]*W[kk]*detJ ;
				// Strain-displacement matrix
				memset (Bmatrix, 0, sizeof (Bmatrix[0][0])*3*8) ; // zero out
				for (PetscInt ll = 0 ; ll < 3 ; ll++) {
					// Add contributions from the different derivatives
					if (ll==0) {dN = dNdxi ;}
					if (ll==1) {dN = dNdeta ;}
					if (ll==2) {dN = dNdzeta ;}
					// Assemble strain operator
					for (PetscInt i = 0 ; i < 3 ; i++) {
						for (PetscInt j = 0 ; j < 1 ; j++) {
							beta[i][j] = invJ[0][ll]*alpha1[i][j] + invJ[1][ll]*alpha2[i][j] +
                invJ[2][ll]*alpha3[i][j] ;
						}
					}
					// Add contributions to strain-displacement matrix
					for (PetscInt i = 0 ; i < 3 ; i++) {
						for (PetscInt j = 0 ; j < 8 ; j++) {
							Bmatrix[i][j] = Bmatrix[i][j] + beta[i][0]*dN[j] ;
						}
					}
				}
			}// end of kk
		}// end of jj
	}// end of ii

	return 0 ;
}


PetscScalar Poisson :: Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l) {
	// Function that returns the dot product of v1 and v2,
	// which must have the same length l
	PetscScalar result = 0.0 ;
	for (PetscInt i = 0 ; i < l ; i++) {
		result = result + v1[i]*v2[i] ;
	}
	return result ;
}


void Poisson :: DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta,
    PetscScalar zeta, PetscScalar *dNdxi, PetscScalar *dNdeta,
    PetscScalar *dNdzeta) {
  //differentiatedShapeFunctions - Computes differentiated shape functions
	// At the point given by (xi, eta, zeta).
	// With respect to xi:
	dNdxi[0]  = -0.125*(1.0-eta)*(1.0-zeta) ;
	dNdxi[1]  =  0.125*(1.0-eta)*(1.0-zeta) ;
	dNdxi[2]  =  0.125*(1.0+eta)*(1.0-zeta) ;
	dNdxi[3]  = -0.125*(1.0+eta)*(1.0-zeta) ;
	dNdxi[4]  = -0.125*(1.0-eta)*(1.0+zeta) ;
	dNdxi[5]  =  0.125*(1.0-eta)*(1.0+zeta) ;
	dNdxi[6]  =  0.125*(1.0+eta)*(1.0+zeta) ;
	dNdxi[7]  = -0.125*(1.0+eta)*(1.0+zeta) ;
	// With respect to eta:
	dNdeta[0] = -0.125*(1.0-xi)*(1.0-zeta) ;
	dNdeta[1] = -0.125*(1.0+xi)*(1.0-zeta) ;
	dNdeta[2] =  0.125*(1.0+xi)*(1.0-zeta) ;
	dNdeta[3] =  0.125*(1.0-xi)*(1.0-zeta) ;
	dNdeta[4] = -0.125*(1.0-xi)*(1.0+zeta) ;
	dNdeta[5] = -0.125*(1.0+xi)*(1.0+zeta) ;
	dNdeta[6] =  0.125*(1.0+xi)*(1.0+zeta) ;
	dNdeta[7] =  0.125*(1.0-xi)*(1.0+zeta) ;
	// With respect to zeta:
	dNdzeta[0]= -0.125*(1.0-xi)*(1.0-eta) ;
	dNdzeta[1]= -0.125*(1.0+xi)*(1.0-eta) ;
	dNdzeta[2]= -0.125*(1.0+xi)*(1.0+eta) ;
	dNdzeta[3]= -0.125*(1.0-xi)*(1.0+eta) ;
	dNdzeta[4]=  0.125*(1.0-xi)*(1.0-eta) ;
	dNdzeta[5]=  0.125*(1.0+xi)*(1.0-eta) ;
	dNdzeta[6]=  0.125*(1.0+xi)*(1.0+eta) ;
	dNdzeta[7]=  0.125*(1.0-xi)*(1.0+eta) ;
}


PetscScalar Poisson :: Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]) {
	//inverse3M - Computes the inverse of a 3x3 matrix
	PetscScalar detJ = J[0][0]*(J[1][1]*J[2][2]-J[2][1]*J[1][2]) -
    J[0][1]*(J[1][0]*J[2][2]-J[2][0]*J[1][2]) +
    J[0][2]*(J[1][0]*J[2][1]-J[2][0]*J[1][1]) ;
	invJ[0][0] = (J[1][1]*J[2][2]-J[2][1]*J[1][2])/detJ ;
	invJ[0][1] = -(J[0][1]*J[2][2]-J[0][2]*J[2][1])/detJ ;
	invJ[0][2] = (J[0][1]*J[1][2]-J[0][2]*J[1][1])/detJ ;
	invJ[1][0] = -(J[1][0]*J[2][2]-J[1][2]*J[2][0])/detJ ;
	invJ[1][1] = (J[0][0]*J[2][2]-J[0][2]*J[2][0])/detJ ;
	invJ[1][2] = -(J[0][0]*J[1][2]-J[0][2]*J[1][0])/detJ ;
	invJ[2][0] = (J[1][0]*J[2][1]-J[1][1]*J[2][0])/detJ ;
	invJ[2][1] = -(J[0][0]*J[2][1]-J[0][1]*J[2][0])/detJ ;
	invJ[2][2] = (J[0][0]*J[1][1]-J[1][0]*J[0][1])/detJ ;
	return detJ ;
}

} // namespace PARA_FEA